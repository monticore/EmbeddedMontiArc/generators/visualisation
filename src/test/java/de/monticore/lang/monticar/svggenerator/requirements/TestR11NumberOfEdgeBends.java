/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Assures that there are not too many edge bends
 */
public class TestR11NumberOfEdgeBends {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNoBends() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.noBends", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(0, getTotalNumberOfEdgeBends(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testSplit() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(2, getTotalNumberOfEdgeBends(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(0, getTotalNumberOfEdgeBends(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testBackConnector() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(4, getTotalNumberOfEdgeBends(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testBusConnector() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(16, getTotalNumberOfEdgeBends(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testZeroLength() {
        LineSymbol zeroLength = new LineSymbol(new Point(3, 3), new Point(3, 3), 0, LayoutMode.NORMAL, false);
        assertTrue(isPoint(zeroLength));
    }

    @Test
    public void testConnectedLines() {
        LineSymbol verticalA = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol verticalB = new LineSymbol(new Point(0, 0), new Point(0, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalA = new LineSymbol(new Point(-1, 0), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalB = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);

        assertTrue(isConnected(verticalA, horizontalA));
        assertTrue(isConnected(horizontalA, verticalA));
        assertTrue(isConnected(verticalA, verticalB));
        assertTrue(isConnected(verticalB, verticalA));
        assertTrue(isConnected(horizontalA, horizontalB));
        assertTrue(isConnected(horizontalB, horizontalA));
    }

    @Test
    public void testUnconnectedLines() {
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontal = new LineSymbol(new Point(-1, 0), new Point(0, 0), 0, LayoutMode.NORMAL, false);

        LineSymbol notConnectedVertical = new LineSymbol(new Point(-10, 10), new Point(-10, 9), 0, LayoutMode.NORMAL, false);
        LineSymbol notConnectedHorizontal = new LineSymbol(new Point(10, 10), new Point(11, 10), 0, LayoutMode.NORMAL, false);

        assertFalse(isConnected(vertical, notConnectedVertical));
        assertFalse(isConnected(notConnectedVertical, vertical));
        assertFalse(isConnected(vertical, notConnectedHorizontal));
        assertFalse(isConnected(notConnectedHorizontal, vertical));
        assertFalse(isConnected(horizontal, notConnectedHorizontal));
        assertFalse(isConnected(notConnectedHorizontal, horizontal));
    }

    @Test
    public void testBends() {
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontal = new LineSymbol(new Point(-1, 0), new Point(0, 0), 0, LayoutMode.NORMAL, false);

        assertTrue(isBend(vertical, horizontal));
        assertTrue(isBend(horizontal, vertical));
    }

    /**
     * Tests if the test recognizes no bend if one of the passed lines is of length zero
     */
    @Test(expected = AssertionError.class)
    public void testNonBends() {
        LineSymbol verticalA = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol zeroLength = new LineSymbol(new Point(3, 3), new Point(3, 3), 0, LayoutMode.NORMAL, false);

        assertTrue(isBend(verticalA, zeroLength));
    }

    /**
     * Tests if the test recognizes two vertical lines as no bend
     */
    @Test(expected = AssertionError.class)
    public void testNoBendsVertical() {
        LineSymbol verticalA = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol verticalB = new LineSymbol(new Point(0, 0), new Point(0, -1), 0, LayoutMode.NORMAL, false);

        assertTrue(isBend(verticalA, verticalB));
    }

    /**
     * Tests if the test recognizes two horizontal lines as no bend
     */
    @Test(expected = AssertionError.class)
    public void testNoBendsHorizontal() {
        LineSymbol horizontalA = new LineSymbol(new Point(-1, 0), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalB = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);

        assertTrue(isBend(horizontalA, horizontalB));
    }

    private int getTotalNumberOfEdgeBends(ExpandedComponentInstanceSymbol enclosingComponent) {
        return enclosingComponent.getConnectors().stream().mapToInt(this::getNumberOfEdgeBends).sum();
    }

    private int getNumberOfEdgeBends(ConnectorSymbol connectorSymbol) {
        int bends = 0;

        List<LineSymbol> lineSymbols = new ArrayList<>(Tags.getLineSymbols(connectorSymbol, LayoutMode.NORMAL));
        for (int i = 0; i < lineSymbols.size() - 1; i++) {
            for (int j = i + 1; j < lineSymbols.size(); j++) {
                LineSymbol first = lineSymbols.get(i);
                LineSymbol second = lineSymbols.get(j);

                if (isBend(first, second)) {
                    bends += 1;
                }
            }
        }

        return bends;
    }

    private boolean isBend(LineSymbol first, LineSymbol second) {
        if (isPoint(first) || isPoint(second) || !isConnected(first, second)) {
            return false;
        }

        if (first.isVertical()) {
            return second.isHorizontal();
        } else {
            return second.isVertical();
        }
    }

    private boolean isConnected(LineSymbol first, LineSymbol second) {
        Point firstStart = first.getStart();
        Point firstEnd = first.getEnd();
        Point secondStart = second.getStart();
        Point secondEnd = second.getEnd();

        return firstStart.equals(secondStart)
                || firstStart.equals(secondEnd)
                || firstEnd.equals(secondStart)
                || firstEnd.equals(secondEnd);
    }

    private boolean isPoint(LineSymbol lineSymbol) {
        return lineSymbol.isVertical() && lineSymbol.isHorizontal();
    }

}
