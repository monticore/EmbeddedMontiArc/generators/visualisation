/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Random;

/**
 * Created by kilianm on 01.01.2017.
 */
public class HTMLBuilderTest {
    private Random random;

    @BeforeClass
    public static void init(){
        LogConfig.init();
    }
    @Before
    public void setUp() {
        random = new Random();
    }

    @Test
    public void checkHeader() {
        System.out.println("Testing HTML header.");
        String randomInstName = TestingUtilities.generateRandomString(random, 20);

        TemplateBuilder tb = new TemplateBuilder();

        String html = tb.buildHTMLFile(randomInstName, 10000, 10000, new TemplateBuilder.Hierarchy(), "");

        Assert.assertNotNull(html);

    }

}
