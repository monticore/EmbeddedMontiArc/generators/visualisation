/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Created by Peters Notebook on 10.11.2017.
 */
public class SVGMainTest {

    @Test
    public void testSVGMain() {
        SVGMain.main(new String[]{"--input", "testManuelSchrick.bumperBot", "--onlineIDE", "--modelPath", "src/test/resources/", "--recursiveDrawing", "true"});

        File htmlFile = new File("output/testManuelSchrick.bumperBot.html");
        assertTrue(htmlFile.exists());

        File svgFile = new File("output/testManuelSchrick.bumperBot.svg");
        assertTrue(svgFile.exists());
    }
}
