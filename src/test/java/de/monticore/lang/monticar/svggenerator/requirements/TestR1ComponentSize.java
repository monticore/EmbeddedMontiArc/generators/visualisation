/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.RoutesUtils;
import de.monticore.lang.monticar.svggenerator.calculators.helper.TableDimensions;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LayoutPosition;
import de.monticore.symboltable.Scope;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * Assures that components don't get too large ot too small
 */
public class TestR1ComponentSize {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testSmallComponents() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        testComponentSizes(calculator.routesLayoutCalculator.layoutState);
    }

    @Test
    public void testComponentsWithBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        testComponentSizes(calculator.routesLayoutCalculator.layoutState);
    }

    @Test
    public void testComponentsWithBackConnectorAndDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        testComponentSizes(calculator.routesLayoutCalculator.layoutState);
    }

    /**
     * Tests if the test fails when called with a component that is not wide enough to fully contain its name.
     */
    @Test(expected = AssertionError.class)
    public void testComponentNotWideEnough() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);

        ExpandedComponentInstanceSymbol component = instance.getSubComponents().stream().findAny().get();

        ComponentLayoutSymbol layoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 10, DrawingConstants.ROW_HEIGHT_LARGE, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);

        List<String> portNames = component.getPorts().stream().map(PortSymbol::getName).collect(Collectors.toList());
        testComponentSize(layoutSymbol, component.getName(), portNames, calculator.routesLayoutCalculator.layoutState);
    }

    /**
     * Tests if the test fails when called with a component that is not tall enough to contain its name and one port
     */
    @Test(expected = AssertionError.class)
    public void testComponentNotTallEnough() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);

        ExpandedComponentInstanceSymbol component = instance.getSubComponents().stream().findAny().get();
        ComponentLayoutSymbol oldLayoutSymbol = Tags.getFirstLayoutTag(component, LayoutMode.NORMAL).get();

        ComponentLayoutSymbol layoutSymbol = new ComponentLayoutSymbol(0, 0, 0, oldLayoutSymbol.getWidth(), DrawingConstants.ROW_HEIGHT, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);

        List<String> portNames = component.getPorts().stream().map(PortSymbol::getName).collect(Collectors.toList());
        testComponentSize(layoutSymbol, component.getName(), portNames, calculator.routesLayoutCalculator.layoutState);
    }

    private void testComponentSizes(RoutesLayoutState layoutState) {
        layoutState.componentInstanceSymbol.getSubComponents().forEach( component -> {
            Optional<ComponentLayoutSymbol> layoutSymbol = Tags.getFirstLayoutTag(component, LayoutMode.NORMAL);

            // Make sure the component has been drawn
            assertTrue(layoutSymbol.isPresent());

            List<String> portNames = component.getPorts().stream().map(PortSymbol::getName).collect(Collectors.toList());
            testComponentSize(layoutSymbol.get(), component.getName(), portNames, layoutState);
        });
    }

    public void testComponentSize(ComponentLayoutSymbol layoutSymbol, String componentName, List<String> portNames, RoutesLayoutState layoutState) {
        TableDimensions dimensions = new TableDimensions(LayoutMode.NORMAL);

        int width = layoutSymbol.getWidth();
        int height = layoutSymbol.getHeight();

        // Test if there is enough horizontal space for two ports in the same row
        assertTrue(width > DrawingConstants.PORT_WIDTH);

        // Test if there is enough vertical space for the name of the component
        assertTrue(height >= DrawingConstants.ROW_HEIGHT_LARGE);

        Node node = getNodeForComponent(layoutState.nodeOrder, componentName);
        int column = RoutesUtils.getColumnOfNode(node, layoutState.routes);
        int columnWidth = RoutesUtils.getColumnWidth(column, layoutState.routes);

        // Assure that component is not wider than column
        assertTrue(width <= columnWidth);

        // Assure that component is wider than the name
        assertTrue(width > dimensions.getDimensionForText(componentName));

        // Assure that component is wider than each port name
        for (String portName: portNames) {
            assertTrue(width > dimensions.getDimensionForText(portName));
        }
    }

    private Node getNodeForComponent(List<Node> nodes, String componentName) {
        Optional<Node> node = nodes.stream().filter( n -> n.compName.equals(componentName)).findFirst();

        assertTrue(node.isPresent());

        return node.get();
    }
}
