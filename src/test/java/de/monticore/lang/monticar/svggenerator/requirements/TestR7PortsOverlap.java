/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.utils.Bounds;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;

public class TestR7PortsOverlap {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.k33.k33");
        checkIfPortsOverlap(getAllPorts(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.normalBus");
        checkIfPortsOverlap(getAllPorts(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testDecomposedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.decomposedBus");
        checkIfPortsOverlap(getAllPorts(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testBackConnectorBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.backConnectorBus");
        checkIfPortsOverlap(getAllPorts(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testConnectedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.connectedBus");
        checkIfPortsOverlap(getAllPorts(layoutState.componentInstanceSymbol));
    }

    @Test
    public void testNoOverlap() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds outsideBounds = new Bounds(200, 300, 200, 300);

        checkIfBoundsOverlap(referenceBounds, outsideBounds);
    }

    /**
     * Test if the test fails when the bottom left corner of a port is covered by another port
     */
    @Test(expected = AssertionError.class)
    public void testOverlapBottomLeft() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingA = new Bounds(-50, 50, 50, 150);

        checkIfBoundsOverlap(referenceBounds, partiallyOverlappingA);
    }

    /**
     * Test if the test fails when the top left corner of a port is covered by another port
     */
    @Test(expected = AssertionError.class)
    public void testOverlapTopLeft() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingB = new Bounds(-50, 50, -50, 50);

        checkIfBoundsOverlap(referenceBounds, partiallyOverlappingB);
    }

    /**
     * Test if the test fails when the top right corner of a port is covered by another port
     */
    @Test(expected = AssertionError.class)
    public void testOverlapTopRight() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingC = new Bounds(50, 150, 50, 150);

        checkIfBoundsOverlap(referenceBounds, partiallyOverlappingC);
    }

    /**
     * Test if the test fails when the bottom right corner of a port is covered by another port
     */
    @Test(expected = AssertionError.class)
    public void testOverlapBottomRight() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingD = new Bounds(50, 150, -50, 50);  // bottom right corner

        checkIfBoundsOverlap(referenceBounds, partiallyOverlappingD);
    }

    /**
     * Test if the test fails when one port is displayed on top of another port
     */
    @Test(expected = AssertionError.class)
    public void testFullyInside() {
        Bounds referenceBounds = new Bounds(0, 100, 0, 100);
        Bounds fullyInside = new Bounds(10, 90, 10, 90);

        checkIfBoundsOverlap(referenceBounds, fullyInside);
    }

    private List<PortSymbol> getAllPorts(ExpandedComponentInstanceSymbol enclosingComponent) {
        List<PortSymbol> ports = new ArrayList<>(enclosingComponent.getPorts());

        enclosingComponent.getSubComponents().stream()
                .map(ExpandedComponentInstanceSymbol::getPorts)
                .forEach(ports::addAll);

        return ports;
    }

    private void checkIfPortsOverlap(Collection<PortSymbol> ports) {
        List<Bounds> bounds = ports.stream().map(Bounds::new).collect(Collectors.toList());

        for (int i = 0; i < bounds.size() - 1; i++) {
            for (int j = i + 1; j < bounds.size(); j++) {
                checkIfBoundsOverlap(bounds.get(i), bounds.get(j));
            }
        }
    }

    private void checkIfBoundsOverlap(Bounds portBounds, Bounds otherPortBounds) {
        assertFalse(portBounds.overlapsWith(otherPortBounds));
        assertFalse(otherPortBounds.overlapsWith(portBounds));
    }

    private RoutesLayoutState resolveAndDraw(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        return calculator.routesLayoutCalculator.layoutState;
    }

}
