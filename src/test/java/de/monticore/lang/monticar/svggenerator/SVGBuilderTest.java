/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by kilianm on 01.01.2017.
 */
public class SVGBuilderTest {


    @BeforeClass
    public static void init(){
        LogConfig.init();
    }

    @Test
    public void checkSVGFile() {

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("svgbody", "OMG DAS IST EIN TEST UND DIESER STRING STEHT GANZ BESTIMMT NICHT " +
                "ZUFÄLLIG IRGENDWO RUM");
        TemplateBuilder tb = new TemplateBuilder();
        String svg = tb.build("svg.ftl", hashMap);

        //------Check structure/syntax of the header-------
        int leftBraces = TestingUtilities.getCountOf(svg, "<");
        int rightBraces = TestingUtilities.getCountOf(svg, ">");
        Assert.assertEquals("Header does not have exactly two <.", 2, leftBraces);
        Assert.assertEquals("Header does not have exactly two >.", 2, rightBraces);
        // Left brace must be the first character
        Assert.assertEquals("< is not the first character in the header.", 0, svg.indexOf("<"));
        // Right brace must be the last character
        String headerTrimmed = svg.trim();
        Assert.assertEquals("> is not the last character in the header (trailing whitespace removed).",
                0, headerTrimmed.indexOf("<"));

        //------Check contents-----------------------------
        Assert.assertTrue("Header does not contain svg element",
                svg.contains("svg"));
        Assert.assertTrue("Header does not include reference to standard",
                svg.contains("http://www.w3.org/2000/svg"));
        Assert.assertTrue("Header does not contain a version attribute",
                svg.contains("version"));
        Assert.assertTrue("Template engine does not work.", svg.contains("OMG DAS IST EIN TEST " +
                "UND DIESER STRING STEHT GANZ BESTIMMT NICHT " +
                        "ZUFÄLLIG IRGENDWO RUM"));

        Assert.assertFalse("Template engine does not replace template strings", svg.contains
                ("${svgbody}"));

    }
}
