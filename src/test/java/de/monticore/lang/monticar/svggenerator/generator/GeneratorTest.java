/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generator;

import de.monticore.lang.monticar.svggenerator.LogConfig;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Felix on 07.03.2017.
 */
public class GeneratorTest {


    @BeforeClass
    public static void init(){
        LogConfig.init();
    }

@Test
    public void mainTest() throws IOException, TemplateException {
// Create your Configuration instance, and specify if up to what FreeMarker
// version (here 2.3.25) do you want to apply the fixes that are not 100%
// backward-compatible. See the Configuration JavaDoc for details.
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);

// Specify the source where the template files come from. Here I set a
// plain directory for it, but non-file-system sources are possible too:
        cfg.setDirectoryForTemplateLoading(Paths.get("src/test/resources/templates").toFile());

// Set the preferred charset template files are stored in. UTF-8 is
// a good choice in most applications:
        cfg.setDefaultEncoding("UTF-8");

// Sets how errors will appear.
// During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

// Don't log exceptions inside FreeMarker that it will thrown at you anyway:
        cfg.setLogTemplateExceptions(false);

        Template temp = cfg.getTemplate("testTemplate.ftl");

        Map<String, Object> root = new HashMap<>();
        root.put("test", "TEST");

        StringWriter out = new StringWriter();
        temp.process(root, out);

        Assert.assertEquals(out.toString().replace("\\r", "").replace("\\n", ""), "test TEST");

    }





}
