/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Tests if there are too many crossing edges
 */
public class TestR10NumberOfCrossingEdges {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNoCrossings() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(0, getNumberOfCrossings(layoutState.componentInstanceSymbol.getConnectors()));
    }

    /**
     * A graph with five nodes where every node is connected to every other node
     * contains at least one edge crossing
     */
    @Test
    public void testFiveClique() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.split.clique", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        int numberOfCrossings = getNumberOfCrossings(layoutState.componentInstanceSymbol.getConnectors());

        assertTrue(numberOfCrossings > 0);
        assertTrue(numberOfCrossings <= 2);
    }

    /**
     * A K33 graph contains at least one edge crossing
     * https://en.wikipedia.org/wiki/Three_utilities_problem
     */
    @Test
    public void testK33Graph() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.split.clique", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        int numberOfCrossings = getNumberOfCrossings(layoutState.componentInstanceSymbol.getConnectors());

        assertTrue(numberOfCrossings > 0);
        assertTrue(numberOfCrossings <= 2);
    }

    @Test
    public void testBusWithoutCrossings() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertEquals(0, getNumberOfCrossings(layoutState.componentInstanceSymbol.getConnectors()));
    }

    @Test
    public void testCrossingLines() {
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontal = new LineSymbol(new Point(-1, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);

        assertTrue(isCrossing(vertical, horizontal));
    }

    /**
     * Tests if the test recognizes a horizontal and a vertical line that are not crossing
     */
    @Test
    public void testNonCrossingLine() {
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontal = new LineSymbol(new Point(-1, 2), new Point(1, 2), 0, LayoutMode.NORMAL, false);

        assertFalse(isCrossing(vertical, horizontal));
    }

    /**
     * Tests if the test recognizes two horizontal lines as not crossing
     */
    @Test(expected = AssertionError.class)
    public void testTwoHorizontalLines() {
        LineSymbol horizontalA = new LineSymbol(new Point(-1, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalB = new LineSymbol(new Point(-1, 2), new Point(1, 2), 0, LayoutMode.NORMAL, false);

        assertTrue(isCrossing(horizontalA, horizontalB));
    }

    /**
     * Tests if the test recognizes two vertical lines as not crossing
     */
    @Test(expected = AssertionError.class)
    public void testTwoVerticalLines() {
        LineSymbol verticalA = new LineSymbol(new Point(0, 1), new Point(0, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol verticalB = new LineSymbol(new Point(2, 1), new Point(2, -1), 0, LayoutMode.NORMAL, false);

        isCrossing(verticalA, verticalB);
    }

    private int getNumberOfCrossings(Collection<ConnectorSymbol> connectors) {
        int crossings = 0;

        ConnectorSymbol[] connectorsArray = connectors.toArray(new ConnectorSymbol[connectors.size()]);

        for (int i = 0; i < connectorsArray.length - 1; i++) {
            for (int j = i + 1; j < connectorsArray.length; j++) {
                crossings += getNumberOfCrossings(connectorsArray[i], connectorsArray[j]);
            }
        }

        return crossings;
    }

    private int getNumberOfCrossings(ConnectorSymbol connector, ConnectorSymbol otherConnector) {
        Collection<LineSymbol> lineSymbols = Tags.getLineSymbols(connector, LayoutMode.NORMAL);
        List<LineSymbol> verticalLines = lineSymbols.stream().filter(LineSymbol::isVertical).collect(Collectors.toList());
        List<LineSymbol> horizontalLines = lineSymbols.stream().filter(LineSymbol::isHorizontal).collect(Collectors.toList());

        Collection<LineSymbol> otherLineSymbols = Tags.getLineSymbols(otherConnector, LayoutMode.NORMAL);
        List<LineSymbol> otherVerticalLines = otherLineSymbols.stream().filter(LineSymbol::isVertical).collect(Collectors.toList());
        List<LineSymbol> otherHorizontalLines = otherLineSymbols.stream().filter(LineSymbol::isHorizontal).collect(Collectors.toList());

        int crossings = 0;

        for (LineSymbol verticalLine : verticalLines) {
            for (LineSymbol otherHorizontalLine : otherHorizontalLines) {
                if (isCrossing(verticalLine, otherHorizontalLine)) {
                    crossings += 1;
                }
            }
        }

        for (LineSymbol horizontalLine : horizontalLines) {
            for (LineSymbol otherVerticalLine : otherVerticalLines) {
                if (isCrossing(otherVerticalLine, horizontalLine)) {
                    crossings += 1;
                }
            }
        }

        return crossings;
    }

    private boolean isCrossing(LineSymbol verticalLine, LineSymbol horizontalLine) {
        // Assure first line is vertical
        assertTrue(verticalLine.getStart().x == verticalLine.getEnd().x);

        // Assure second line is horizontal
        assertTrue(horizontalLine.getStart().y == horizontalLine.getEnd().y);

        int top = Math.min(verticalLine.getStart().y, verticalLine.getEnd().y);
        int bottom = Math.max(verticalLine.getStart().y, verticalLine.getEnd().y);
        int left = Math.min(horizontalLine.getStart().x, horizontalLine.getEnd().x);
        int right = Math.max(horizontalLine.getStart().x, horizontalLine.getEnd().x);
        int verticalX = verticalLine.getStart().x;
        int horizontalY = horizontalLine.getStart().y;

        boolean horizontalIsBetweenTopAndBottom = horizontalY >= top && horizontalY <= bottom;
        boolean verticalIsBetweenLeftAndRight = verticalX >= left && verticalX <= right;

        return horizontalIsBetweenTopAndBottom && verticalIsBetweenLeftAndRight;
    }
}
