/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.RoutesUtils;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Tests if edges are unnecessarily long by checking if a node can be moved to the left
 */
public class TestR15EdgeLength {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnections() {
        testModel("testManuelSchrick.noBends");
    }

    @Test
    public void testSplit() {
        testModel("testManuelSchrick.k33.k33");
    }

    @Test
    public void testBackConnector() {
        testModel("testManuelSchrick.thesisExample.thesisExample");
    }

    @Test
    public void testBus() {
        testModel("testManuelSchrick.bus.normalBus");
    }

    @Test
    public void testDecomposedBus() {
        testModel("testManuelSchrick.bus.decomposedBus");
    }

    @Test
    public void testBackConnectorBus() {
        testModel("testManuelSchrick.bus.backConnectorBus");
    }

    @Test
    public void testConnectedBus() {
        testModel("testManuelSchrick.bus.connectedBus");
    }

    @Test
    public void testCannotMove() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.noBends", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        for (Node node: layoutState.nodeOrder) {
            assertFalse(canMoveNode(node, layoutState.routes));
        }
    }

    /**
     * Tests if the test fails when there is a connector that can be shortened
     */
    @Test(expected = AssertionError.class)
    public void testCanMove() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.noBends", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        for (Route route: layoutState.routes) {
            route.expandAt(2);
        }

        for (Node node: layoutState.nodeOrder) {
            assertFalse(canMoveNode(node, layoutState.routes));
        }
    }

    private void testModel(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        for (Node node: layoutState.nodeOrder) {
            assertFalse(canMoveNode(node, layoutState.routes));
        }
    }

    private boolean canMoveNode(Node node, List<Route> routes) {
        int columnCount = RoutesUtils.getColumnCount(routes);
        int columnIndex = RoutesUtils.getColumnOfNode(node, routes);

        int adjacentColumnIndex;
        if (node.isTemporary || node.getInputs().isEmpty()) {
            adjacentColumnIndex = columnIndex + 1;
        } else {
            adjacentColumnIndex = columnIndex - 1;
        }

        if (adjacentColumnIndex <= 1 || adjacentColumnIndex >= columnCount - 2) {
            return false;
        }

        for (Route route : routes) {
            Node nodeInRow = route.getNode(columnIndex);

            if (nodeInRow == null) {
                continue;
            }

            RouteState adjacentState = route.getState(adjacentColumnIndex);

            if (adjacentState == RouteState.NODE) {
                return false;
            }
        }

        return true;
    }

}
