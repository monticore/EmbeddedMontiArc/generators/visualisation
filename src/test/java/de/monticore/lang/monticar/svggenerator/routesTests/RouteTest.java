/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.routesTests;

import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Peters Notebook on 10.11.2017.
 */
public class RouteTest {

    private Node nodeA = new Node(null, "nodeA");
    private Node nodeB = new Node(null, "nodeB");
    private Node nodeC = new Node(null, "nodeC");

    @Test
    public void testRouteMethods() {
        Route route = new Route();
        Route secondRoute = new Route();

        assertEquals(route.size(), 0);
        assertNotEquals(route.id, secondRoute.id);

        route.addNode(nodeA);

        assertEquals(route.size(), 1);
        assertNotNull(route.getNode(0));
        assertEquals(route.getNode(0).compName, nodeA.compName);

        route.addNode(nodeB);

        assertEquals(route.size(), 2);
        assertNotNull(route.getNode(1));
        assertEquals(route.getNode(1).compName, nodeB.compName);

        route.expandAt(1);
        route.expandAt(1);
        route.expandAt(1);

        assertEquals(route.nodeCount(), 2);
        assertEquals(route.size(), 5);
        assertEquals(route.getNode(0).compName, nodeA.compName);
        assertEquals(route.getNode(4).compName, nodeB.compName);
        assertNull(route.getNode(2));

        assertTrue(route.isFreeBetweenNodes(0, 4));
        route.set(2, nodeC);

        assertEquals(route.nodeCount(), 3);
        assertNotNull(route.getNode(2));
        assertEquals(route.getNode(2).compName, nodeC.compName);
    }

    @Test
    public void testIsFreeBetweenNodes() {
        Route route = new Route();
        route.addNode(nodeA);
        route.addNode(nodeB);

        assertTrue(route.isFreeBetweenNodes(0, 1));

        route.expandAt(1);
        route.expandAt(1);
        route.expandAt(1);

        assertTrue(route.isFreeBetweenNodes(0, 4));

        nodeC.isTemporary = true;
        route.set(2, nodeC);

        assertFalse(route.isFreeBetweenNodes(0, 4));
        assertTrue(route.isFreeBetweenNodes(0, 4, true));

        nodeC.isTemporary = false;

        assertTrue(route.isFreeBetweenNodes(0, 2));
        assertFalse(route.isFreeBetweenNodes(0, 4, true));

        route.set(1, RouteState.ROUTE);
        assertFalse(route.isFreeBetweenNodes(0, 2));
        assertFalse(route.isFreeBetweenNodes(0, 2, true));
    }

    @Test
    public void testEquals() {
        Route route = new Route();
        route.addNode(nodeA);
        route.addNode(nodeB);

        route.expandAt(1);
        route.expandAt(1);
        route.expandAt(1);
        route.set(2, nodeC);
        route.set(1, RouteState.ROUTE);

        Route copyRoute = route.copyNodesOnly();
        Route copyOfCopyRoute = copyRoute.copyNodesOnly();

        assertFalse(route.equals(copyRoute));
        assertFalse(copyRoute.equals(copyOfCopyRoute));
        assertTrue(route.equals(route));
    }

    @Test
    public void testToString() {
        Route route = new Route();
        route.addNode(nodeA);
        route.addNode(nodeB);

        route.id = 0;
        assertEquals(route.toString(), "   0:  nodeA  nodeB ");
        route.id = 42;
        assertEquals(route.toString(), "  42:  nodeA  nodeB ");
        route.id = 128;
        assertEquals(route.toString(), " 128:  nodeA  nodeB ");
        route.id = 1337;
        assertEquals(route.toString(), "1337:  nodeA  nodeB ");

        route.expandAt(1);
        route.expandAt(1);
        route.expandAt(1);
        route.set(2, nodeC);
        route.set(1, RouteState.ROUTE);

        assertEquals(route.toString(), "1337:  nodeA --- nodeC     nodeB ");
    }
}
