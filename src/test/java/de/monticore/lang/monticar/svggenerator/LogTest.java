/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Log;
import de.monticore.symboltable.Scope;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Peters Notebook on 11.11.2017.
 */
public class LogTest {

    private MainCalculator calculator = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();

    @Test
    public void testLog() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.split.clique", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        assertNotNull(instance);

        RoutesLayoutState tempLayoutState = new RoutesLayoutState();
        tempLayoutState.componentInstanceSymbol = instance;
        Log.printUndrawnComponents(tempLayoutState);
        Log.printUndrawnConnectors(tempLayoutState);
        Log.printUndrawnPorts(tempLayoutState);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        Log.printNodes(layoutState);
        Log.printRoutes(layoutState);
        Log.printBusses(layoutState);
        Log.printUndrawnComponents(layoutState);
        Log.printUndrawnConnectors(layoutState);
        Log.printUndrawnPorts(layoutState);
    }
}
