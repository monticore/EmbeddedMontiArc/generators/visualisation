/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Tests if there are no unnecessary dots drawn onto connectors.
 * Dots are drawn on connects to symbolize which lines are connected and which are not.
 * This helps to distinguish between e.g. an edge crossing and a bus.
 *
 * Unignore this test to check if the bug was fixed that caused too many dots to be drawn.
 */
@Ignore
public class TestConnectorDots {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.k33.k33");
        testNumberOfDots(0 , layoutState);
        testIfDotsAreCorrect(layoutState);
    }

    @Test
    public void testNormalBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.normalBus");
        testNumberOfDots(10 , layoutState);
        testIfDotsAreCorrect(layoutState);
    }

    @Test
    public void testBackConnectorBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.backConnectorBus");
        testNumberOfDots(10 , layoutState);
        testIfDotsAreCorrect(layoutState);
    }

    private void testIfDotsAreCorrect(RoutesLayoutState layoutState) {
        List<LineSymbol> lines = new ArrayList<>();
        List<Point> points = new ArrayList<>();

        layoutState.componentInstanceSymbol.getConnectors().stream()
                .map(this::getAllNonZeroLines)
                .forEach(lines::addAll);

        layoutState.componentInstanceSymbol.getConnectors().stream()
                .map(this::getAllPoints)
                .forEach(connectorPoints -> {
                    for (Point point: connectorPoints) {
                        if (!points.contains(point)) {
                            points.add(point);
                        }
                    }
                });

        points.forEach(point -> {
            boolean shouldHaveDot = shouldPointHaveDot(point, lines);
            boolean doesHaveDot = doesPointHaveDot(point, lines);
            if (shouldHaveDot && !doesHaveDot) {
                throw new AssertionError("Point " + point + " has no dot even though it should.");
            } else if (!shouldHaveDot && doesHaveDot) {
                throw new AssertionError("Point " + point + " has a dot even though it shouldn't.");
            }
        });
    }

    private void testNumberOfDots(int expectedMaximum, RoutesLayoutState layoutState) {
        List<LineSymbol> lineSymbols = new ArrayList<>();
        layoutState.componentInstanceSymbol.getConnectors()
                .stream()
                .map(this::getAllNonZeroLines)
                .forEach(lineSymbols::addAll);

        List<Point> pointsWithDots = new ArrayList<>();
        lineSymbols.stream()
                .filter(LineSymbol::hasKnotAtStart)
                .map(LineSymbol::getStart)
                .forEach(point -> {
                    if (!pointsWithDots.contains(point)) {
                        pointsWithDots.add(point);
                    }
                });

        if(pointsWithDots.size() > expectedMaximum) {
            throw new AssertionError("Wrong number of dots: expected at most " + expectedMaximum + ", actual was " + pointsWithDots.size());
        }
    }

    private List<LineSymbol> getAllNonZeroLines(ConnectorSymbol connectorSymbol) {
        return Tags.getLineSymbols(connectorSymbol, LayoutMode.NORMAL).stream()
                .filter(line ->  !line.getStart().equals(line.getEnd()))
                .collect(Collectors.toList());
    }

    private List<Point> getAllPoints(ConnectorSymbol connectorSymbol) {
        return getAllNonZeroLines(connectorSymbol).stream()
                .map(LineSymbol::getStart)
                .collect(Collectors.toList());
    }

    private RoutesLayoutState resolveAndDraw(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        return calculator.routesLayoutCalculator.layoutState;
    }

    private boolean shouldPointHaveDot(Point point, List<LineSymbol> lines) {
        List<LineSymbol> startConnectedLines = lines.stream()
                .filter(line -> line.getStart().equals(point))
                .collect(Collectors.toList());
        List<LineSymbol> endConnectedLines = lines.stream()
                .filter(line -> line.getEnd().equals(point))
                .collect(Collectors.toList());

        int hasTopLine = 0;
        int hasBottomLine = 0;
        int hasLeftLine = 0;
        int hasRightLine = 0;
        List<Integer> lineSizes = new ArrayList<>();

        for (LineSymbol line: startConnectedLines) {
            Point end = line.getEnd();
            if (end.x < point.x) {
                hasBottomLine = 1;
            } else if (end.x > point.x) {
                hasTopLine = 1;
            } else if (end.y > point.y) {
                hasRightLine = 1;
            } else if (end.y < point.y) {
                hasLeftLine = 1;
            }

            if (!lineSizes.contains(line.getLineWidth())) {
                lineSizes.add(line.getLineWidth());
            }
        }

        for (LineSymbol line: endConnectedLines) {
            Point start = line.getStart();
            if (start.x < point.x) {
                hasBottomLine = 1;
            } else if (start.x > point.x) {
                hasTopLine = 1;
            } else if (start.y > point.y) {
                hasRightLine = 1;
            } else if (start.y < point.y) {
                hasLeftLine = 1;
            }

            if (!lineSizes.contains(line.getLineWidth())) {
                lineSizes.add(line.getLineWidth());
            }
        }

        int connectedDirections = hasTopLine + hasBottomLine + hasLeftLine + hasRightLine;
        if (connectedDirections > 2) {
            return true;
        } else if (hasBottomLine + hasTopLine == 2) {
            return false;
        } else if (hasLeftLine + hasRightLine == 2) {
            return false;
        } else {
            return lineSizes.size() > 1;
        }
    }

    private boolean doesPointHaveDot(Point point, List<LineSymbol> lines) {
        for (LineSymbol line: lines) {
            if (line.getStart().equals(point) && line.hasKnotAtStart()) {
                return true;
            }
        }

        return false;
    }
}
