/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestR4ConnectorStartEnd {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testBackConnectorBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.backConnectorBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testConnectedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.connectedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    /**
     * Tests if the test fails if the connector has not been drawn.
     */
    @Test(expected=AssertionError.class)
    public void testNoLineSegments() {
        checkFirstAndLastLineSegments(new ArrayList<>());
    }

    /**
     * Tests if the test fails when the connector starts with a vertical line
     */
    @Test(expected=AssertionError.class)
    public void testFirstSegmentVertical() {
        LineSymbol horizontal = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);

        List<LineSymbol> lines = new ArrayList<>();
        lines.add(vertical);
        lines.add(horizontal);

        checkFirstAndLastLineSegments(lines);
    }

    /**
     * Tests if the test fails when the connector ends with a vertical line
     */
    @Test(expected=AssertionError.class)
    public void testLastSegmentVertical() {
        LineSymbol horizontal = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);

        List<LineSymbol> lines = new ArrayList<>();
        lines.add(horizontal);
        lines.add(vertical);

        checkFirstAndLastLineSegments(lines);
    }

    /**
     * Tests if the test fails when the connector starts and ends with a vertical line
     */
    @Test(expected=AssertionError.class)
    public void testFirstAndLastSegmentVertical() {
        LineSymbol horizontal = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol verticalA = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol verticalB = new LineSymbol(new Point(1, 0), new Point(0, -1), 0, LayoutMode.NORMAL, false);

        List<LineSymbol> lines = new ArrayList<>();
        lines.add(verticalA);
        lines.add(horizontal);
        lines.add(verticalB);

        checkFirstAndLastLineSegments(lines);
    }

    @Test
    public void testCorrectSegments() {
        LineSymbol horizontalA = new LineSymbol(new Point(-1, 1), new Point(0, 1), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalB = new LineSymbol(new Point(0, 0), new Point(1, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol vertical = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        List<LineSymbol> lines = new ArrayList<>();


        lines.add(horizontalA);
        lines.add(vertical);
        lines.add(horizontalB);
        checkFirstAndLastLineSegments(lines);
        lines.clear();
    }

    private void checkConnectors(Collection<ConnectorSymbol> connectors) {
        connectors.stream().map(c -> Tags.getLineSymbols(c, LayoutMode.NORMAL)).forEach(this::checkFirstAndLastLineSegments);
    }

    private void checkFirstAndLastLineSegments(Collection<LineSymbol> lineSymbols) {
        //Check if connector has been drawn
        assertFalse(lineSymbols.isEmpty());

        //FInd first and last lines which are the ones that are connected with at most one other line
        HashMap<Point, Integer> linesPerPoint = new HashMap<>();
        for (LineSymbol lineSymbol : lineSymbols) {
            Integer countStart = linesPerPoint.getOrDefault(lineSymbol.getStart(), 0);
            Integer countEnd = linesPerPoint.getOrDefault(lineSymbol.getEnd(), 0);

            countStart += 1;
            countEnd += 1;

            linesPerPoint.put(lineSymbol.getStart(), countStart);
            linesPerPoint.put(lineSymbol.getEnd(), countEnd);
        }

        for (LineSymbol lineSymbol : lineSymbols) {
            int linesAtStart = linesPerPoint.getOrDefault(lineSymbol.getStart(), -1);
            int linesAtEnd = linesPerPoint.getOrDefault(lineSymbol.getEnd(), -1);

            // Assure that start and end of this line have at least one adjacent line segment
            assertTrue(linesAtEnd > 0);
            assertTrue(linesAtStart > 0);

            // If there are two line segments connected with start and end, this is
            // neither the first nor the last segment
            if (linesAtEnd > 1 && linesAtStart > 1) {
                continue;
            }

            Point start = lineSymbol.getStart();
            Point end = lineSymbol.getEnd();

            // Assure that the line segment is horizontal
            assertTrue(lineSymbol.isHorizontal());

            // Check if line segment goes from left to right
            assertTrue(end.x > start.x);
        }
    }

}
