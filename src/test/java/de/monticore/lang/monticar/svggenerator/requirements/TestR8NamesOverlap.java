/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.helper.TableDimensions;
import de.monticore.lang.monticar.svggenerator.utils.Bounds;
import de.monticore.symboltable.Scope;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.*;
import org.w3c.dom.svg.SVGDocument;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestR8NamesOverlap {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;
    private TableDimensions dimensions = new TableDimensions(LayoutMode.NORMAL);

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.k33.k33");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testBackConnection() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.thesisExample.thesisExample");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.normalBus");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testDecomposedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.decomposedBus");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testConnectedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.connectedBus");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testBackConnectorBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.backConnectorBus");
        SVGDocument document = loadSVGFromFile(svgGenerator.getSvgFileName(layoutState.componentInstanceSymbol, LayoutMode.NORMAL));
        testBounds(getTextBounds(document));
    }

    @Test
    public void testGetBounds() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String referenceTextTag = "<text x=\"7\" y=\"8\" font-size=\"10\">test</text>";
        String goodOtherTextTag = "<text x=\"25\" y=\"10\" font-size=\"10\">test</text>";
        String translationStartTag = "<g transform=\"translate(10 10)\">";
        String translationEndTag = "</g>";

        String goodSVGWithTranslation = startTag + referenceTextTag + translationStartTag + goodOtherTextTag + translationEndTag + endTag;

        SVGDocument document = loadSVGFromString(goodSVGWithTranslation);
        List<Bounds> bounds = getTextBounds(document);
        assertEquals(2, bounds.size());
        assertEquals(7, bounds.get(0).topLeft().x);
        assertEquals(8, bounds.get(0).topLeft().y);
        assertEquals(35, bounds.get(1).topLeft().x);
        assertEquals(20, bounds.get(1).topLeft().y);
    }

    @Test
    public void testNoOverlap() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String referenceTextTag = "<text x=\"7\" y=\"8\" font-size=\"10\">test</text>";
        String goodOtherTextTag = "<text x=\"25\" y=\"10\" font-size=\"10\">test</text>";

        String goodSVG = startTag + referenceTextTag + goodOtherTextTag + endTag;

        SVGDocument document = loadSVGFromString(goodSVG);
        testBounds(getTextBounds(document));
    }

    @Test
    public void testNoOverlapWithTranslation() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String referenceTextTag = "<text x=\"7\" y=\"8\" font-size=\"10\">test</text>";
        String badOtherTextTag = "<text x=\"15\" y=\"10\" font-size=\"10\">test</text>";
        String translationStartTag = "<g transform=\"translate(10 10)\">";
        String translationEndTag = "</g>";

        String goodSVGWithTranslation = startTag + referenceTextTag + translationStartTag + badOtherTextTag + translationEndTag + endTag;

        // Test should succeed if text does not overlap because one is translated
        SVGDocument document = loadSVGFromString(goodSVGWithTranslation);
        testBounds(getTextBounds(document));
    }

    /**
     * Tests if the test fails when two text fields overlap
     */
    @Test(expected = AssertionError.class)
    public void testOverlap() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String referenceTextTag = "<text x=\"7\" y=\"8\" font-size=\"10\">test</text>";
        String badOtherTextTag = "<text x=\"12\" y=\"13\" font-size=\"10\">test</text>";

        String badSVG = startTag + referenceTextTag + badOtherTextTag + endTag;

        SVGDocument document = loadSVGFromString(badSVG);
        testBounds(getTextBounds(document));
    }

    /**
     * Tests if the test fails when two text are translated to a position where they overlap
     */
    @Test(expected = AssertionError.class)
    public void testOverlapWithTranslation() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String referenceTextTag = "<text x=\"7\" y=\"8\" font-size=\"10\">test</text>";
        String badOtherTextTag = "<text x=\"15\" y=\"10\" font-size=\"10\">test</text>";
        String translationStartTag = "<g transform=\"translate(10 10)\">";
        String translationEndTag = "</g>";

        String badSVGWithTranslation = startTag + translationStartTag + referenceTextTag + translationEndTag + badOtherTextTag + endTag;

        SVGDocument document = loadSVGFromString(badSVGWithTranslation);
        testBounds(getTextBounds(document));
    }

    /**
     * Tests if the test fails when it's passed an invalid SVG file
     */
    @Test(expected = AssertionError.class)
    public void testBadFormedSVG() {
        String startTag = "<svg xmlns=\"http://www.w3.org/2000/svg\">";
        String endTag = "</svg>";

        String unwellFormedSVG = startTag + "<" + endTag;

        SVGDocument document = loadSVGFromString(unwellFormedSVG);
        testBounds(getTextBounds(document));
    }

    private void testBounds(List<Bounds> boundsList) {
        for (int i = 0; i < boundsList.size() - 1; i++) {
            for (int j = i + 1; j < boundsList.size(); j++) {
                checkIfBoundsOverlap(boundsList.get(i), boundsList.get(j));
            }
        }
    }

    private void checkIfBoundsOverlap(Bounds bounds, Bounds otherBounds) {
        assertFalse(bounds.overlapsWith(otherBounds));
        assertFalse(otherBounds.overlapsWith(bounds));
    }

    private RoutesLayoutState resolveAndDraw(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        return calculator.routesLayoutCalculator.layoutState;
    }

    private SVGDocument loadSVGFromString(String svgString) {
        try {
            String parser = XMLResourceDescriptor.getXMLParserClassName();
            SAXSVGDocumentFactory factory = new SAXSVGDocumentFactory(parser);
            return factory.createSVGDocument("", new StringReader(svgString));
        } catch (Exception e) {
            fail(e.getMessage());
        }

        return null;
    }

    private SVGDocument loadSVGFromFile(String fileName) {
        File file = new File(fileName);

        assertTrue(file.exists());

        URI localFileAsUri = file.toURI();
        String uri = localFileAsUri.toASCIIString();

        try {
            String parser = XMLResourceDescriptor.getXMLParserClassName();
            SAXSVGDocumentFactory f = new SAXSVGDocumentFactory(parser);
            f.createDocument(uri, new FileInputStream(file));
            return (SVGDocument) f.createDocument(uri);
        } catch (Exception e) {
            fail(e.getMessage());
        }

        return null;
    }

    private List<Bounds> getTextBounds(SVGDocument document) {
        List<Bounds> bounds = new ArrayList<>();

        NodeList nodes = document.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);

            if (child.hasChildNodes()) {
                bounds.addAll(getBounds(child, 0, 0));
            }
        }

        return bounds;
    }

    private List<Bounds> getBounds(Node node, int translateX, int translateY) {
        List<Bounds> boundsList = new ArrayList<>();

        NamedNodeMap attributes = node.getAttributes();
        Node transform = attributes.getNamedItem("transform");
        if (transform != null) {
            Point translate = parseTranslation(transform.getNodeValue());
            translateX += translate.x;
            translateY += translate.y;
        }

        String text = node.getTextContent().replace(" ", "").replace("\n", "");
        if (node.getNodeName().equals("text")) {
            int x = translateX + Integer.valueOf(attributes.getNamedItem("x").getNodeValue());
            int y = translateY + Integer.valueOf(attributes.getNamedItem("y").getNodeValue());
            int height = Integer.valueOf(attributes.getNamedItem("font-size").getNodeValue());
            int width = dimensions.getDimensionForText(text);

            boundsList.add(new Bounds(x, x + width, y, y + height));
        }

        NodeList nodes = node.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node child = nodes.item(i);

            if (child.hasChildNodes()) {
                boundsList.addAll(getBounds(child, translateX, translateY));
            }
        }

        return boundsList;
    }

    private Point parseTranslation(String transformString) {
        if (!transformString.contains("translate")) {
            return new Point(0, 0);
        }

        String[] transformAttributes = transformString.split("[\\(||\\)]");

        int x = 0;
        int y = 0;

        for (int i = 0; i < transformAttributes.length - 1; i++) {
            if (transformAttributes[i].equals("translate")) {
                String[] coordinates = transformAttributes[i + 1].split(" ");
                x = Integer.valueOf(coordinates[0]);
                y = Integer.valueOf(coordinates[1]);
                break;
            }
        }

        return new Point(x, y);
    }
}
