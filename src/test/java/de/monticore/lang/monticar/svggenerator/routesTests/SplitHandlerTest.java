/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.routesTests;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.RoutesUtils;
import de.monticore.lang.monticar.svggenerator.calculators.routes.DummyPort;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Peters Notebook on 10.11.2017.
 */
public class SplitHandlerTest {

    private MainCalculator calculator = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();
    private Node nodeA = new Node(null, "testA");
    private Node nodeB = new Node(null, "testB");
    private Node nodeC = new Node(null, "testC");
    private Node nodeD = new Node(null, "testD");
    private Node nodeE = new Node(null, "testE");
    private Node nodeF = new Node(null, "testF");

    @Before
    public void init() {
        nodeA.indexInOrder = 0;
        nodeB.indexInOrder = 1;
        nodeC.indexInOrder = 2;
        nodeD.indexInOrder = 3;
        nodeE.indexInOrder = 4;
        nodeF.indexInOrder = 5;
    }

    @Test
    public void testNormalSplit() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.split.clique", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        assertNotNull(instance);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        assertTrue(areNodesIntact(layoutState.routes));
    }

    @Test
    public void testSplitAbove() {
        List<Route> routes = new ArrayList<>();
        routes.add(getDummyRouteC());
        routes.add(getDummyRouteA(false));
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(true));

        RoutesLayoutState layoutState = new RoutesLayoutState();
        layoutState.routes = routes;

        assertFalse(areNodesIntact(routes));
//        SplitHandler.removeSplits(layoutState);
//        assertTrue(areNodesIntact(routes));
    }

    @Test
    public void testSplitBelow() {
        List<Route> routes = new ArrayList<>();
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(false));
        routes.add(getDummyRouteC());

        RoutesLayoutState layoutState = new RoutesLayoutState();
        layoutState.routes = routes;

        assertFalse(areNodesIntact(routes));
//        SplitHandler.removeSplits(layoutState);
//        assertTrue(areNodesIntact(routes));
    }

    @Test
    public void testOneSplitMultipleConnections() {
        List<Route> routes = new ArrayList<>();
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(false));
        routes.add(getDummyRouteC());
        routes.add(getDummyRouteC());

        RoutesLayoutState layoutState = new RoutesLayoutState();
        layoutState.routes = routes;

        assertFalse(areNodesIntact(routes));
//        SplitHandler.removeSplits(layoutState);
//        assertTrue(areNodesIntact(routes));
    }

    @Test
    public void testSplitsMultipleNodesInOneColumn() {
        List<Route> routes = new ArrayList<>();
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteA(false));
        routes.add(getDummyRouteA(true));
        routes.add(getDummyRouteB());

        RoutesLayoutState layoutState = new RoutesLayoutState();
        layoutState.routes = routes;

        assertFalse(areNodesIntact(routes));
//        SplitHandler.removeSplits(layoutState);
//        assertTrue(areNodesIntact(routes));
    }

    private boolean areNodesIntact(List<Route> routes) {
        int length = RoutesUtils.getColumnCount(routes);
        List<Node> visitedNodes = new ArrayList<>();

        for (int columnIndex = 0; columnIndex < length; columnIndex++) {
            Node currentNode = null;
            for (Route route: routes) {
                RouteState state = route.getState(columnIndex);

                if (state == RouteState.NODE) {
                    Node node = route.getNode(columnIndex);
                    if (node.isTemporary) {
                        continue;
                    }

                    if (currentNode != node && visitedNodes.contains(node)) {
                        return false;
                    }

                    currentNode = node;
                    visitedNodes.add(node);
                } else if (state == RouteState.ROUTE) {
                    currentNode = null;
                }
            }
        }

        return true;
    }

    private Route getDummyRouteA(boolean shouldContainCenterNode) {
        Route route = new Route();

        route.addNode(nodeA, null, null);
        route.addNode(nodeB, DummyPort.create(), DummyPort.create());

        if (shouldContainCenterNode) {
            route.addNode(nodeC, DummyPort.create(), DummyPort.create());
        }

        route.addNode(nodeD, DummyPort.create(), DummyPort.create());
        route.addNode(nodeE, DummyPort.create(), DummyPort.create());

        if (!shouldContainCenterNode) {
            route.expandAt(2);
        }

        return route;
    }

    private Route getDummyRouteB() {
        Route route = new Route();

        route.addNode(nodeA, null, null);
        route.addNode(nodeB, DummyPort.create(), DummyPort.create());
        route.addNode(nodeF, DummyPort.create(), DummyPort.create());
        route.addNode(nodeD, DummyPort.create(), DummyPort.create());
        route.addNode(nodeE, DummyPort.create(), DummyPort.create());

        return route;
    }

    private Route getDummyRouteC() {
        Route route = new Route();

        route.addNode(nodeA, null, null);
        route.addState(RouteState.ROUTE);
        route.addNode(nodeC, DummyPort.create(), DummyPort.create());
        route.addState(RouteState.ROUTE);
        route.addNode(nodeE, DummyPort.create(), DummyPort.create());

        return route;
    }
}
