/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Checks if input (output) ports are drawn on the left (right) side of the respective component
 */
public class TestR5Ports {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkEnclosingPortPositions(layoutState.componentInstanceSymbol);
        layoutState.componentInstanceSymbol.getSubComponents().forEach(this::checkInternalPortPositions);
    }

    @Test
    public void testBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkEnclosingPortPositions(layoutState.componentInstanceSymbol);
        layoutState.componentInstanceSymbol.getSubComponents().forEach(this::checkInternalPortPositions);
    }

    @Test
    public void testDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkEnclosingPortPositions(layoutState.componentInstanceSymbol);
        layoutState.componentInstanceSymbol.getSubComponents().forEach(this::checkInternalPortPositions);
    }

    @Test
    public void testBackConnectorBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.backConnectorBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkEnclosingPortPositions(layoutState.componentInstanceSymbol);
        layoutState.componentInstanceSymbol.getSubComponents().forEach(this::checkInternalPortPositions);
    }

    @Test
    public void testConnectedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.connectedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkEnclosingPortPositions(layoutState.componentInstanceSymbol);
        layoutState.componentInstanceSymbol.getSubComponents().forEach(this::checkInternalPortPositions);
    }

    /**
     * Tests if the test fails when an input port of the enclosing component is not positioned correctly
     */
    @Test(expected=AssertionError.class)
    public void testIncorrectCanvasInputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        CanvasLayoutSymbol canvasLayoutSymbol = new CanvasLayoutSymbol(0, 0, 0, 420, 230, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol badInputPortSymbol = new CanvasPortLayoutSymbol(0, 4 * portOffset, 0, LayoutMode.NORMAL);

        checkEnclosingPortPosition(canvasLayoutSymbol, badInputPortSymbol, true);
    }

    /**
     * Tests if the test fails when an output port of the enclosing component is not positioned correctly
     */
    @Test(expected=AssertionError.class)
    public void testIncorrectCanvasOutputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        CanvasLayoutSymbol canvasLayoutSymbol = new CanvasLayoutSymbol(0, 0, 0, 420, 230, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol badOutputPortSymbol = new CanvasPortLayoutSymbol(0, 420 + 4 * portOffset, 0, LayoutMode.NORMAL);

        checkEnclosingPortPosition(canvasLayoutSymbol, badOutputPortSymbol, false);
    }

    /**
     * Tests if the test fails when an input port of an inner component is not positioned correctly
     */
    @Test(expected=AssertionError.class)
    public void testInputPassedAsOutputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        CanvasLayoutSymbol canvasLayoutSymbol = new CanvasLayoutSymbol(0, 0, 0, 420, 230, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol goodInputPortSymbol = new CanvasPortLayoutSymbol(0, portOffset, 0, LayoutMode.NORMAL);

        checkEnclosingPortPosition(canvasLayoutSymbol, goodInputPortSymbol, false);
    }

    /**
     * Tests if the test fails when an output port of an inner component is not positioned correctly
     */
    @Test(expected=AssertionError.class)
    public void testOutputPassedAsInputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        CanvasLayoutSymbol canvasLayoutSymbol = new CanvasLayoutSymbol(0, 0, 0, 420, 230, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol goodOutputPortSymbol = new CanvasPortLayoutSymbol(0, 420 + portOffset, 0, LayoutMode.NORMAL);

        checkEnclosingPortPosition(canvasLayoutSymbol, goodOutputPortSymbol, true);
    }

    @Test
    public void testCorrectCanvasPorts() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        CanvasLayoutSymbol canvasLayoutSymbol = new CanvasLayoutSymbol(0, 0, 0, 420, 230, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol goodInputPortSymbol = new CanvasPortLayoutSymbol(0, portOffset, 0, LayoutMode.NORMAL);
        CanvasPortLayoutSymbol goodOutputPortSymbol = new CanvasPortLayoutSymbol(0, 420 + portOffset, 0, LayoutMode.NORMAL);

        // Test should succeed if ports are positioned correctly
        checkEnclosingPortPosition(canvasLayoutSymbol, goodInputPortSymbol, true);
        checkEnclosingPortPosition(canvasLayoutSymbol, goodOutputPortSymbol, false);
    }

    @Test(expected=AssertionError.class)
    public void testIncorrectInternalInputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 420, 230, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        PortLayoutSymbol badInputPortSymbol = new PortLayoutSymbol(0, 4 * portOffset, 0, true, LayoutMode.NORMAL);

        checkInternalPortPosition(componentLayoutSymbol, badInputPortSymbol, true);
    }

    @Test(expected=AssertionError.class)
    public void testIncorrectInternalOutputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 420, 230, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        PortLayoutSymbol badOutputPortSymbol = new PortLayoutSymbol(0, 420 + 4 * portOffset, 0, true, LayoutMode.NORMAL);

        checkInternalPortPosition(componentLayoutSymbol, badOutputPortSymbol, false);
    }

    @Test(expected=AssertionError.class)
    public void testInternalInputPassedAsOutputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 420, 230, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        PortLayoutSymbol goodInputPortSymbol = new PortLayoutSymbol(0, portOffset, 0, true, LayoutMode.NORMAL);

        checkInternalPortPosition(componentLayoutSymbol, goodInputPortSymbol, false);
    }

    @Test(expected=AssertionError.class)
    public void testInternalOutputPassedAsInputPort() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 420, 230, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        PortLayoutSymbol goodOutputPortSymbol = new PortLayoutSymbol(0, 420 + portOffset, 0, true, LayoutMode.NORMAL);

        checkInternalPortPosition(componentLayoutSymbol, goodOutputPortSymbol, true);
    }

    @Test
    public void testCorrectInternalPorts() {
        int portOffset = (int) -(DrawingConstants.PORT_WIDTH / 2.0);

        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 420, 230, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        PortLayoutSymbol goodInputPortSymbol = new PortLayoutSymbol(0, portOffset, 0, true, LayoutMode.NORMAL);
        PortLayoutSymbol goodOutputPortSymbol = new PortLayoutSymbol(0, 420 + portOffset, 0, true, LayoutMode.NORMAL);

        // Test should succeed if ports are positioned correctly
        checkInternalPortPosition(componentLayoutSymbol, goodInputPortSymbol, true);
        checkInternalPortPosition(componentLayoutSymbol, goodOutputPortSymbol, false);
    }

    private void checkInternalPortPositions(ExpandedComponentInstanceSymbol component) {
        Optional<ComponentLayoutSymbol> canvasLayoutSymbol = Tags.getFirstLayoutTag(component, LayoutMode.NORMAL);

        // Assure the component has been drawn
        assertTrue(canvasLayoutSymbol.isPresent());

        Collection<PortSymbol> ports = component.getPorts();

        if (ports.isEmpty()) {
            return;
        }

        for (PortSymbol portSymbol : ports) {
            Optional<PortLayoutSymbol> portLayoutSymbol = Tags.getFirstLayoutTag(portSymbol, LayoutMode.NORMAL);

            // Assure the port has been drawn
            assertTrue(portLayoutSymbol.isPresent());

            checkInternalPortPosition(canvasLayoutSymbol.get(), portLayoutSymbol.get(), portSymbol.isIncoming());
        }
    }

    private void checkInternalPortPosition(ComponentLayoutSymbol componentLayoutSymbol, PortLayoutSymbol portLayoutSymbol, boolean isInput) {
        int componentLeft = componentLayoutSymbol.getX();
        int componentRight = componentLeft + componentLayoutSymbol.getWidth();

        int portCenterX = portLayoutSymbol.getX() + (int) (DrawingConstants.PORT_WIDTH / 2.0);

        // Check if port is at the right side of the component
        if (isInput) {
            assertTrue(portCenterX == componentLeft);
        } else {
            assertTrue(portCenterX == componentRight);
        }
    }

    private void checkEnclosingPortPositions(ExpandedComponentInstanceSymbol component) {
        Optional<CanvasLayoutSymbol> canvasLayoutSymbol = Tags.getFirstCanvasLayoutTag(component, LayoutMode.NORMAL);

        // Assure the component has been drawn
        assertTrue(canvasLayoutSymbol.isPresent());

        Collection<PortSymbol> ports = component.getPorts();

        if (ports.isEmpty()) {
            return;
        }

        for (PortSymbol portSymbol : ports) {
            Optional<CanvasPortLayoutSymbol> portLayoutSymbol = Tags.getFirstCanvasLayoutTag(portSymbol, LayoutMode.NORMAL);

            // Assure the port has been drawn
            assertTrue(portLayoutSymbol.isPresent());

            checkEnclosingPortPosition(canvasLayoutSymbol.get(), portLayoutSymbol.get(), portSymbol.isIncoming());
        }
    }

    private void checkEnclosingPortPosition(CanvasLayoutSymbol canvasLayoutSymbol, CanvasPortLayoutSymbol portLayoutSymbol, Boolean isInputPort) {
        int componentLeft = canvasLayoutSymbol.getX();
        int componentRight = componentLeft + canvasLayoutSymbol.getWidth();

        int portCenterX = portLayoutSymbol.getX() + (int) (DrawingConstants.PORT_WIDTH / 2.0);

        // Check if port is at the right side of the component
        if (isInputPort) {
            assertTrue(portCenterX == componentLeft);
        } else {
            assertTrue(portCenterX == componentRight);
        }
    }
}
