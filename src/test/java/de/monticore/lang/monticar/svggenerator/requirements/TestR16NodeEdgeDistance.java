/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LayoutPosition;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.*;

public class TestR16NodeEdgeDistance {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnections() {
        testModel("testManuelSchrick.noBends");
    }

    @Test
    public void testSplit() {
        testModel("testManuelSchrick.k33.k33");
    }

    @Test
    public void testBackConnector() {
        testModel("testManuelSchrick.thesisExample.thesisExample");
    }

    @Test
    public void testBus() {
        testModel("testManuelSchrick.bus.normalBus");
    }

    @Test
    public void testDecomposedBus() {
        testModel("testManuelSchrick.bus.decomposedBus");
    }

    @Test
    public void testBackConnectorBus() {
        testModel("testManuelSchrick.bus.backConnectorBus");
    }

    @Test
    public void testConnectedBus() {
        testModel("testManuelSchrick.bus.connectedBus");
    }

    /**
     * Tests if lines that are above or below a given component are recognized as such
     */
    @Test
    public void testIsAboveOrBelow() {
        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 5, 5, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        LineSymbol lineTop = new LineSymbol(new Point(-1, -1), new Point(6, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol lineBottom = new LineSymbol(new Point(-1, 6), new Point(6, 6), 0, LayoutMode.NORMAL, false);

        assertTrue(isAboveOrBelow(lineTop, componentLayoutSymbol));
        assertTrue(isAboveOrBelow(lineBottom, componentLayoutSymbol));
    }

    /**
     * Tests if a line that is located to the left of a component is recognized
     * as one that is neither above nor below that component
     */
    @Test(expected = AssertionError.class)
    public void testLineLeft() {
        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 5, 5, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        LineSymbol lineLeft = new LineSymbol(new Point(-6, 2), new Point(-1, 2), 0, LayoutMode.NORMAL, false);

        assertTrue(isAboveOrBelow(lineLeft, componentLayoutSymbol));
    }

    /**
     * Tests if a line that is located to the right of a component is recognized
     * as one that is neither above nor below that component
     */
    @Test(expected = AssertionError.class)
    public void testLineRight() {
        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 5, 5, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        LineSymbol lineRight = new LineSymbol(new Point(6, 2), new Point(11, 2), 0, LayoutMode.NORMAL, false);

        assertTrue(isAboveOrBelow(lineRight, componentLayoutSymbol));
    }

    /**
     * Tests if the distance between a given component and a connector is calculated correctly
     */
    @Test
    public void testDistance() {
        ComponentLayoutSymbol componentLayoutSymbol = new ComponentLayoutSymbol(0, 0, 0, 5, 5, LayoutPosition.UNDEFINED, LayoutMode.NORMAL);
        LineSymbol lineTop = new LineSymbol(new Point(-1, -1), new Point(6, -1), 0, LayoutMode.NORMAL, false);
        LineSymbol lineBottom = new LineSymbol(new Point(-1, 6), new Point(6, 6), 0, LayoutMode.NORMAL, false);

        assertEquals(1, getDistance(lineTop, componentLayoutSymbol));
        assertEquals(1, getDistance(lineBottom, componentLayoutSymbol));
    }

    private void testModel(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        Collection<ExpandedComponentInstanceSymbol> components = layoutState.componentInstanceSymbol.getSubComponents();
        Collection<ConnectorSymbol> connectors = layoutState.componentInstanceSymbol.getConnectors();

        for (ExpandedComponentInstanceSymbol component : components) {
            for (ConnectorSymbol connector : connectors) {
                testConnector(connector, component);
            }
        }
    }

    private void testConnector(ConnectorSymbol connector, ExpandedComponentInstanceSymbol component) {
        Optional<ComponentLayoutSymbol> componentLayoutSymbol = Tags.getFirstLayoutTag(component, LayoutMode.NORMAL);
        Collection<LineSymbol> lineSymbols = Tags.getLineSymbols(connector, LayoutMode.NORMAL);
        int minDistance = (int) (DrawingConstants.ROW_HEIGHT / 2.0);

        assertTrue(componentLayoutSymbol.isPresent());
        assertFalse(lineSymbols.isEmpty());

        for (LineSymbol lineSymbol : lineSymbols) {
            if (lineSymbol.isVertical()) {
                continue;
            } else if (!isAboveOrBelow(lineSymbol, componentLayoutSymbol.get())) {
                continue;
            }

            int distance = getDistance(lineSymbol, componentLayoutSymbol.get());
            assertTrue(distance >= minDistance);
        }
    }

    private int getDistance(LineSymbol lineSymbol, ComponentLayoutSymbol componentLayoutSymbol) {
        int compTop = componentLayoutSymbol.getY();
        int compBottom = compTop + componentLayoutSymbol.getHeight();
        int lineY = lineSymbol.getStart().y;

        int distanceTop = Math.abs(compTop - lineY);
        int distanceBottom = Math.abs(compBottom - lineY);

        return Math.min(distanceBottom, distanceTop);
    }

    private boolean isAboveOrBelow(LineSymbol lineSymbol, ComponentLayoutSymbol componentLayoutSymbol) {
        assertTrue(lineSymbol.isHorizontal());

        int compTop = componentLayoutSymbol.getY();
        int compBottom = compTop + componentLayoutSymbol.getHeight();
        int compLeft = componentLayoutSymbol.getX();
        int compRight = compLeft + componentLayoutSymbol.getWidth();

        int lineY = lineSymbol.getStart().y;
        int lineStartX = lineSymbol.getStart().x;
        int lineEndX = lineSymbol.getEnd().x;
        int lineLeft = Math.min(lineStartX, lineEndX);
        int lineRight = Math.max(lineStartX, lineEndX);

        if (lineY > compTop && lineY < compBottom) {
            return false;
        }

        return lineLeft < compRight && lineRight > compLeft;
    }

}
