/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.utils.Bounds;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestR9ComponentsInsideBounds {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.k33.k33");
        checkIfSubcomponentsAreInsideCanvas(layoutState);
    }

    @Test
    public void testBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.normalBus");
        checkIfSubcomponentsAreInsideCanvas(layoutState);
    }

    @Test
    public void testDecomposedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.decomposedBus");
        checkIfSubcomponentsAreInsideCanvas(layoutState);
    }

    @Test
    public void testBackConnectorBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.backConnectorBus");
        checkIfSubcomponentsAreInsideCanvas(layoutState);
    }

    @Test
    public void testConnectedBus() {
        RoutesLayoutState layoutState = resolveAndDraw("testManuelSchrick.bus.connectedBus");
        checkIfSubcomponentsAreInsideCanvas(layoutState);
    }

    @Test
    public void testFullyInside() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds fullyInside = new Bounds(10, 90, 10, 90);

        checkBounds(outerBounds, fullyInside);
    }

    /**
     * Tests if the test fails if a component reaches out of the canvas in the bottom left corner
     */
    @Test(expected = AssertionError.class)
    public void testReachOutBottomLeft() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingA = new Bounds(-50, 50, 50, 150);

        checkBounds(outerBounds, partiallyOverlappingA);
    }

    /**
     * Tests if the test fails if a component reaches out of the canvas in the top left corner
     */
    @Test(expected = AssertionError.class)
    public void testReachOutTopLeft() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingB = new Bounds(-50, 50, -50, 50);

        checkBounds(outerBounds, partiallyOverlappingB);
    }

    /**
     * Tests if the test fails if a component reaches out of the canvas in the top right corner
     */
    @Test(expected = AssertionError.class)
    public void testReachOutTopRight() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingC = new Bounds(50, 150, 50, 150);

        checkBounds(outerBounds, partiallyOverlappingC);
    }

    /**
     * Tests if the test fails if a component reaches out of the canvas in the bottom right corner
     */
    @Test(expected = AssertionError.class)
    public void testReachOutBottomRight() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds partiallyOverlappingD = new Bounds(50, 150, -50, 50);

        checkBounds(outerBounds, partiallyOverlappingD);
    }

    /**
     * Tests if the test fails if a component is larger than the canvas
     */
    @Test(expected = AssertionError.class)
    public void testInsideOut() {
        Bounds outerBounds = new Bounds(0, 100, 0, 100);
        Bounds fullyInside = new Bounds(10, 90, 10, 90);

        checkBounds(fullyInside, outerBounds);
    }

    private void checkIfSubcomponentsAreInsideCanvas(RoutesLayoutState layoutState) {
        Bounds canvasBounds = new Bounds(layoutState.componentInstanceSymbol);
        List<Bounds> bounds = layoutState.componentInstanceSymbol.getSubComponents().stream()
                .map(Bounds::new)
                .collect(Collectors.toList());

        for (int i = 0; i < bounds.size(); i++) {
            checkBounds(canvasBounds, bounds.get(i));
        }
    }

    private void checkBounds(Bounds outerBounds, Bounds innerBounds) {
        assertTrue(outerBounds.contains(innerBounds.topLeft()));
        assertTrue(outerBounds.contains(innerBounds.topRight()));
        assertTrue(outerBounds.contains(innerBounds.bottomLeft()));
        assertTrue(outerBounds.contains(innerBounds.bottomRight()));
    }

    private RoutesLayoutState resolveAndDraw(String modelName) {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(modelName, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        return calculator.routesLayoutCalculator.layoutState;
    }

}
