/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.ModelPath;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.InstancingRegister;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._symboltable.EmbeddedMontiArcMathLanguage;
import de.monticore.lang.monticar.struct._symboltable.StructLanguage;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Random;

/**
 * Offers testing utilities such as creating a sym tab
 * Created by Kilian on 11/7/2016.
 */
public class TestingUtilities {
    public static String filterString(String inputString, String deletedCharacters) {
        String filteredString = new String();
        for (int i = 0; i < inputString.length(); i++) {
            if (!deletedCharacters.contains("" + inputString.charAt(i))) {
                // Character at position i is not in the list of characters to be deleted.
                filteredString += inputString.charAt(i);
            }
        }
        return filteredString;
    }

    public static String generateRandomString(Random random, int length) {
        String characters = "abcdefghijklmnopqrstuvwxyz";
        String listOfChars = "0123456789" + characters + characters.toUpperCase();

        String result = new String();
        for (int i = 0; i < length; i++) {
            result += listOfChars.charAt(
                    random.nextInt(listOfChars.length())); // Add random character from listOfChars
        }
        return result;
    }

    public static int getCountOf(String outerString, String subString) {
        return (outerString.length() - outerString.replace(subString, "").length()) / subString.length();
    }

    // Create Symbol Table
    public static Scope createSymTab(String modelPath) {
        ModelingLanguageFamily fam = new ModelingLanguageFamily();
        fam.addModelingLanguage(new EmbeddedMontiArcLanguage());
        fam.addModelingLanguage(new EmbeddedMontiArcMathLanguage());
        fam.addModelingLanguage(new StructLanguage());
        final ModelPath mp = new ModelPath();
        mp.addEntry(Paths.get(modelPath));

        GlobalScope scope = new GlobalScope(mp, fam);

        de.monticore.lang.monticar.Utils.addBuiltInTypes(scope);

        InstancingRegister.reset();
        Tags.initTaggingResolver(scope, modelPath);
        return scope;
    }

    public static String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

    public static String readFile(String path) {
        try {
            return readFile(path, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void drawComponentsAndSubComponents(ExpandedComponentInstanceSymbol instance, int maxDepth, MainCalculator cal, SVGGenerator svgGenerator) {
        if (maxDepth <= 0 || instance.getSubComponents().isEmpty()) {
            return;
        }

        try {
            // Calculate Layout information for the given input
            cal.calculateLayout(instance);

            // Draw the layout (create output SVG file)
            svgGenerator.drawLayouts(instance, cal.routesLayoutCalculator.layoutState);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collection<ExpandedComponentInstanceSymbol> subComponents = instance.getSubComponents();
        for (ExpandedComponentInstanceSymbol symbol : subComponents) {
            drawComponentsAndSubComponents(symbol, maxDepth - 1, cal, svgGenerator);
        }
    }
}
