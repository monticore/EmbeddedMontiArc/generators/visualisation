/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.templatetests;

import de.monticore.lang.monticar.svggenerator.LogConfig;
import de.monticore.lang.monticar.svggenerator.ViewModel.*;
import de.monticore.lang.monticar.svggenerator.ViewModel.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;

//  de.monticore.lang.monticar.svggenerator.templatetests
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class ViewModelTest {


    @BeforeClass
    public static void init(){
        LogConfig.init();
    }

    @Test
    public void testComponentViewModel() {
        HashMap<String, Object> expected = new HashMap<>();
        expected.put("X", 1);
        expected.put("Y", 2);
        expected.put("id", 3);
        expected.put("height", 4);
        expected.put("width", 5);
        expected.put("name", "Name");
        expected.put("fullName", "Fullname");
        expected.put("isEmpty", false);
        expected.put("fileNameSuffix", "someSuffix");
        expected.put("idePath", "some/path.ema");

        ComponentViewModel viewModel = new ComponentViewModel();
        viewModel.x = 1;
        viewModel.y = 2;
        viewModel.id = 3;
        viewModel.height = 4;
        viewModel.width = 5;
        viewModel.name = "Name";
        viewModel.fullname = "Fullname";
        viewModel.isEmpty = false;
        viewModel.fileNameSuffix = "someSuffix";
        viewModel.idePath = "some/path.ema";

        Assert.assertEquals(expected, viewModel.hashMap());

    }

    @Test
    public void testPortViewModel() {
        // Ports need some global definition and port specific definition
        // This test cares only about the port specific information

        HashMap<String, Object> expected = new HashMap<>();
        expected.put("X", 1);
        expected.put("Y", 2);
        expected.put("anchor", "start");
        expected.put("angle", 3);
        expected.put("portname", "Portname");

        PortViewModel viewModel = new PortViewModel();
        viewModel.x = 1;
        viewModel.y = 2;
        viewModel.anchor = "start";
        viewModel.angle = 3;
        viewModel.portname = "Portname";

        Assert.assertEquals(expected, viewModel.hashMap());
    }

    @Test
    public void testCanvasViewModel() {
        HashMap<String, Object> expected = new HashMap<>();
        expected.put("X", 1);
        expected.put("Y", 2);
        expected.put("id", 3);
        expected.put("height", 4);
        expected.put("width", 5);
        expected.put("name", "Name");

        CanvasViewModel viewModel = new CanvasViewModel();
        viewModel.x = 1;
        viewModel.y = 2;
        viewModel.id = 3;
        viewModel.height = 4;
        viewModel.width = 5;
        viewModel.name = "Name";

        Assert.assertEquals(expected, viewModel.hashMap());
    }

    @Test
    public void testConnectorViewModel() {
        HashMap<String, Object> expected = new HashMap<>();
        String color1 = "1,1,1";
        String color2 = "2,2,2";
        HashSet<String> expectedColors = new HashSet<>(2);
        expectedColors.add(color1);
        expectedColors.add(color2);
        expected.put("colors", expectedColors.toArray());

        ConnectorViewModel viewModel = new ConnectorViewModel();
        viewModel.colorSet = expectedColors;

        Assert.assertEquals(expected.keySet(), viewModel.hashMap().keySet());
        Assert.assertArrayEquals((Object[])expected.get("colors"), (Object[]) viewModel.hashMap().get
                ("colors"));
    }

    @Test
    public void testLineViewModel() {
        HashMap<String, Object> expected = new HashMap<>();
        expected.put("X", 1);
        expected.put("Y", 2);
        expected.put("XX", 3);
        expected.put("YY", 4);
        expected.put("red", 5);
        expected.put("green", 6);
        expected.put("blue", 7);
        expected.put("linewidth", 8);
        expected.put("isEnd", true);

        LineViewModel viewModel = new LineViewModel();
        viewModel.x = 1;
        viewModel.y = 2;
        viewModel.xx = 3;
        viewModel.yy = 4;
        viewModel.red = 5;
        viewModel.green = 6;
        viewModel.blue = 7;
        viewModel.linewidth = 8;
        viewModel.isEnd = true;

        Assert.assertEquals(expected, viewModel.hashMap());
    }
}
