/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.utils;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasPortLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class Bounds {

    private int top;
    private int left;
    private int right;
    private int bottom;

    public Bounds(ExpandedComponentInstanceSymbol component) {
        Optional<ComponentLayoutSymbol> layoutSymbol = Tags.getFirstLayoutTag(component, LayoutMode.NORMAL);
        Optional<CanvasLayoutSymbol> canvasLayoutSymbol = Tags.getFirstCanvasLayoutTag(component, LayoutMode.NORMAL);

        assertTrue(layoutSymbol.isPresent() || canvasLayoutSymbol.isPresent());

        if(canvasLayoutSymbol.isPresent()) {
            top = canvasLayoutSymbol.get().getY();
            left = canvasLayoutSymbol.get().getX();
            right = left + canvasLayoutSymbol.get().getWidth();
            bottom = top + canvasLayoutSymbol.get().getHeight();
        } else {
            top = layoutSymbol.get().getY();
            left = layoutSymbol.get().getX();
            right = left + layoutSymbol.get().getWidth();
            bottom = top + layoutSymbol.get().getHeight();
        }
    }

    public Bounds(PortSymbol portSymbol) {
        Optional<PortLayoutSymbol> layoutSymbol = Tags.getFirstLayoutTag(portSymbol, LayoutMode.NORMAL);
        Optional<CanvasPortLayoutSymbol> canvasLayoutSymbol = Tags.getFirstCanvasLayoutTag(portSymbol, LayoutMode.NORMAL);

        assertTrue(layoutSymbol.isPresent() || canvasLayoutSymbol.isPresent());

        if(canvasLayoutSymbol.isPresent()) {
            top = canvasLayoutSymbol.get().getY();
            left = canvasLayoutSymbol.get().getX();
        } else {
            top = layoutSymbol.get().getY();
            left = layoutSymbol.get().getX();
        }

        bottom = top + DrawingConstants.PORT_HEIGHT;
        right = left + DrawingConstants.PORT_WIDTH;
    }

    public Bounds(int left, int right, int top, int bottom) {
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
    }

    public boolean overlapsWith(Bounds otherBounds) {
        if (contains(otherBounds.topLeft())) {
            return true;
        }
        if (contains(otherBounds.topRight())) {
            return true;
        }
        if (contains(otherBounds.bottomLeft())) {
            return true;
        }
        if (contains(otherBounds.bottomRight())) {
            return true;
        }

        return false;
    }

    public boolean contains(Point point) {
        return (point.x > left && point.x < right) && (point.y > top && point.y < bottom);
    }

    public Point topLeft() {
        return new Point(left, top);
    }

    public Point topRight() {
        return new Point(right, top);
    }

    public Point bottomLeft() {
        return new Point(left, bottom);
    }

    public Point bottomRight() {
        return new Point(right, bottom);
    }

    @Override
    public String toString() {
        return "Bounds( " + topLeft() + ", " + bottomRight() + " )";
    }
}
