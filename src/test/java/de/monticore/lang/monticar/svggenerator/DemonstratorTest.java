/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.symboltable.Scope;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.Assert.assertNotNull;

/**
 * Created by hendrik, eugen, kilianm on 18.01.2017.
 */
public class DemonstratorTest {

    private MainCalculator cal = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();

    @BeforeClass
    public static void init() {
        LogConfig.init();
    }

    @Test
    public void test() {
//        helpSVGBuild("fas.demo_fas_Fkt_m.fAS", 47, true);
//        helpSVGBuild("fas.demo_fas_Fkt_m.emergencyBrake_m.emergencyBrake_Function", 0, true);
//        helpSVGBuild("fas.demo_fas_Fkt_m.velocityControl", 0, true);
//        helpSVGBuild("testManuelSchrick.bumperBot", 17, true);
//        helpSVGBuild("testManuelSchrick.bus.normalBus", 17, true);
//        helpSVGBuild("testManuelSchrick.bumperBotEmergency", 17, true);
//        helpSVGBuild("testManuelSchrick.worldGraph.worldGraph");
//        helpSVGBuild("testManuelSchrick.split.clique", 17, true);
//        helpSVGBuild("testManuelSchrick.thesisExample.thesisExample", 17, true);
//        helpSVGBuild("parkassistant.parkAssistant", 17, true);
//        helpSVGBuild("testManuelSchrick.autopilot.autopilot", 17, true);
//        helpSVGBuild("testManuelSchrick.autopilot.autopilot.motionPlanning.engineAndBrakes", 0);
//        helpSVGBuild("testManuelSchrick.autopilot.autopilot.behaviorGeneration.calcMotionCmds", 0);

//        SVGMain.main(new String[]{"--input", "testManuelSchrick.testA", "--modelPath", "src/test/resources/", "--recursiveDrawing", "true"});
//        SVGMain.main(new String[]{"--input", "testManuelSchrick.testA", "--modelPath", "C:/svggen/arc/", "--recursiveDrawing", "true"});
//        SVGMain.main(new String[]{"--input", "testManuelSchrick.bus.normalBus", "--modelPath", "src/test/resources/", "--recursiveDrawing", "true"});
    }


    public void helpSVGBuild(String testCase, int maxDepth, boolean redrawComponents) {
        System.out.println("Start Building SVG File: " + testCase);

        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve(testCase, ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(instance);

        boolean skip = false;

        if (!checkInstance(instance)) {
            skip = true;
        }

        String fileName = svgGenerator.getSvgFileName(instance, LayoutMode.NORMAL);
        File file = new File(fileName);
        if (file.exists() && !redrawComponents) {
            System.out.println("Skipping " + instance.getFullName() + ": Layout file exists");
            skip = true;
        }

        long startTime = System.currentTimeMillis();
        try {
            if (!skip) {
                // Calculate Layout information for the given input
                cal.calculateLayout(instance);

                // Draw the layout (create output SVG file)
                svgGenerator.drawLayouts(instance, cal.routesLayoutCalculator.layoutState);
            }

            drawSubComponents(instance, maxDepth, redrawComponents);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void drawSubComponents(ExpandedComponentInstanceSymbol instanceSymbol, int maxDepth, boolean redrawComponents) throws Exception {
        if (maxDepth <= 0) {
            System.out.println("maxDepth is " + maxDepth);
            return;
        }

        Collection<ExpandedComponentInstanceSymbol> subComponents = instanceSymbol.getSubComponents();
        for (ExpandedComponentInstanceSymbol subComponent : subComponents) {
            if (subComponent.getSubComponents().isEmpty()) {
                continue;
            }

            boolean skip = false;

            if (!checkInstance(subComponent)) {
                skip = true;
            }

            String fileName = svgGenerator.getSvgFileName(subComponent, LayoutMode.NORMAL);
            File file = new File(fileName);
            if (file.exists() && !redrawComponents) {
                System.out.println("Skipping " + subComponent.getFullName() + ": Layout file exists");
                skip = true;
            }

            if (!skip) {
//                System.out.println("drawing component: " + subComponent.getFullName());

                assertNotNull(subComponent);

                // Calculate Layout information for the given input
                cal.calculateLayout(subComponent);

                // Draw the layout (create output SVG file)
                svgGenerator.drawLayouts(subComponent, cal.routesLayoutCalculator.layoutState);
            }

            drawSubComponents(subComponent, maxDepth - 1, redrawComponents);
        }
    }

    private boolean checkInstance(ExpandedComponentInstanceSymbol instance) {
        HashMap<String, ConnectorSymbol> connectorsToTargetPort = new HashMap<>();

        for (ConnectorSymbol con: instance.getConnectors()) {
//            System.out.println(con.getSource() + " -> " + con.getTarget());
//            System.out.println("ports:");
//            System.out.println(con.getSourcePort().getName() + " -> " + con.getTargetPort().getName());
//            System.out.println(con.getSourcePort().getFullName() + " -> " + con.getTargetPort().getFullName());

            PortSymbol targetPort = con.getTargetPort();

            if (connectorsToTargetPort.containsKey(targetPort.getFullName())) {
                System.out.println("port " + targetPort.getFullName() + " has multiple inputs");
                Assert.fail();
            }

            ExpandedComponentInstanceSymbol source = con.getSourcePort().getComponentInstance().get();
            ExpandedComponentInstanceSymbol target = con.getTargetPort().getComponentInstance().get();

//            System.out.println("source & target");
//            System.out.println(source.getFullName() + " -> " + target.getFullName() + "\n");

            if (source.getFullName().equals(target.getFullName())) {
                return false;
            }
        }

        return true;
    }

    private void printSize(ExpandedComponentInstanceSymbol instance) {
        int instanceCount = countInstances(instance);
        int connectorCount = countConnectors(instance);
        int maxDepth = getMaxDepth(instance);

        System.out.println("size:");
        System.out.println("  " + instanceCount + " instances");
        System.out.println("  " + connectorCount + " connectors");
        System.out.println("  " + maxDepth + " max depth");
    }

    private int countInstances(ExpandedComponentInstanceSymbol instance) {
        return 1 + instance.getSubComponents().stream().mapToInt(this::countInstances).sum();
    }

    private int countConnectors(ExpandedComponentInstanceSymbol instance) {
        int count = instance.getConnectors().size();

        return count + instance.getSubComponents().stream().mapToInt(this::countConnectors).sum();
    }

    private int getMaxDepth(ExpandedComponentInstanceSymbol instance) {
        return 1 + instance.getSubComponents().stream().mapToInt(this::getMaxDepth).max().orElse(-1);
    }
}
