/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.OptimalRouteOrderCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 */
public class BaseLayoutTest {

    private ExpandedComponentInstanceSymbol instance;
    private SVGGenerator svgGenerator = new SVGGenerator();

    @Before
    public void init() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);
    }

    @Test
    public void testBaseLayoutFeaturesSimulatedAnnealing() {
        assertNotNull(instance);

        MainCalculator calculator = new MainCalculator();
        TestingUtilities.drawComponentsAndSubComponents(instance, 7, calculator, svgGenerator);

        checkIfEverythingWasDrawn(instance);
    }

    @Test
    public void testBaseLayoutFeaturesOptimal() {
        MainCalculator calculator = new MainCalculator(new OptimalRouteOrderCalculator());

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);

        checkIfEverythingWasDrawn(instance);
    }

    private void checkIfEverythingWasDrawn(ExpandedComponentInstanceSymbol instance) {
        List<ExpandedComponentInstanceSymbol> instancesToCheck = new ArrayList<>();
        instancesToCheck.addAll(instance.getSubComponents());

        while (!instancesToCheck.isEmpty()) {
            ExpandedComponentInstanceSymbol inst = instancesToCheck.remove(0);
            boolean hasSubComponents = !inst.getSubComponents().isEmpty();

//          Check if component was is drawn
            assertTrue(Tags.hasTags(inst, ComponentLayoutSymbol.KIND));
            if (hasSubComponents) {
                assertTrue(Tags.hasTags(inst, CanvasLayoutSymbol.KIND));
            }

//          Check if all ports were drawn
            inst.getPorts().forEach(port -> {
                assertTrue(Tags.hasTags(port, PortLayoutSymbol.KIND));

                if (hasSubComponents) {
                    assertTrue(Tags.hasTags(port, CanvasPortLayoutSymbol.KIND));
                }
            });

//          Check if all connectors were drawn
            inst.getConnectors().forEach(connector -> assertTrue(Tags.hasTags(connector, LineSymbol.KIND)));

            Collection<ExpandedComponentInstanceSymbol> subComponents = inst.getSubComponents();
            if (!subComponents.isEmpty()) {
                instancesToCheck.addAll(inst.getSubComponents());

//              If the component has subcomponents, there has to be a CanvasLayoutSymbolPresent
                assertTrue(Tags.hasTags(inst, CanvasLayoutSymbol.KIND));
            }
        }
    }

}
