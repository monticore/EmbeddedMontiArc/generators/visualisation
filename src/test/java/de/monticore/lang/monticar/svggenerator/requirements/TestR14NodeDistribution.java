/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

/**
 * We used node orthogonality, (partial) symmetry and node distribution as a measures of readability,
 * however we did not include it as a metric into the heuristic. Hence a layout that is optimized
 * regarding these measures is not the goal of this algorithm and is therefor not tested tested.
 */
public class TestR14NodeDistribution {
}
