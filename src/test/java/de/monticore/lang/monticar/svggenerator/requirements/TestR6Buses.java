/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.symboltable.Scope;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * Tests if Connections that share the same source and target
 * component are combined to a bus
 */
public class TestR6Buses {

    private MainCalculator calculator = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();

    @Test
    public void testNormalBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.connectedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        testHasBus(layoutState);

//      Check if all routes have been inserted
        testNumberOfRoutes(12, layoutState);

        testNumberOfTemporaryNodes(4, layoutState);
    }

    @Test
    public void testBackConnectorBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.backConnectorBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

//      Check if all routes have been inserted
        layoutState.busses.values().forEach(bus -> {
            if (!bus.removed) {
                assertEquals(bus.portConnections.size(), 5);
            }
        });

        testNumberOfDecomposedBuses(1, layoutState);

//      Check if there are exactly two temporary bus nodes
        testNumberOfTemporaryNodes(2, layoutState);
    }

    /**
     * Test no bus is created if not needed. For example when the source and target component are
     * located in adjacent columns.
     */
    @Test(expected = AssertionError.class)
    public void testDecomposedBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        testHasBus(layoutState);
    }

    /**
     * Tests that there is no bus created if there is no bus needed
     */
    @Test(expected = AssertionError.class)
    public void testNoBuses() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.simpleModel.simpleModel", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        testHasBus(layoutState);
    }

    private void testHasBus(RoutesLayoutState layoutState) {
        assertTrue(layoutState.connectorBusses.size() > 0);
    }

    private void testNumberOfTemporaryNodes(int expected, RoutesLayoutState layoutState) {
        List<Node> temporaryNodes = layoutState.nodeOrder.stream().filter(node -> node.isTemporary && node.isConnected()).collect(Collectors.toList());
        assertEquals(expected, temporaryNodes.size());
    }

    private void testNumberOfDecomposedBuses(int expected, RoutesLayoutState layoutState) {
        long removedBusCount = layoutState.busses.values().stream().filter(bus -> bus.decomposed).count();
        assertEquals(expected, removedBusCount);
    }

    private void testNumberOfRoutes(int expectedMaximum, RoutesLayoutState layoutState) {
        assertTrue(layoutState.routes.size() <= expectedMaximum);
    }
}
