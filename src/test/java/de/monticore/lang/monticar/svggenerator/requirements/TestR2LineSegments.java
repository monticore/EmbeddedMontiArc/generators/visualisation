/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Checks if every connector is made up of horizontals and vertical line segments
 */
public class TestR2LineSegments {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testPartedNodes() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testBackConnectorBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.backConnectorBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testConnectedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.connectedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkConnectors(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test(expected=AssertionError.class)
    public void testNoLineSegments() {
        checkLineSegments(new ArrayList<>());
    }

    @Test(expected=AssertionError.class)
    public void testDiagonalLine() {
        LineSymbol diagonalLine = new LineSymbol(new Point(0, 0), new Point(1, 1), 0, LayoutMode.NORMAL, false);

        List<LineSymbol> lines = new ArrayList<>();
        lines.add(diagonalLine);

        checkLineSegments(lines);
    }

    @Test
    public void testCorrectLines() {
        List<LineSymbol> lines = new ArrayList<>();

        LineSymbol verticalLine = new LineSymbol(new Point(0, 1), new Point(0, 0), 0, LayoutMode.NORMAL, false);
        LineSymbol horizontalLine = new LineSymbol(new Point(-1, 0), new Point(0, 0), 0, LayoutMode.NORMAL, false);

        lines.add(verticalLine);

        // Test should succeed with only one vertical line
        checkLineSegments(lines);

        lines.clear();
        lines.add(horizontalLine);

        // Test should succeed with only one horizontal line
        checkLineSegments(lines);

        lines.add(verticalLine);

        // Test should succeed with vertical and horizontal lines
        checkLineSegments(lines);
    }

    private void checkConnectors(Collection<ConnectorSymbol> connectors) {
        connectors.stream().map(c -> Tags.getLineSymbols(c, LayoutMode.NORMAL)).forEach(this::checkLineSegments);
    }

    private void checkLineSegments(Collection<LineSymbol> lines) {
        assertFalse(lines.isEmpty());

        lines.forEach(lineSymbol -> {
            Point start = lineSymbol.getStart();
            Point end = lineSymbol.getEnd();

            boolean isVertical = start.x == end.x;
            boolean isHorizontal = start.y == end.y;

            // Assure that the line element is horizontal or vertical
            assertTrue(isVertical || isHorizontal);
        });
    }
}
