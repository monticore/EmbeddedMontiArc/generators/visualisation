/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.routesTests;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.symboltable.Scope;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Peters Notebook on 10.11.2017.
 */
public class BusHandlerTest {

    private MainCalculator calculator = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();

    @Test
    public void testNormalBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

//      Check if a bus has been created
        assertEquals(1, layoutState.busses.size());

//      Check if all routes have been inserted
        assertEquals(6, layoutState.routes.size());

//      Check if there are exactly two temporary bus nodes
        List<Node> temporaryNodes = layoutState.nodeOrder.stream().filter(node -> node.isTemporary && node.isConnected()).collect(Collectors.toList());
        assertEquals(2, temporaryNodes.size());
    }

    @Test
    public void testDecomposedBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

//      Check if all routes have been inserted
        assertEquals(layoutState.routes.size(), 5);
    }

    @Test
    public void testBackConnectorBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.backConnectorBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

//      Check if all routes have been inserted
        layoutState.busses.values().forEach(bus -> {
            if (!bus.removed) {
                assertEquals(bus.portConnections.size(), 5);
            }
        });

        long removedBusCount = layoutState.busses.values().stream().filter(bus -> bus.decomposed).count();
        assertEquals(1, removedBusCount);

//      Check if there are exactly two temporary bus nodes
        List<Node> temporaryNodes = layoutState.nodeOrder.stream().filter(node -> node.isTemporary && node.isConnected()).collect(Collectors.toList());
        assertEquals(2, temporaryNodes.size());
    }

}
