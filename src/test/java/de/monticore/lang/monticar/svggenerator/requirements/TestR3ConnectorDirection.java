/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.requirements;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasPortLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.DrawableSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Checks if the first and last line segment of each connector is horizontal
 */
public class TestR3ConnectorDirection {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = TestingUtilities.createSymTab("src/test/resources/");
    }

    @Test
    public void testNormalConnectionsAndSplit() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.k33.k33", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkDirection(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.normalBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkDirection(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testDecomposedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.decomposedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkDirection(layoutState.componentInstanceSymbol.getConnectors());
    }

    @Test
    public void testConnectedBus() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.bus.connectedBus", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkDirection(layoutState.componentInstanceSymbol.getConnectors());
    }

    /**
     * Tests if test fails when the model contains a back connector, i.e. a connector that goes from right to left
     */
    @Test(expected = AssertionError.class)
    public void testBackConnector() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.thesisExample.thesisExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        checkDirection(layoutState.componentInstanceSymbol.getConnectors());
    }

    /**
     * Tests if test fails when passing a source port that is positioned on the right of the target port
     */
    @Test(expected=AssertionError.class)
    public void testReversedDirection() {
        PortLayoutSymbol source = new PortLayoutSymbol(0, 50, 0, true, LayoutMode.NORMAL);
        PortLayoutSymbol target = new PortLayoutSymbol(0, 0, 0, true, LayoutMode.NORMAL);

        checkCoordinates(source, target);
    }

    private void checkDirection(Collection<ConnectorSymbol> connectors) {
        connectors.forEach(connector -> {
            Optional<PortLayoutSymbol> sourcePortLayoutSymbol = Tags.getFirstLayoutTag(connector.getSourcePort(), LayoutMode.NORMAL);
            Optional<CanvasPortLayoutSymbol> sourceCanvasPortLayoutSymbol = Tags.getFirstCanvasLayoutTag(connector.getSourcePort(), LayoutMode.NORMAL);

            Optional<PortLayoutSymbol> targetPortLayoutSymbol = Tags.getFirstLayoutTag(connector.getTargetPort(), LayoutMode.NORMAL);
            Optional<CanvasPortLayoutSymbol> targetCanvasPortLayoutSymbol = Tags.getFirstCanvasLayoutTag(connector.getTargetPort(), LayoutMode.NORMAL);

            DrawableSymbol source = null;
            DrawableSymbol target = null;

            if (sourcePortLayoutSymbol.isPresent()) {
                source = sourcePortLayoutSymbol.get();
            } else if (sourceCanvasPortLayoutSymbol.isPresent()) {
                source = sourceCanvasPortLayoutSymbol.get();
            }

            if (targetPortLayoutSymbol.isPresent()) {
                target = targetPortLayoutSymbol.get();
            } else if (targetCanvasPortLayoutSymbol.isPresent()) {
                target = targetCanvasPortLayoutSymbol.get();
            }

            assertNotNull(source);
            assertNotNull(target);

            // Check if y coordinate of target port is larger than y coordinate of source port
            checkCoordinates(source, target);
        });
    }

    private void checkCoordinates(DrawableSymbol sourcePort, DrawableSymbol targetPort) {
        assertTrue(sourcePort instanceof PortLayoutSymbol || sourcePort instanceof CanvasPortLayoutSymbol);
        assertTrue(targetPort instanceof PortLayoutSymbol || targetPort instanceof CanvasPortLayoutSymbol);

        checkXCoordinates(sourcePort.getX(), targetPort.getX());
    }

    private void checkXCoordinates(int sourceX, int targetX) {
        assertTrue(targetX > sourceX);
    }
}
