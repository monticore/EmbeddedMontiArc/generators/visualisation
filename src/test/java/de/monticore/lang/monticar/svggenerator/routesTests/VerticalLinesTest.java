/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.routesTests;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class VerticalLinesTest {

    private MainCalculator calculator = new MainCalculator();
    private SVGGenerator svgGenerator = new SVGGenerator();

    @Test
    public void testNormalBus() {
        Scope symTab = TestingUtilities.createSymTab("src/test/resources/");
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("testManuelSchrick.verticalLines.verticalLines", ExpandedComponentInstanceSymbol.KIND).orElse(null);

        TestingUtilities.drawComponentsAndSubComponents(instance, 1, calculator, svgGenerator);
        RoutesLayoutState layoutState = calculator.routesLayoutCalculator.layoutState;

        layoutState.componentInstanceSymbol
                .getConnectors()
                .forEach(connector -> {
                    Collection<LineSymbol> tags = Tags.getLineSymbols(connector, LayoutMode.NORMAL);

                    //Check if layoutSymbol have been set
                    assertFalse(tags.isEmpty());

                    List<Integer> verticalLineXCoordinates = new ArrayList<>();
                    for (LineSymbol lineSymbol: tags) {
                        Point start = lineSymbol.getStart();
                        Point end = lineSymbol.getEnd();

                        if (start.x == end.x) {
                            verticalLineXCoordinates.add(start.x);
                        }
                    }

                    //Assure that the connections not connected to enclosing components
                    // all contain vertical line segments
                    if (verticalLineXCoordinates.isEmpty()) {
                        assertFalse(connector.getSource().equals(instance.getName()) || connector.getTarget().equals(instance.getName()));
                    }

                    //Check if there are no vertical lines drawn above one another
                    for (int i = 0; i < verticalLineXCoordinates.size() - 1; i++) {
                        for (int j = i + 1; j < verticalLineXCoordinates.size(); j++) {
                            assertNotEquals(verticalLineXCoordinates.get(i), verticalLineXCoordinates.get(j));
                        }
                    }
        });
    }

}
