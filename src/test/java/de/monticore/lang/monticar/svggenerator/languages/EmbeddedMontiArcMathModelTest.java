/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.languages;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.SVGGenerator;
import de.monticore.lang.monticar.svggenerator.SVGMain;
import de.monticore.lang.monticar.svggenerator.TestingUtilities;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.symboltable.Scope;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class EmbeddedMontiArcMathModelTest {

    private MainCalculator calculator;
    private SVGGenerator svgGenerator;
    private Scope symTab;

    @Before
    public void setup() {
        calculator = new MainCalculator();
        svgGenerator = new SVGGenerator();
        symTab = SVGMain.createSymTab("src/test/resources/");
    }

    @Test
    public void testClusterFiddle() {
        ExpandedComponentInstanceSymbol instance = symTab.<ExpandedComponentInstanceSymbol>resolve("detection.objectDetector1", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(instance);

        TestingUtilities.drawComponentsAndSubComponents(instance, 5, calculator, svgGenerator);
    }

}
