/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Manuel Schrick on 10.12.2017.
 */
public class TableDimensions implements DrawingConstants {

    private final static int LETTER_DIMENSION_FACTOR = 3;

    private LayoutMode layoutMode;
    private HashMap<String, Integer> componentWidths;
    private HashMap<Character, Integer> letterDimensions;
    private List<Integer> rowHeights;
    private List<Integer> rowSpacings;
    private List<Integer> columnSpacings;
    private int canvasMarginTop;
    private int canvasMarginBottom;
    private int canvasMarginLeft;
    private int canvasMarginRight;

    public TableDimensions(LayoutMode layoutMode) {
        componentWidths = new HashMap<>();
        letterDimensions = new HashMap<>();
        rowHeights = new ArrayList<>();
        rowSpacings = new ArrayList<>();
        columnSpacings = new ArrayList<>();
        this.layoutMode = layoutMode;

        buildLetterDimensionsMap();
    }

    public void computeDimensions(RoutesLayoutState layoutState) {
        layoutState.componentInstanceSymbol
                .getSubComponents()
                .forEach(comp -> componentWidths.put(comp.getName(), computeComponentWidth(comp)));

        computeRowHeights(layoutState);
        computeRowSpacings(layoutState);
        computeColumnSpacings(layoutState);
        computeCanvasMargins(layoutState.componentInstanceSymbol);
    }

    public int getNodeWidth(Node node) {
        if (node == null) {
            return 0;
        }

        if (node.isTemporary) {
            return TEMP_NODE_COLUMN_WIDTH;
        }

        return componentWidths.getOrDefault(node.compName, COLUMN_WIDTH);
    }

    public int getRowHeight(int rowIndex) {
        if (rowIndex < rowHeights.size()) {
            return rowHeights.get(rowIndex);
        }

        return ROW_HEIGHT_LARGE;
    }

    public int getSpacingAboveRow(int rowIndex) {
        if (rowIndex < rowSpacings.size()) {
            return rowSpacings.get(rowIndex);
        }

        return 0;
    }

    public int getSpacingBelowRow(int rowIndex) {
        return getSpacingAboveRow(rowIndex + 1);
    }

    public int getColumnSpacingLeft(int columnIndex) {
        if (columnIndex < columnSpacings.size()) {
            return columnSpacings.get(columnIndex);
        }

        return COLUMN_SPACING;
    }

    public int getColumnSpacingRight(int columnIndex) {
        return getColumnSpacingLeft(columnIndex + 1);
    }

    private void computeRowHeights(RoutesLayoutState layoutState) {
        rowHeights = new ArrayList<>();
        List<Route> routes = layoutState.routes;

        for(int row = 0; row < routes.size(); row++) {
            Route route = routes.get(row);
            int rowHeight = 0;

            for(int column = 1; column < route.size() - 1; column++) {
                int cellHeight = getCellHeight(row, column, layoutState);
                rowHeight = Math.max(rowHeight, cellHeight);
            }

            rowHeights.add(rowHeight);
        }
    }

    private int getCellHeight(int row, int column, RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;
        RouteState state = routes.get(row).getState(column);

        if (state == RouteState.EMPTY) {
            return 0;
        } else if (state == RouteState.ROUTE) {
            return ROW_HEIGHT;
        } else if (row == 0) {
            return ROW_HEIGHT_LARGE;
        } else {
            Node node = routes.get(row).getNode(column);
            Node nodeAbove = routes.get(row - 1).getNode(column);

            if (node.isTemporary) {
                return ROW_HEIGHT;
            } else if (nodeAbove == null || !node.compName.equals(nodeAbove.compName)) {
                return ROW_HEIGHT_LARGE;
            } else {
                return ROW_HEIGHT;
            }
        }
    }

    private void computeRowSpacings(RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;
        rowSpacings.add(TOP_ROW_EXTRA_SPACING);

        for (int row = 0; row < routes.size() - 1; row++) {
            Route upperRoute = routes.get(row);
            Route lowerRoute = routes.get(row + 1);
            int spacing = 0;

            for (int column = 1; column < upperRoute.size() - 1; column++) {
                Node upperNode = upperRoute.getNode(column);
                Node lowerNode = lowerRoute.getNode(column);

                if (upperNode == null || lowerNode == null) {
                    continue;
                } else if (upperNode.isTemporary || lowerNode.isTemporary) {
                    continue;
                } else if (!upperNode.compName.equals(lowerNode.compName)) {
                    spacing = ROW_SPACING;
                    break;
                }
            }

            rowSpacings.add(spacing);
        }

        rowSpacings.add(ROW_SPACING);
    }

    private void computeColumnSpacings(RoutesLayoutState layoutState) {
        columnSpacings = new ArrayList<>();
        HashMap<String, Integer> connectionsPerPort = new HashMap<>();
        List<Route> routes = layoutState.routes;
        int length = RoutesUtils.getColumnCount(layoutState);
        int columnIndexLeft = 0;

        layoutState.componentInstanceSymbol.getConnectors().forEach(connector -> {
            if (!layoutState.connectorBusses.containsKey(connector)) {
                PortSymbol sourcePort = connector.getSourcePort();
                PortSymbol targetPort = connector.getTargetPort();
                int sourceConnections = connectionsPerPort.getOrDefault(sourcePort.getFullName(), 0);
                int targetConnections = connectionsPerPort.getOrDefault(targetPort.getFullName(), 0);

                connectionsPerPort.put(sourcePort.getFullName(), sourceConnections + 1);
                connectionsPerPort.put(targetPort.getFullName(), targetConnections + 1);
            }
        });

        columnSpacings.add(COLUMN_SPACING);

        while (columnIndexLeft < length - 1) {
            boolean leftTempNodesOnly = true;
            boolean rightTempNodesOnly = true;
            int maxConnectionsPerBus = 1;

            for (Route route: routes) {
                Node nodeLeft = route.getNode(columnIndexLeft);
                Node nodeRight = route.getNode(columnIndexLeft + 1);

                if (nodeLeft != null && !nodeLeft.isTemporary) {
                    leftTempNodesOnly = false;
                }
                if (nodeRight != null && !nodeRight.isTemporary) {
                    rightTempNodesOnly = false;
                }

                PortSymbol portLeft = route.getNodeOutputPort(nodeLeft);
                if (portLeft != null) {
                    int connectionsOfPort = connectionsPerPort.getOrDefault(portLeft.getFullName(), 1);
                    maxConnectionsPerBus = Math.max(maxConnectionsPerBus, connectionsOfPort);
                }

                PortSymbol portRight = route.getNodeInputPort(nodeRight);
                if (portRight != null) {
                    int connectionsOfPort = connectionsPerPort.getOrDefault(portRight.getFullName(), 1);
                    maxConnectionsPerBus = Math.max(maxConnectionsPerBus, connectionsOfPort);
                }
            }

            if (leftTempNodesOnly || rightTempNodesOnly) {
                columnSpacings.add(COLUMN_SPACING_SMALL);
                columnIndexLeft += 1;
            } else {
                int spacing = COLUMN_SPACING + (maxConnectionsPerBus - 2) * VERTICAL_LINES_SPACING;
                columnSpacings.add(spacing);
                columnIndexLeft += 1;
            }

        }

        columnSpacings.add(COLUMN_SPACING);
    }

    private void computeCanvasMargins(ExpandedComponentInstanceSymbol component) {
        canvasMarginTop = 20;
        canvasMarginBottom = 5;

        if (layoutMode != LayoutMode.NORMAL) {
            canvasMarginLeft = PORT_WIDTH;
            canvasMarginRight = PORT_WIDTH;
            return;
        }

        canvasMarginLeft = component.getPorts().stream()
                .filter(PortSymbol::isIncoming)
                .mapToInt(port -> getDimensionForText(port.getName()))
                .max()
                .orElse(0);
        canvasMarginLeft = PORT_WIDTH + canvasMarginLeft * LETTER_DIMENSION_FACTOR;

        canvasMarginRight = component.getPorts().stream()
                .filter(PortSymbol::isOutgoing)
                .mapToInt(port -> getDimensionForText(port.getName()))
                .max()
                .orElse(0);
        canvasMarginRight = PORT_WIDTH + canvasMarginRight * LETTER_DIMENSION_FACTOR;
    }

    private int computeComponentWidth(ExpandedComponentInstanceSymbol component) {
        int spaceForName = (int) (1.5 * (getDimensionForText(component.getName()) + letterDimensions.get('m')));

        if (layoutMode != LayoutMode.NORMAL) {
            return LETTER_DIMENSION_FACTOR * spaceForName;
        }

        int spaceForInputPorts = component.getPorts().stream()
                .filter(PortSymbol::isIncoming)
                .mapToInt(p -> getDimensionForText(p.getName()))
                .max()
                .orElse(0);

        int spaceForOutputPorts = component.getPorts().stream()
                .filter(PortSymbol::isOutgoing)
                .mapToInt(p -> getDimensionForText(p.getName()))
                .max()
                .orElse(0);

        int spaceForPortNames = spaceForInputPorts + spaceForOutputPorts + 3 * letterDimensions.get('m');

        return LETTER_DIMENSION_FACTOR * Math.max(spaceForName, spaceForPortNames);
    }

    public int getDimensionForText(String text) {
        int dim = 0;
        
        for(int i = 0; i < text.length(); i++) {
            dim += getDimensionForLetter(text.charAt(i));
        }
        
        return dim;
    }
    
    private int getDimensionForLetter(Character letter) {
        if (letterDimensions.containsKey(letter)) {
            return letterDimensions.get(letter);
        }
        
        return 2;
    }

    private void buildLetterDimensionsMap() {
        letterDimensions.put('a', 2);
        letterDimensions.put('b', 2);
        letterDimensions.put('c', 2);
        letterDimensions.put('d', 2);
        letterDimensions.put('e', 2);
        letterDimensions.put('f', 1);
        letterDimensions.put('g', 2);
        letterDimensions.put('h', 2);
        letterDimensions.put('i', 1);
        letterDimensions.put('j', 1);
        letterDimensions.put('k', 2);
        letterDimensions.put('l', 1);
        letterDimensions.put('m', 3);
        letterDimensions.put('n', 2);
        letterDimensions.put('o', 2);
        letterDimensions.put('p', 2);
        letterDimensions.put('q', 2);
        letterDimensions.put('r', 2);
        letterDimensions.put('s', 2);
        letterDimensions.put('t', 1);
        letterDimensions.put('u', 2);
        letterDimensions.put('v', 2);
        letterDimensions.put('w', 3);
        letterDimensions.put('x', 2);
        letterDimensions.put('y', 2);
        letterDimensions.put('z', 2);
        letterDimensions.put('A', 2);
        letterDimensions.put('B', 2);
        letterDimensions.put('C', 2);
        letterDimensions.put('D', 2);
        letterDimensions.put('E', 2);
        letterDimensions.put('F', 2);
        letterDimensions.put('G', 2);
        letterDimensions.put('H', 2);
        letterDimensions.put('I', 2);
        letterDimensions.put('J', 2);
        letterDimensions.put('K', 2);
        letterDimensions.put('L', 2);
        letterDimensions.put('M', 3);
        letterDimensions.put('N', 2);
        letterDimensions.put('O', 2);
        letterDimensions.put('P', 2);
        letterDimensions.put('Q', 2);
        letterDimensions.put('R', 2);
        letterDimensions.put('S', 2);
        letterDimensions.put('T', 2);
        letterDimensions.put('U', 2);
        letterDimensions.put('V', 2);
        letterDimensions.put('W', 3);
        letterDimensions.put('X', 2);
        letterDimensions.put('Y', 2);
        letterDimensions.put('Z', 2);
    }

    public int getCanvasMarginTop() {
        return canvasMarginTop;
    }

    public int getCanvasMarginBottom() {
        return canvasMarginBottom;
    }

    public int getCanvasMarginLeft() {
        return canvasMarginLeft;
    }

    public int getCanvasMarginRight() {
        return canvasMarginRight;
    }
}
