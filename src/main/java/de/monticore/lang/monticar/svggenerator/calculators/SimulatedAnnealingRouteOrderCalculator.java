/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public class SimulatedAnnealingRouteOrderCalculator implements RouteOrderCalculator {

    private int iterationCount;
    private int swapCount;
    private Random random;
    private int iteration;

    public SimulatedAnnealingRouteOrderCalculator(int swapCount, int iterationCount) {
        this.swapCount = swapCount;
        this.iterationCount = iterationCount;
        this.random = new Random();
        this.iteration = 0;
    }

    @Override
    public List<Route> calculateRouteOrder(RoutesLayoutState layoutState) {
        if (layoutState.routes.size() <= 2) {
//            System.out.println("Model too small, no optimization necessary");
            return layoutState.routes;
        }

        double bestFitness = Double.NEGATIVE_INFINITY;
        double avg = 0;
        List<Route> bestOrder = new ArrayList<>();

        iterationCount = layoutState.routes.size() * 1000;
//        System.out.println("Number of Iterations: " + iterationCount);

        for (int i = 0; i < 1; i++) {
            List<Route> order = calculateOrder(layoutState);
            double fitness = getFitness(order, false);

            if (fitness > bestFitness) {
                bestFitness = fitness;
                bestOrder = order;
            }

            avg = (avg * i + fitness) / ((double) (i + 1));
        }

        return bestOrder;
    }

    private List<Route> calculateOrder(RoutesLayoutState layoutState) {
        long start = System.currentTimeMillis();
        List<Route> currentState = new ArrayList<>(layoutState.routes);
        double currentFitness = getFitness(currentState, false);
        double bestFitness = Double.NEGATIVE_INFINITY;
        List<Route> bestState = new ArrayList<>();

        for (int i = 0; i < iterationCount; i++) {
            double progress = i / (double) iterationCount;
            double temperature = getTemperature(progress);

            List<Route> candidate = getCandidate(currentState, progress);
            double candidateFitness = getFitness(candidate, false);

            if (shouldTransition(temperature, currentFitness, candidateFitness)) {
                currentState = candidate;
                currentFitness = candidateFitness;

                if (currentFitness > bestFitness) {
                    bestFitness = currentFitness;
                    bestState = currentState;
//                    System.out.println(i + ", better candidate: " + candidateFitness);
                }
            }
        }

        long duration = System.currentTimeMillis() - start;
//        System.out.println("iter" + iteration + ": dur: " + duration + ", fitness: " + currentFitness);
        iteration++;

//        System.out.println("last: " + currentFitness);
//        System.out.println("final: " + bestFitness);
        return bestState;
    }

    private double getTemperature(double progress) {
        return 1 / Math.log(progress + 1) - 1;
        //return Math.min(25, (1 / Math.pow(progress, 2)) - 1);
    }

    private List<Route> getCandidate(List<Route> routes, double progress) {
        List<Route> candidate = new ArrayList<>(routes);
        List<Integer> indices = new ArrayList<>();

        while (indices.size() < Math.max(swapCount, 2)) {
            int nextIndex = random.nextInt(candidate.size());

            if (!indices.contains(nextIndex)) {
                indices.add(nextIndex);
            }
        }

        for (int i = 0; i < indices.size() - 1; i++) {
            int index = indices.get(i);
            int otherIndex = indices.get(i + 1);

//            System.out.println("swap " + index + " with " + otherIndex);

            Route temp = candidate.get(otherIndex);
            candidate.set(otherIndex, candidate.get(index));
            candidate.set(index, temp);
        }

        return candidate;
    }

    public double getFitness(List<Route> candidate, boolean print) {
        int length = candidate.stream().mapToInt(Route::size).max().orElse(-1);
        double fitness = length * candidate.size();
        int totalSplits = 0;
        int totalSize = 0;

        for (int column = 0; column < length; column++) {
            boolean componentHasStarted = false;
            Node node = null;
            int size = 0;
            int splits = 0;
            int tempSize = 0;
            int tempSplits = 0;

            for (Route route : candidate) {
                if (route.size() <= column) {
                    if (componentHasStarted) {
                        tempSize += 1;
                    }
                    continue;
                }
                RouteState state = route.getState(column);

                if (state.equals(RouteState.NODE)) {
                    node = route.getNode(column);
                    componentHasStarted = true;
                    size += tempSize + 1;
                    splits += tempSplits;

                    tempSize = 0;
                    tempSplits = 0;
                } else if (componentHasStarted) {
                    if (state.equals(RouteState.ROUTE)) {
                        tempSplits += 1;
                    } else {
                        tempSize += 1;
                    }
                }
            }

            if (node == null || node.isTemporary) {
                fitness -= 2 * size;
                fitness -= Math.pow(splits + 1, 2);
            } else {
                fitness -= size * Math.pow(splits + 1, 2);
                fitness -= Math.pow(splits + 1, 2);
            }

            totalSize += size;
            totalSplits += splits;
        }

        if (print) {
            System.out.println("size: " + totalSize + ", splits: " + totalSplits + " -> fitness: " + fitness);
        }

        return fitness;
    }

    private boolean shouldTransition(double temperature, double currentFitness, double candidateFitness) {
        if (candidateFitness > currentFitness) {
//            System.out.println("better fitness");
            return true;
        }

        double exponent = -(currentFitness - candidateFitness) / temperature;
        double probability = Math.exp(exponent);

//        System.out.println("t: " + temperature + ", f1: " + currentFitness + ", f2: " + candidateFitness + ", prob: " + probability);

        return random.nextDouble() <= probability;
    }
}
