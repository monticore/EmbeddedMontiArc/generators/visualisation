/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Bus;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Peters Notebook on 04.11.2017.
 */
public class Log {

    public static void printNodes(RoutesLayoutState layoutState) {
        System.out.println("nodes: ");
        for (int i = 0; i < layoutState.nodeOrder.size(); i++) {
            System.out.println(i + ": " + layoutState.nodeOrder.get(i).compName);
        }
        System.out.println("");
    }

    public static void printRoutes(RoutesLayoutState layoutState) {
        printRoutes("routes:", layoutState);
    }

    public static void printRoutes(String title, RoutesLayoutState layoutState) {
        printRoutes(title, layoutState.routes);
    }

    public static void printRoutes(String title, List<Route> routes) {
        System.out.println(title);
        routes.forEach(Log::printRoute);
        System.out.println("");
    }

    public static void printRouteWithNames(Route route) {
        System.out.println(getRouteNameString(route));
    }

    public static void printRoute(Route route) {
        System.out.println(getRouteIdString(route));
    }

    public static void printBusses(RoutesLayoutState layoutState) {
        System.out.println("\nbusses: ");
        layoutState.busses.values().forEach(Log::printBus);
    }

    public static void printBus(Bus bus) {
        System.out.println(bus.sourceBus.node.compName + " -> " + bus.sourceBus.busNode.compName + " -> " + bus.targetBus.busNode.compName + " -> " + bus.targetBus.node.compName);
        System.out.println(bus.sourceBus.node.indexInOrder + " -> " + bus.sourceBus.busNode.indexInOrder + " -> " + bus.targetBus.busNode.indexInOrder + " -> " + bus.targetBus.node.indexInOrder);
    }

    public static void printUndrawnComponents(RoutesLayoutState layoutState) {
        if (!Tags.hasTags(layoutState.componentInstanceSymbol, CanvasLayoutSymbol.KIND)) {
            System.err.println("Canvas not drawn");
        } else {
            System.out.println("Canvas drawn");
        }

        List<ExpandedComponentInstanceSymbol> undrawnSubcomponents = layoutState.componentInstanceSymbol.getSubComponents().stream()
                .filter(c -> !Tags.hasTags(c, ComponentLayoutSymbol.KIND))
                .collect(Collectors.toList());

        if (!undrawnSubcomponents.isEmpty()) {
            System.err.println(undrawnSubcomponents.size() + "undrawn subcomponents:");
            undrawnSubcomponents.forEach(c -> System.err.println("   - " + c.getName()));
            System.out.println("");
        } else {
            System.out.println("All subcomponents drawn");
        }
    }

    public static void printUndrawnPorts(RoutesLayoutState layoutState) {
        List<PortSymbol> allPorts = new ArrayList<>(layoutState.componentInstanceSymbol.getPorts());
        for (ExpandedComponentInstanceSymbol subComp : layoutState.componentInstanceSymbol.getSubComponents()) {
            allPorts.addAll(subComp.getPorts());
        }

        List<PortSymbol> undrawnPorts = allPorts.stream()
                .filter(p -> !Tags.hasTags(p, PortLayoutSymbol.KIND) && !Tags.hasTags(p, CanvasPortLayoutSymbol.KIND))
                .collect(Collectors.toList());

        if (!undrawnPorts.isEmpty()) {
            System.err.println("undrawn ports: " + undrawnPorts.size());
            undrawnPorts.forEach(p -> {
                String message = "   - ";

                if (p.getComponentInstance().isPresent()) {
                    message += p.getComponentInstance().get().getName();
                }

                message += "." + p.getName() + " " + (p.isIncoming() ? "(in)" : "(out)");
                System.err.println(message);
            });
            System.out.println("");
        } else {
            System.out.println("All Ports drawn");
        }
    }

    public static void printUndrawnConnectors(RoutesLayoutState layoutState) {
        List<ConnectorSymbol> undrawnConnectors = layoutState.componentInstanceSymbol.getConnectors().stream()
                .filter(c -> !Tags.hasTags(c, LineSymbol.KIND))
                .collect(Collectors.toList());

        if (!undrawnConnectors.isEmpty()) {
            System.err.println(undrawnConnectors.size() + " undrawn connectors:");
            undrawnConnectors.forEach(c -> {
                PortSymbol sourcePort = c.getSourcePort();
                PortSymbol targetPort = c.getTargetPort();
                String message = "  - ";

                if (sourcePort.getComponentInstance().isPresent()) {
                    message += sourcePort.getComponentInstance().get().getName();
                } else {
                    message += "null";
                }
                message += "." + sourcePort.getName() + " -> ";

                if (targetPort.getComponentInstance().isPresent()) {
                    message += targetPort.getComponentInstance().get().getName();
                } else {
                    message += "null";
                }
                message += "." + targetPort.getName();
                System.err.println(message);
            });
            System.out.println("");
        } else {
            System.out.println("All Connectors drawn");
        }
    }

    public static String getRouteIdString(Route route) {
        int id = route.id;
        String message;

        if (id < 10) {
            message = "   " + String.valueOf(id) + ": ";
        } else if (id < 100) {
            message = "  " + String.valueOf(id) + ": ";
        } else if (id < 1000) {
            message = " " + String.valueOf(id) + ": ";
        } else {
            message = String.valueOf(id) + ": ";
        }

        for (int i = 0; i < route.size(); i++) {
            RouteState state = route.getState(i);

            switch (state) {
                case EMPTY:
                    message += "    ";
                    break;
                case NODE:
                    message += getNodeString(route, i, false);
                    break;
                case ROUTE:
                    message += "----";
                    break;
            }
        }

        return message;
    }

    private static String getNodeString(Route route, int columnIndex, boolean useNodeName) {
        Node node = route.getNode(columnIndex);
        if (node == null) {
            return " NL ";
        } else {
            int index = node.indexInOrder;
            String message = "";

            String connectionSymbol = node.isTemporary ? "~" : "-";

            if (route.getNodeInputPort(node) != null) {
                if (index < 10 ) {
                    message += "-" + connectionSymbol;
                } else {
                    message += connectionSymbol;
                }
            } else {
                message += index < 10 ? "  " : " ";
            }

            if (useNodeName) {
                message += node.compName;
            } else {
                message += String.valueOf(index);
            }

            if (route.getNodeOutputPort(node) != null) {
                message += connectionSymbol;
            } else {
                message += " ";
            }

            return message;
        }
    }

    private static String getRouteNameString(Route route) {
        int id = route.id;
        StringBuilder messageBuilder = new StringBuilder(String.valueOf(id) + ": ");

        for (int i = 0; i < route.size(); i++) {
            RouteState state = route.getState(i);

            switch (state) {
                case EMPTY:
                    messageBuilder.append("  ");
                    break;
                case NODE:
                    messageBuilder.append(getNodeString(route, i, true));
                    break;
                case ROUTE:
                    messageBuilder.append("--");
                    break;
            }
        }

        return messageBuilder.toString();
    }
}
