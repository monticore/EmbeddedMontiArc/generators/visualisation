/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.monticar.svggenerator.ViewModel.*;
import de.monticore.lang.monticar.svggenerator.ViewModel.*;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Felix on 07.03.2017.
 */
public class TemplateBuilder {

    // Create your Configuration instance, and specify if up to what FreeMarker
    // version (here 2.3.23) do you want to apply the fixes that are not 100%
    // backward-compatible. See the Configuration JavaDoc for details.
    private static Configuration defaultConf = new Configuration(Configuration.VERSION_2_3_23);

    static { // fancy java static blocks

        // Specify the source where the template files come from. Here I set a
        // plain directory for it, but non-file-system sources are possible too:
//        try {
//            defaultConf.setDirectoryForTemplateLoading(new File("src/main/templates"));
            defaultConf.setClassForTemplateLoading(TemplateBuilder.class, "/src/main/templates");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        // Set the preferred charset template files are stored in. UTF-8 is
        // a good choice in most applications:
        defaultConf.setDefaultEncoding("UTF-8");
        try {
            defaultConf.setSetting("number_format", "computer");
        } catch (TemplateException e) {
            e.printStackTrace();
        }

        // Sets how errors will appear.
        // During web page *development* TemplateExceptionHandler.HTML_DEBUG_HANDLER is better.
        defaultConf.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        // Don't log exceptions inside FreeMarker that it will thrown at you anyway:
        defaultConf.setLogTemplateExceptions(false);
    }

    public String build(String templateName, Map<String, Object> root) {
        Template temp = null;
        try {
            temp = this.configuration.getTemplate(templateName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        StringWriter out = new StringWriter();
        try {
            temp.process(root, out);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return out.toString();
    }

    private Configuration configuration;
    public CanvasViewModel canvas;
    public ComponentViewModel[] components;
    public ConnectorViewModel connectors;
    public PortViewModel[] ports;

    public TemplateBuilder(Configuration configuration){
        this.configuration = configuration;
    }
    public TemplateBuilder() {
        this.configuration = defaultConf;
    }

    //Used to build a string representation
    //All public properties should be set
    public String build() throws IOException {

        StringBuilder output = new StringBuilder();

        output.append(build("canvas.ftl", this.canvas.hashMap()));

        for(ComponentViewModel viewModel : this.components) {
            output.append(build("component.ftl", viewModel.hashMap()));
        }

        output.append(build("arrowHeads.ftl", this.connectors.hashMap()));

        for(LineViewModel viewModel : this.connectors.lines) {
            output.append(build("line.ftl", viewModel.hashMap()));
        }

        if (ports != null) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("portWidth", DrawingConstants.PORT_WIDTH);
            hashMap.put("portHeight", DrawingConstants.PORT_HEIGHT);
            output.append(build("portDefine.ftl", hashMap));

            for (PortViewModel viewModel : this.ports) {
                PortTemplate.drawPortDescription(output, viewModel);
            }
        }

        HashMap<String, Object> svgMap = new HashMap<>();
        svgMap.put("svgbody", output.toString());

        return build("svg.ftl", svgMap);

    }

    public String buildHTMLFile(String name, int width, int height, Hierarchy hierarchy, String fileNameSuffix) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("fullName", name);
        hashMap.put("width", width);
        hashMap.put("height", height);
        hashMap.put("nameHierarchy", hierarchy.names);
        hashMap.put("fullNameHierarchy", hierarchy.fullNames);
        hashMap.put("fileNameSuffix", fileNameSuffix);
        hashMap.put("onlineIDE", RunOptions.onlineIDEFlag);

        return build("html.ftl", hashMap);
    }

    public static class Hierarchy {
        public List<String> names;
        public List<String> fullNames;
    }
}
