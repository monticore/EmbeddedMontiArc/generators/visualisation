/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

public class Point {

    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Point) {
            Point otherPoint = (Point) obj;

            return x == otherPoint.x && y == otherPoint.y;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return (x + "-" + y).hashCode();
    }
}
