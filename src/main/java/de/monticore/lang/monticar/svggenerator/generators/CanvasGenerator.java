/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;
import de.monticore.lang.monticar.svggenerator.ViewModel.CanvasViewModel;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hanna Franzen on 30.10.16.
 */
public class CanvasGenerator extends InputGenerator {

    private LayoutMode layoutMode;

    public CanvasGenerator(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    @Override
    public void draw(ExpandedComponentInstanceSymbol inst,
                     TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix) {

        List<CanvasLayoutSymbol> canvasLayoutSymbols = new ArrayList<>(Tags.getCanvasLayoutTags(inst, layoutMode));

        if (!canvasLayoutSymbols.isEmpty()) {
            CanvasLayoutSymbol sym = canvasLayoutSymbols.get(0);

            CanvasViewModel viewModel = new CanvasViewModel();
            viewModel.x = sym.getX();
            viewModel.y = sym.getY();
            viewModel.id = sym.getId();
            viewModel.height = sym.getHeight();
            viewModel.width = sym.getWidth();
            viewModel.name = inst.getName();

            templateBuilder.canvas = viewModel;
        } else {
            ComponentLayoutSymbol sym = Tags.getFirstLayoutTag(inst, layoutMode).get();

            CanvasViewModel viewModel = new CanvasViewModel();
            viewModel.x = sym.getX();
            viewModel.y = sym.getY();
            viewModel.id = sym.getId();
            viewModel.height = sym.getHeight();
            viewModel.width = sym.getWidth();
            viewModel.name = inst.getName();

            templateBuilder.canvas = viewModel;
        }
    }
}
