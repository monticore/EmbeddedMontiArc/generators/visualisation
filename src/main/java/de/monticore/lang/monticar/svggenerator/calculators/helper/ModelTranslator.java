/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Peters Notebook on 04.11.2017.
 */
public class ModelTranslator implements DrawingConstants {

    public static TranslatedModel translateModel(ExpandedComponentInstanceSymbol instance) {
        TranslatedModel translatedModel = new TranslatedModel();

        HashMap<String, Node> nodes = new HashMap<>();

        Node rootIn = new Node(instance, NAME_ROOT_IN);
        nodes.put(rootIn.compName, rootIn);

        Node rootOut = new Node(instance, NAME_ROOT_OUT);
        nodes.put(rootOut.compName, rootOut);

        String parentName = instance.getPackageName() + "." + instance.getName();

        instance.getSubComponents().stream()
                .map(Node::new)
                .forEach(node -> nodes.put(node.compName, node));

        instance.getConnectors().stream()
                .filter(connector -> connector.getSourcePort() != null && connector.getTargetPort() != null)
                .filter(connector -> connector.getSourcePort().getComponentInstance().isPresent())
                .filter(connector -> connector.getTargetPort().getComponentInstance().isPresent())
                .forEach(connector -> {
                    PortSymbol sourcePort = connector.getSourcePort();
                    PortSymbol targetPort = connector.getTargetPort();
                    ExpandedComponentInstanceSymbol sourceComp = sourcePort.getComponentInstance().get();
                    ExpandedComponentInstanceSymbol targetComp = targetPort.getComponentInstance().get();

                    Node sourceNode;

                    if (sourceComp.equals(instance) || sourceComp.getFullName().equals(parentName)) {
                        sourceNode = rootIn;
                    } else {
                        sourceNode = nodes.get(sourceComp.getName());
                    }

                    Node targetNode;

                    if (targetComp.equals(instance) || targetComp.getFullName().equals(parentName)) {
                        targetNode = rootOut;
                    } else {
                        targetNode = nodes.get(targetComp.getName());
                    }

                    if (sourceNode == null) {
                        System.err.println("ModelTranslator: source node is null:");
                        System.err.println("   " + sourceComp.getFullName());
                        System.err.println("   " + sourcePort.getFullName());
                        System.err.println("   ->");
                        System.err.println("   " + targetComp.getFullName());
                        System.err.println("   " + targetPort.getFullName());
                        System.err.println("   " + instance.getFullName());
                        System.err.println("   " + parentName);
                    }

                    if (targetNode == null) {
                        System.err.println("ModelTranslator: target node is null:");
                        System.err.println("   " + sourceComp.getFullName());
                        System.err.println("   " + sourcePort.getFullName());
                        System.err.println("   ->");
                        System.err.println("   " + targetComp.getFullName());
                        System.err.println("   " + targetPort.getFullName());
                        System.err.println("   " + instance.getFullName());
                        System.err.println("   " + parentName);
                    }

                    sourceNode.addOutput(targetNode, sourcePort);
                    targetNode.addInput(sourceNode, targetPort);

                    translatedModel.putCorrelation(sourcePort, targetPort, connector);
                });

        translatedModel.nodes = new ArrayList<>(nodes.values());

        return translatedModel;
    }
}
