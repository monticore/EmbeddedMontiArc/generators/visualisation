/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;

public class PortLayoutSymbol extends DrawableSymbol implements LayoutModeDepending {
    public static final PortLayoutKind KIND = PortLayoutKind.INSTANCE;

    public PortLayoutSymbol(int id, int x, int y, boolean internal, LayoutMode layoutMode) {
        super(KIND, id, x, y);
        addValues(internal, layoutMode);
    }

    public boolean isInternal() {
        return getValue(3);
    }

    public LayoutMode getLayoutMode() {
        return getValue(4);
    }


    public static class PortLayoutKind extends DrawableKind {
        public static final PortLayoutKind INSTANCE = new PortLayoutKind();

        protected PortLayoutKind() {
        }
    }
}
