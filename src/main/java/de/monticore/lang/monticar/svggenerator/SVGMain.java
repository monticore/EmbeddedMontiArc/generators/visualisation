/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.ModelingLanguageFamily;
import de.monticore.io.paths.ModelPath;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.EmbeddedMontiArcLanguage;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.InstancingRegister;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarcmath._symboltable.EmbeddedMontiArcMathLanguage;
import de.monticore.lang.monticar.struct._symboltable.StructLanguage;
import de.monticore.lang.monticar.svggenerator.ViewModel.RunOptions;
import de.monticore.lang.monticar.svggenerator.calculators.MainCalculator;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Scope;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Felix on 03.03.2017.
 * <p>
 * Welcome to our SVG Renderer \o/
 * This class will give you a nowhere specified interface to some classes we wrote back in the
 * '16s. As someone maybe mentioned this tool could be used via commandline I thought it for an
 * awesome idea to provide a not so awesome cmdline interface. The following is the documentation:
 * <p>
 * ./whatever.jar --input someRandomModel --modelPath /path/to/modelPath
 * <p>
 * No, you must not enter the file name! Enter the name of the model. If the model contains
 * uppercase letters... Things will happen.
 * <p>
 * We will write the files to "./output/". So please make sure this exists and nothing important
 * is in there.
 * <p>
 * As you can see in the html output it is possible to click the models. You are responsible to
 * build the dependencies of your input. Otherwise link may return 404. Another thing to
 * mention is that the output size of our html is hard coded, so for very large diagrams it is
 * possible that you can only see parts of it. If you fixed this, please remove these sentences.
 * <p>
 * No, I have no idea how to disable logs. This logs may be printed into stdout. Sorry for
 * this.
 */

public class SVGMain {
    public final static String KEY_INPUT = "--input";
    public final static String KEY_MODEL_PATH = "--modelPath";
    public final static String KEY_OUTPUT_PATH = "--outputPath";
    public final static String KEY_RECURSIVE_DRAWING = "--recursiveDrawing";
    public final static String KEY_ONLINE_IDE = "--onlineIDE";

    public static void main(String[] args) {
        HashMap<String, String> argsMap = new HashMap<>();

        int i = 0;
        while (i < args.length) {
            String key = args[i];
            if (key.equals(KEY_ONLINE_IDE)) {
                RunOptions.onlineIDEFlag = true;
                i += 1;
            } else if (i < args.length - 1) {
                argsMap.put(args[i], args[i + 1]);
                i += 2;
            } else {
                i += 1;
            }
        }

        if (!argsMap.containsKey(KEY_INPUT)) {
            System.err.println("You missed something. Please provide a model via --input");
            System.exit(1);
        } else if (!argsMap.containsKey(KEY_MODEL_PATH)) {
            argsMap.put(KEY_MODEL_PATH, "");
        }

        try {
            boolean recursiveDrawing = argsMap.getOrDefault(KEY_RECURSIVE_DRAWING, "false").equals("true");
            String input = argsMap.getOrDefault(KEY_INPUT, "");
            String path = argsMap.getOrDefault(KEY_MODEL_PATH, "");
            String output = argsMap.getOrDefault(KEY_OUTPUT_PATH, "");

            build(input, path, output, recursiveDrawing);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Scope createSymTab(String modelPath) {
        ModelingLanguageFamily fam = new ModelingLanguageFamily();
        fam.addModelingLanguage(new EmbeddedMontiArcLanguage());
        fam.addModelingLanguage(new EmbeddedMontiArcMathLanguage());
        fam.addModelingLanguage(new StructLanguage());
        final ModelPath mp = new ModelPath();
        mp.addEntry(Paths.get(modelPath));

        GlobalScope scope = new GlobalScope(mp, fam);

        de.monticore.lang.monticar.Utils.addBuiltInTypes(scope);

        InstancingRegister.reset();
        Tags.initTaggingResolver(scope, modelPath);
        return scope;
    }

    private static void build(String input, String modelPath, String outputPath, boolean recursiveDrawing) {
        MainCalculator calculator = new MainCalculator();
        SVGGenerator svgGenerator = new SVGGenerator(outputPath);

        Scope symTab = createSymTab(modelPath);

        if (symTab == null) {
            System.out.println("Could not create symbol table. Is your model path correct?");
        }

        ExpandedComponentInstanceSymbol inst = symTab.<ExpandedComponentInstanceSymbol>resolve(input, ExpandedComponentInstanceSymbol.KIND).orElse(null);

        if (inst == null) {
            System.out.println("Could not resolve instance symbol. Is your input path correct?");
        }

        // Calculate Layout information for the given input
        calculator.calculateLayout(inst);

        boolean success = true;

        // Draw the layout (create output SVG file)
        try {
            svgGenerator.drawLayouts(inst, calculator.routesLayoutCalculator.layoutState);
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }

        success = success && checkEnclosingComponent(inst) && checkSubComponents(inst);

        System.out.println(inst.getFullName());
        if (success) {
            System.out.println("   success");
        } else {
            System.out.println("   failed");
        }

        if (recursiveDrawing) {
            success = success && drawSubComponents(inst, calculator, svgGenerator);
        }

        if (success) {
            System.out.println("Overall: success");
        } else {
            System.out.println("Overall: failure");
        }
    }

    private static boolean drawSubComponents(ExpandedComponentInstanceSymbol instanceSymbol, MainCalculator calculator, SVGGenerator svgGenerator) {
        boolean success = true;

        Collection<ExpandedComponentInstanceSymbol> subComponents = instanceSymbol.getSubComponents();
        for (ExpandedComponentInstanceSymbol symbol : subComponents) {
            System.out.println(symbol.getFullName());
            if (symbol.getSubComponents().isEmpty()) {
                System.out.println("   skipped");
                continue;
            }

            try {
                // Calculate Layout information for the given input
                calculator.calculateLayout(symbol);

                // Draw the layout (create output SVG file)
                svgGenerator.drawLayouts(symbol, calculator.routesLayoutCalculator.layoutState);

                success = success && checkEnclosingComponent(symbol);

                if (checkSubComponents(instanceSymbol)) {
                    System.out.println("   success");
                } else {
                    System.out.println("   failed");
                    success = false;
                }
            } catch (IOException e) {
                e.printStackTrace();
                success = false;
            }
        }

        for (ExpandedComponentInstanceSymbol symbol : subComponents) {
            success = success && drawSubComponents(symbol, calculator, svgGenerator);
        }

        return success;
    }

    private static boolean checkSubComponents(ExpandedComponentInstanceSymbol enclosingComponent) {
        List<String> undrawnComponentNames = new ArrayList<>();

        enclosingComponent.getSubComponents().forEach(subComp -> {
            boolean drawn = true;

            for (LayoutMode layoutMode: LayoutMode.values()) {
                if (!Tags.hasTags(subComp, ComponentLayoutSymbol.KIND, layoutMode)) {
                    System.out.println("   subComp " + subComp.getName() + " not drawn in LayoutMode " + layoutMode);
                    drawn = false;
                    break;
                }
            }

            if (!drawn) {
                undrawnComponentNames.add(subComp.getName());
            }

        });

        return undrawnComponentNames.isEmpty();
    }

    private static boolean checkEnclosingComponent(ExpandedComponentInstanceSymbol enclosingComponent) {
        boolean drawn = true;

        for (LayoutMode layoutMode: LayoutMode.values()) {
            if (!Tags.hasTags(enclosingComponent, ComponentLayoutSymbol.KIND, layoutMode)) {
                System.out.println("   not drawn in LayoutMode " + layoutMode);
                drawn = false;
                break;
            }
        }

        return drawn;
    }

}


