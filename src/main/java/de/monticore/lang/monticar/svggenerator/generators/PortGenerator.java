/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;
import de.monticore.lang.monticar.svggenerator.ViewModel.PortViewModel;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Robert on 01.11.2016.
 */
public class PortGenerator extends InputGenerator {

    private LayoutMode layoutMode;

    public PortGenerator(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    // Add the SVG code of all Ports
    @Override
    public void draw(ExpandedComponentInstanceSymbol inst,
                     TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix) {
        LinkedList<PortViewModel> portViewModels = new LinkedList<>();

        // DRAW Canvas Ports
        inst.getConnectors().forEach(con -> buildConnectorPortViewModels(con, portViewModels));

        //http://stackoverflow.com/a/5374359/1857527
        PortViewModel[] modelArray = new PortViewModel[portViewModels.size()];
        modelArray = portViewModels.toArray(modelArray);

        templateBuilder.ports = modelArray;
    }

    private void buildConnectorPortViewModels(ConnectorSymbol connector, LinkedList<PortViewModel> portViewModels) {
        //System.out.println("build port view models for connector: " + connector.getSourcePort().getFullName() + " -> " + connector.getTargetPort().getFullName());
        
        List<PortSymbol> ports = new ArrayList<>();
        ports.add(connector.getSourcePort());
        ports.add(connector.getTargetPort());

        ports.stream()
                .forEach(port -> {
                    List<PortLayoutSymbol> portLayoutSymbols = SymbolsHelper.portLayoutSymbolsOfPort(port, layoutMode);
                    List<CanvasPortLayoutSymbol> canvasPortLayoutSymbols = SymbolsHelper.canvasPortLayoutSymbolsOfPort(port, layoutMode);

                    if (!canvasPortLayoutSymbols.isEmpty()) {
                        portViewModels.add(getPortViewModel(port, canvasPortLayoutSymbols.get(0)));
                    } else if (!portLayoutSymbols.isEmpty()) {
                        portViewModels.add(getPortViewModel(port, portLayoutSymbols.get(0)));
                    }

                    //System.out.println("   - " + port.getName() + ": " + portViewModel.x + " - " + portViewModel.y);
                });
    }

    private PortViewModel getPortViewModel(PortSymbol port, PortLayoutSymbol sym) {
        PortViewModel portViewModel = new PortViewModel();
        portViewModel.x = sym.getX();
        portViewModel.y = sym.getY();
        portViewModel.portname = layoutMode == LayoutMode.NORMAL ? port.getName() : "";
        portViewModel.isInternal = true;
        portViewModel.isOutgoing = port.isOutgoing();
        portViewModel.isIncoming = port.isIncoming();
        portViewModel.layoutPosition = LayoutPosition.LEFT;

        return portViewModel;
    }

    private PortViewModel getPortViewModel(PortSymbol port, CanvasPortLayoutSymbol sym) {
        PortViewModel portViewModel = new PortViewModel();
        portViewModel.x = sym.getX();
        portViewModel.y = sym.getY();
        portViewModel.portname = layoutMode == LayoutMode.NORMAL ? port.getName() : "";
        portViewModel.isInternal = true;
        portViewModel.isOutgoing = port.isOutgoing();
        portViewModel.isIncoming = port.isIncoming();
        portViewModel.layoutPosition = LayoutPosition.UNDEFINED;

        return portViewModel;
    }
}

