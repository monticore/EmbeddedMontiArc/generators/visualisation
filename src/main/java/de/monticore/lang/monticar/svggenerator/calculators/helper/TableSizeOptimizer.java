/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.NodeExtent;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class TableSizeOptimizer {

    public static void optimizeVertically(RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;
        int routeIndex = 1;
        while (routeIndex < routes.size()) {
            Route route = routes.get(routeIndex);
            Route routeAbove = routes.get(routeIndex - 1);
            boolean canMergeRoutes = canMergeRoutes(route, routeAbove);

            if (canMergeRoutes) {
                routeAbove.merge(route);
                routes.remove(routeIndex);
            } else {
                routeIndex++;
            }
        }


    }

    private static boolean canMergeRoutes(Route route, Route otherRoute) {
        for (int columnIndex = 0; columnIndex < route.size(); columnIndex++) {
            RouteState state = route.getState(columnIndex);
            RouteState otherState = otherRoute.getState(columnIndex);
            Node node = route.getNode(columnIndex);
            Node otherNode = otherRoute.getNode(columnIndex);

            if (state == RouteState.NODE && otherState == RouteState.NODE) {
                boolean inputPortsConflict = route.getNodeInputPort(node) != null && otherRoute.getNodeInputPort(otherNode) != null;
                boolean outputPortsConflict = route.getNodeOutputPort(node) != null && otherRoute.getNodeOutputPort(otherNode) != null;

                if (inputPortsConflict || outputPortsConflict) {
                    return false;
                }
            } else if (state == RouteState.NODE && otherState == RouteState.ROUTE) {
                return false;
            } else if (state == RouteState.ROUTE && otherState != RouteState.EMPTY) {
                return false;
            }
        }

        return true;
    }

    public static void optimizeHorizontally(RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;
        int length = routes.stream().mapToInt(Route::size).max().orElse(0);

        if (length <= 3) {
            return;
        }

        moveNodes(routes, Direction.LEFT, node -> true);
        removeEmptyColumns(layoutState.routes);
        moveTemporaryNodesRight(layoutState);
    }

    private static void removeEmptyColumns(List<Route> routes) {
        int length = RoutesUtils.getColumnCount(routes);
        int deletedColumnCount = 0;
        int columnIndex = 0;
        while (columnIndex < length - deletedColumnCount) {
            boolean isEmpty = RoutesUtils.isColumnEmpty(columnIndex, routes);

            if (isEmpty) {
                for (Route route : routes) {
                    route.remove(columnIndex);
                }

                deletedColumnCount += 1;
            } else {
                columnIndex += 1;
            }
        }
    }

    private static void moveTemporaryNodesRight(RoutesLayoutState layoutState) {
        int length = RoutesUtils.getColumnCount(layoutState);

        if (length <= 3) {
            return;
        }

        List<Node> busTargetNodes = new ArrayList<>();
        layoutState.busses.values().forEach( bus -> busTargetNodes.add(bus.targetBus.busNode));

        moveNodes(layoutState.routes, Direction.RIGHT, busTargetNodes::contains);

        List<Node> nodesWithNoInputConnection = layoutState.nodeOrder.stream()
                .filter( node -> node.getInputs().isEmpty() )
                .collect(Collectors.toList());

        moveNodes(layoutState.routes, Direction.RIGHT, nodesWithNoInputConnection::contains);
    }

    private static void moveNodes(List<Route> routes, Direction direction, Predicate<Node> predicate) {
        int length = RoutesUtils.getColumnCount(routes);

        if (length <= 3) {
            return;
        }

        int indexStep = (direction == Direction.LEFT) ? -1 : 1;
        int columnIndex = (direction == Direction.LEFT) ? 2 : length - 2;
        int rowIndex = 0;

        while (columnIndex > 0 && columnIndex < length - 1) {
            int tempColumnIndex = columnIndex;

            NodeExtent nodeExtent = RoutesUtils.getNextNodeExtent(columnIndex, rowIndex, routes);
            rowIndex = nodeExtent.bottomRowIndex + 1;

            if (nodeExtent.node == null) {
                rowIndex = 0;
                columnIndex -= indexStep;
                continue;
            } else if (!predicate.test(nodeExtent.node)) {
                continue;
            }

            int nextColumnIndex = tempColumnIndex + indexStep;
            boolean columnIsFree = isColumnFreeAt(nextColumnIndex, nodeExtent.topRowIndex, nodeExtent.bottomRowIndex, routes);

            while (columnIsFree && nextColumnIndex >= 1 && nextColumnIndex < length - 1) {
                moveNode(nodeExtent, tempColumnIndex, direction, routes);
                tempColumnIndex += indexStep;

                nextColumnIndex = tempColumnIndex + indexStep;
                columnIsFree = isColumnFreeAt(nextColumnIndex, nodeExtent.topRowIndex, nodeExtent.bottomRowIndex, routes);
            }
        }
    }

    private static boolean isColumnFreeAt(int columnIndex, int rowIndexTop, int rowIndexBottom, List<Route> routes) {
        List<Integer> occupiedIndices = getOccupiedRowIndicesInColumn(columnIndex, routes);

        for (int routeIndex = rowIndexTop; routeIndex <= rowIndexBottom; routeIndex++) {
            if (occupiedIndices.contains(routeIndex)) {
                return false;
            }
        }

        return true;
    }

    private static void moveNode(NodeExtent nodeExtent, int sourceColumnIndex, Direction direction, List<Route> routes) {
        int indexStep = (direction == Direction.LEFT) ? -1 : 1;
        int targetColumnIndex = sourceColumnIndex + indexStep;
        int previousColumnIndex = sourceColumnIndex - indexStep;

        for (int routeIndex = nodeExtent.topRowIndex; routeIndex <= nodeExtent.bottomRowIndex; routeIndex++) {
            Route route = routes.get(routeIndex);
            RouteState state = route.getState(sourceColumnIndex);
            RouteState nextState = route.getState(targetColumnIndex);

            if (nextState != RouteState.NODE) {
                if (state == RouteState.NODE) {
                    route.set(targetColumnIndex, route.getNode(sourceColumnIndex));
                } else if (state == RouteState.ROUTE) {
                    route.set(targetColumnIndex, route.getState(sourceColumnIndex));
                }
            }

            RouteState newState;
            if (route.size() > previousColumnIndex && previousColumnIndex > 0) {
                RouteState previousState = route.getState(previousColumnIndex);

                if (state == RouteState.ROUTE || previousState == RouteState.ROUTE) {
                    newState = RouteState.ROUTE;
                } else if (previousState == RouteState.NODE) {
                    Node previousNode = route.getNode(previousColumnIndex);
                    PortSymbol portSymbol = (direction == Direction.LEFT) ? route.getNodeInputPort(previousNode) : route.getNodeOutputPort(previousNode);
                    if (portSymbol != null) {
                        newState = RouteState.ROUTE;
                    } else {
                        newState = RouteState.EMPTY;
                    }
                } else if (nextState == RouteState.NODE) {
                    Node nextNode = route.getNode(targetColumnIndex);
                    PortSymbol portSymbol = (direction == Direction.LEFT) ? route.getNodeOutputPort(nextNode) : route.getNodeInputPort(nextNode);
                    if (portSymbol != null) {
                        newState = RouteState.ROUTE;
                    } else {
                        newState = RouteState.EMPTY;
                    }
                } else {
                    newState = RouteState.EMPTY;
                }
            } else {
                newState = RouteState.EMPTY;
            }

            route.set(sourceColumnIndex, newState);
        }
    }

    private static List<Integer> getOccupiedRowIndicesInColumn(int columnIndex, List<Route> routes) {
        List<Integer> indices = new ArrayList<>();
        List<Integer> tempIndices = new ArrayList<>();
        Node currentNode = null;

        for (int i = 0; i < routes.size(); i++) {
            Route route = routes.get(i);
            RouteState state = route.getState(columnIndex);

            if (state == RouteState.NODE) {
                Node node = route.getNode(columnIndex);
                if (currentNode == null) {
                    currentNode = node;
                } else if (currentNode.equals(node)) {
                    indices.addAll(tempIndices);
                    tempIndices.clear();
                } else {
                    currentNode = node;
                    tempIndices.clear();
                }

                indices.add(i);
            } else if (currentNode != null) {
                tempIndices.add(i);
            }
        }

        return indices;
    }

    private enum Direction {
        LEFT, RIGHT
    }
}
