/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;

public class CanvasPortLayoutSymbol extends DrawableSymbol implements LayoutModeDepending {
    public static final CanvasPortLayoutKind KIND = CanvasPortLayoutKind.INSTANCE;

    public CanvasPortLayoutSymbol(int id, int x, int y, LayoutMode layoutMode) {
        super(KIND, id, x, y);

        addValues(layoutMode);
    }

    public LayoutMode getLayoutMode() {
        return getValue(3);
    }

    @Override
    public String toString() {
        return String.format(
                "componentLayout = { id=%d, x=%d, y=%d}",
                getId(), getX(), getY());
    }


    public static class CanvasPortLayoutKind extends DrawableKind {
        public static final CanvasPortLayoutKind INSTANCE = new CanvasPortLayoutKind();

        protected CanvasPortLayoutKind() {
        }
    }
}
