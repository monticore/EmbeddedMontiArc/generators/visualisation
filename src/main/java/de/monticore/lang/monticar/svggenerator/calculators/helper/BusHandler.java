/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class BusHandler {

    private RoutesLayoutState layoutState;
    private TranslatedModel translatedModel;

    public BusHandler(RoutesLayoutState layoutState, TranslatedModel translatedModel) {
        this.layoutState = layoutState;
        this.translatedModel = translatedModel;
    }

    public void removeBuses() {
        for (Pair<Node, Node> pair: layoutState.busses.keySet()) {
            Bus bus = layoutState.busses.get(pair);

            if (bus.isConnectedWithOtherBuses()) {
                continue;
            } else if (bus.sourceBus.busNode == null || bus.targetBus.busNode == null) {
                bus.removed = true;
                continue;
            }

            int tempSourceIndex = RoutesUtils.getColumnOfNode(bus.sourceBus.busNode, layoutState.routes);
            int tempTargetIndex = RoutesUtils.getColumnOfNode(bus.targetBus.busNode, layoutState.routes);

            if (tempTargetIndex <= tempSourceIndex) {
                continue;
            }

            Route sourceRoute = RoutesUtils.getRouteForConnection(bus.sourceBus.node, bus.sourceBus.busNode, layoutState);
            Route targetRoute = RoutesUtils.getRouteForConnection(bus.targetBus.busNode, bus.targetBus.node, layoutState);

            if (layoutState.routes.indexOf(sourceRoute) == layoutState.routes.indexOf(targetRoute)) {
                sourceRoute.set(tempSourceIndex, RouteState.ROUTE);
                targetRoute.set(tempTargetIndex, RouteState.ROUTE);
            }

            bus.removed = true;
        }
    }

    public void insertBuses() {
        List<Pair<Node, Node>> decomposedBuses = new ArrayList<>();
        List<BusPart> insertedBuses = new ArrayList<>();

        for (Bus bus: layoutState.busses.values()) {
            Node source = bus.sourceBus.node;
            Node target = bus.targetBus.node;

            Route route = RoutesUtils.getRouteForConnection(bus.sourceBus.busNode, bus.targetBus.busNode, layoutState);

            if (route != null && shouldDecomposeBus(bus)) {
                decomposeBus(bus);
                decomposedBuses.add(new Pair<>(source, target));
                insertedBuses.add(bus.sourceBus);
                insertedBuses.add(bus.targetBus);
                continue;
            }

            Route sourceRoute = RoutesUtils.getRouteForConnection(bus.sourceBus.node, bus.sourceBus.busNode, layoutState);
            Route targetRoute = RoutesUtils.getRouteForConnection(bus.targetBus.busNode, bus.targetBus.node, layoutState);

            if (sourceRoute != null) {
                if (!insertedBuses.contains(bus.sourceBus)) {
                    insertBusPartRoutes(bus.sourceBus, sourceRoute, bus.targetBus.node);
                    insertedBuses.add(bus.sourceBus);
                }
            } else {
                System.err.println("BusHandler: There is no source Route for the bus from " + bus.sourceBus.node.compName + " to " + bus.targetBus.node.compName);
            }

            if (targetRoute != null) {
                if (!insertedBuses.contains(bus.targetBus)) {
                    insertBusPartRoutes(bus.targetBus, targetRoute, bus.sourceBus.node);
                    insertedBuses.add(bus.targetBus);
                }
            } else {
                System.err.println("BusHandler: There is no target Route for the bus from " + bus.sourceBus.node.compName + " to " + bus.targetBus.node.compName);
            }
        }

        for (Pair<Node, Node> connection : decomposedBuses) {
            Bus bus = layoutState.busses.remove(connection);

            if (bus == null) {
                continue;
            }

            for(Pair<PortSymbol, PortSymbol> portPair: bus.portConnections) {
                ConnectorSymbol connector = translatedModel.getConnector(portPair.getKey(), portPair.getValue());
                layoutState.connectorBusses.remove(connector);
            }
        }
    }

    private void decomposeBus(Bus bus) {
        List<Route> routes = layoutState.routes;
        Node source = bus.sourceBus.node;
        Node target = bus.targetBus.node;
        Route route = RoutesUtils.getRouteForConnection(bus.sourceBus.busNode, bus.targetBus.busNode, layoutState);

        int routeIndexAbove = routes.indexOf(route) - 1;
        int routeIndexBelow = routes.indexOf(route) + 1;
        int sourceIndex = layoutState.nodeOrder.indexOf(source);
        int targetIndex = layoutState.nodeOrder.indexOf(target);

        PortSymbol sourceOutputPort = bus.portConnections.get(0).getKey();
        PortSymbol targetInputPort = bus.portConnections.get(0).getValue();
        route.set(sourceIndex, source, route.getNodeInputPort(source), sourceOutputPort);
        route.set(targetIndex, target, targetInputPort, route.getNodeOutputPort(target));

        for (int i = sourceIndex + 1; i < targetIndex; i++) {
            route.set(i, RouteState.ROUTE);
        }

        for (int i = 1; i < bus.portConnections.size(); i++) {
            Route routeAbove = routeIndexAbove >= 0 ? routes.get(routeIndexAbove) : null;
            Route routeBelow = routeIndexBelow < routes.size() ? routes.get(routeIndexBelow) : null;
            Route temp;

            if (routeAbove != null && routeAbove.isFreeBetweenNodes(sourceIndex, targetIndex)) {
                temp = routeAbove;
                routeIndexAbove -= 1;
            } else if (routeBelow != null && routeBelow.isFreeBetweenNodes(sourceIndex, targetIndex)) {
                temp = routeBelow;
                routeIndexBelow += 1;
            } else {
                temp = RoutesUtils.addRowAt(routes.indexOf(route), layoutState);
                routeIndexAbove += 1;
            }

            for (int j = sourceIndex + 1; j < targetIndex; j++) {
                temp.set(j, RouteState.ROUTE);
            }

            sourceOutputPort = bus.portConnections.get(i).getKey();
            targetInputPort = bus.portConnections.get(i).getValue();
            temp.set(sourceIndex, source, temp.getNodeInputPort(source), sourceOutputPort);
            temp.set(targetIndex, target, targetInputPort, temp.getNodeOutputPort(target));
        }

        bus.decomposed = true;
    }

    private void insertBusPartRoutes(BusPart bus, Route initialRoute, Node otherNode) {
        List<Route> routes = layoutState.routes;

        Node leftNode = bus.getLeftNode();
        Node rightNode = bus.getRightNode();

        PortSymbol leftInputPort;
        PortSymbol leftOutputPort;
        PortSymbol rightInputPort;
        PortSymbol rightOutputPort;

        int routeIndexAbove = routes.indexOf(initialRoute) - 1;
        int routeIndexBelow = routes.indexOf(initialRoute) + 1;
        int leftIndex = initialRoute.columnIndexOf(leftNode);
        int rightIndex = initialRoute.columnIndexOf(rightNode);

        if (bus.isSource) {
            bus.busNode.addOutput(otherNode, DummyPort.create());
        } else {
            bus.busNode.addInput(otherNode, DummyPort.create());
        }

        for (int i = 1; i < bus.ports.size(); i++) {
            if (bus.isSource) {
                leftOutputPort = bus.ports.get(i);
                rightInputPort = DummyPort.create();
            } else {
                leftOutputPort = DummyPort.create();
                rightInputPort = bus.ports.get(i);
            }
            insertBusNodesInputAndOutputForConnection(bus, otherNode, leftOutputPort, rightInputPort);

            Route routeAbove = routeIndexAbove >= 0 ? routes.get(routeIndexAbove) : null;
            Route routeBelow = routeIndexBelow < routes.size() ? routes.get(routeIndexBelow) : null;
            Route temp;

            boolean routeAboveIsFree = routeAbove != null && routeAbove.isFreeBetweenNodes(leftIndex, rightIndex);
            boolean routeBelowIsFree = routeBelow != null && routeBelow.isFreeBetweenNodes(leftIndex, rightIndex);
            boolean routeAboveContainsNodes = routeAbove != null && routeAbove.columnIndexOf(leftNode) >= 0 && routeAbove.columnIndexOf(rightNode) >= 0;

            if (routeAboveIsFree && (routeAboveContainsNodes || !routeBelowIsFree)) {
                temp = routeAbove;
                routeIndexAbove -= 1;
            } else if (routeBelowIsFree) {
                temp = routeBelow;
                routeIndexBelow += 1;
            } else if (routeAboveContainsNodes) {
                temp = RoutesUtils.addRowAt(routes.indexOf(initialRoute), layoutState);
                routeIndexBelow += 1;
            } else {
                temp = RoutesUtils.addRowAt(routes.indexOf(initialRoute) + 1, layoutState);
                routeIndexBelow += 1;
            }

            leftInputPort = temp.getNodeInputPort(leftNode);
            rightOutputPort = temp.getNodeOutputPort(rightNode);

            temp.set(leftIndex, leftNode, leftInputPort, leftOutputPort);
            temp.set(rightIndex, rightNode, rightInputPort, rightOutputPort);

            for (int index = leftIndex + 1; index < rightIndex; index++) {
                temp.set(index, RouteState.ROUTE);
            }
        }
    }

    private void insertBusNodesInputAndOutputForConnection(BusPart bus, Node otherNode, PortSymbol leftOutputPort, PortSymbol rightInputPort) {
        if (bus.isSource) {
            bus.node.addOutput(bus.busNode, leftOutputPort);
            bus.busNode.addInput(bus.node, DummyPort.create());
        } else {
            bus.busNode.addOutput(bus.node, DummyPort.create());
            bus.node.addInput(bus.busNode, rightInputPort);
        }
    }

    private boolean shouldDecomposeBus(Bus bus) {
//        System.out.println("\nshould decompose");
//        Log.printBus(bus);

        int sourceIndex = layoutState.nodeOrder.indexOf(bus.sourceBus.node);
        int targetIndex = layoutState.nodeOrder.indexOf(bus.targetBus.node);
        int numberOfSourcePorts = bus.sourceBus.ports.size();
        int numberOfTargetPorts = bus.targetBus.ports.size();

        if (targetIndex < sourceIndex
                || bus.isConnectedWithOtherBuses()
                || numberOfSourcePorts != numberOfTargetPorts) {
//            System.out.println("false A:");
//            System.out.println("  " + targetIndex + " < " + sourceIndex);
//            System.out.println("  " + bus.isConnectedWithOtherBuses());
//            System.out.println("  " + numberOfSourcePorts + " != " + numberOfTargetPorts);
            return false;
        } else if (RoutesUtils.isNodeSplit(bus.sourceBus.node, layoutState)) {
//            System.out.println("false B");
            return false;
        } else if (RoutesUtils.isNodeSplit(bus.targetBus.node, layoutState)) {
//            System.out.println("false C");
            return false;
        } else if (bus.portConnections.size() <= 1) {
//            System.out.println("true D");
            return true;
        } else if (targetIndex > sourceIndex + 4) {
//            System.out.println("false E");
            return false;
        }

        int sourceBusIndex = layoutState.nodeOrder.indexOf(bus.sourceBus.busNode);
        int targetBusIndex = layoutState.nodeOrder.indexOf(bus.targetBus.busNode);

        if (bus.sourceBus.ports.size() > 2 || bus.targetBus.ports.size() > 2) {
            for (int i = sourceBusIndex + 1; i < targetBusIndex; i++) {
                if (!layoutState.nodeOrder.get(i).isTemporary) {
//                    System.out.println("false F: " + layoutState.nodeOrder.get(i).indexInOrder);
                    return false;
                }
            }
        }

//        System.out.println("true G");
        return true;
    }
}
