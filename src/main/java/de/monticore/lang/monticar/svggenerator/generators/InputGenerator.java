/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;

/**
 * Created by hanna on 30.10.16.
 */
public abstract class InputGenerator {

    // Add the SVG code of the input using the given SVGBuilder
    // Modes: All inputs of this type black OR different colors for every component
    // Color mode works best if inputs that come from one component are defined one after the other
    public abstract void draw(ExpandedComponentInstanceSymbol inst, TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix);
}
