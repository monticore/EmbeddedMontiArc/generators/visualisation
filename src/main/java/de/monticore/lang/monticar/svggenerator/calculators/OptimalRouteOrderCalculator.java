/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.monticar.svggenerator.calculators.helper.Log;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public class OptimalRouteOrderCalculator implements RouteOrderCalculator {

    @Override
    public List<Route> calculateRouteOrder(RoutesLayoutState layoutState) {
        layoutState.routes.forEach(route -> route.computeOpenAndClosedNodes(layoutState.nodeOrder));
        List<Route> remainingRoutes = new ArrayList<>(layoutState.routes);
        List<Route> routeOrder = new ArrayList<>();

        List<List<Node>> openNodesStack = new ArrayList<>();
        List<List<Route>> possibleRoutesStack = new ArrayList<>();
        HashMap<Route, Integer> routePriorities = new HashMap<>();
        List<Route> nextRouteStack = new ArrayList<>();

        openNodesStack.add(new ArrayList<>());
        possibleRoutesStack.add(getNextPossibleRoutes(remainingRoutes, new ArrayList<>(), routePriorities));

        List<Route> possibleRoutes;
        List<Node> openNodes;

        int stepCounter = 0;
        boolean foundOrder = false;
        while (!foundOrder && !remainingRoutes.isEmpty() && !possibleRoutesStack.isEmpty()) {
            stepCounter++;
            possibleRoutes = possibleRoutesStack.get(0);

            Log.printRoutes("step " + stepCounter + ": ", routeOrder);

            if (possibleRoutes.isEmpty()) {
                possibleRoutesStack.remove(0);
                openNodesStack.remove(0);
                routeOrder.remove(routeOrder.size() - 1);

                Route reverted = nextRouteStack.remove(0);

                remainingRoutes.forEach(route -> {
                    int priority = routePriorities.getOrDefault(route, 0) + 1;
                    routePriorities.put(route, priority);
                });

                remainingRoutes.add(reverted);
                continue;
            }

            openNodes = openNodesStack.get(0);

            Route nextRoute = possibleRoutes.remove(0);
            routeOrder.add(nextRoute);
            remainingRoutes.remove(nextRoute);

            for (Node open : nextRoute.openNodes) {
                if (!openNodes.contains(open)) {
                    openNodes.add(open);
                }
            }

            openNodes.removeAll(nextRoute.closedNodes);

            possibleRoutesStack.add(0, getNextPossibleRoutes(remainingRoutes, openNodes, routePriorities));
            openNodesStack.add(0, openNodes);
            nextRouteStack.add(0, nextRoute);

            foundOrder = remainingRoutes.isEmpty() || routeOrder.size() > layoutState.routes.size();
        }

        return routeOrder;
    }

    private List<Route> getNextPossibleRoutes(List<Route> remainingRoutes, List<Node> openNodes, HashMap<Route, Integer> routePriorities) {
        List<Route> nextPossibleRoutes = new ArrayList<>();

        for (Route route : remainingRoutes) {
            boolean hasConflict = false;

            for (Node closing : route.closedNodes) {
                if (!openNodes.contains(closing)) {
                    continue;
                }

                for (Route other : remainingRoutes) {
                    if (other.equals(route)) {
                        continue;
                    }

                    if (other.openNodes.contains(closing)) {
                        hasConflict = true;
                        break;
                    }
                }

                if (hasConflict) {
                    break;
                }
            }

            if (!hasConflict) {
                nextPossibleRoutes.add(route);
            }
        }

        nextPossibleRoutes.sort((lhs, rhs) -> {
            int lhsPriority = routePriorities.getOrDefault(lhs, 0);
            int rhsPriority = routePriorities.getOrDefault(rhs, 0);

            return Integer.compare(lhsPriority, rhsPriority);
        });

        return nextPossibleRoutes;
    }
}
