/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

public enum LayoutMode {
    NORMAL, NO_PORT_NAMES, SIMPLIFIED
}
