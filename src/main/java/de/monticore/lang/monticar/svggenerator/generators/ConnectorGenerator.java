/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;
import de.monticore.lang.monticar.svggenerator.ViewModel.ConnectorViewModel;
import de.monticore.lang.monticar.svggenerator.ViewModel.LineViewModel;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ConnectorLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.SymbolsHelper;

import java.util.*;

/**
 * Created by svg-group
 */
public class ConnectorGenerator extends InputGenerator implements DrawingConstants {

    private LayoutMode layoutMode;

    public ConnectorGenerator(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    @Override
    public void draw(ExpandedComponentInstanceSymbol inst, TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix) {
        ConnectorViewModel connectorViewModel = new ConnectorViewModel();

        // Array of colors which are used for drawing the lines
        HashSet<String> colorSet = new HashSet<>();
        connectorViewModel.colorSet = colorSet;
        connectorViewModel.lines = new ArrayList<>();

        // Adding the first Black color
        colorSet.add("0,0,0");

        inst.getConnectors().forEach(connector -> {
            List<ConnectorLayoutSymbol> connectorLayoutSymbols = SymbolsHelper.connectorLayoutSymbolsOfConnector(connector, layoutMode);
//            if (!connectorLayoutSymbols.isEmpty()) {
//                ConnectorLayoutSymbol connectorLayoutSymbol = connectorLayoutSymbols.get(0);
//                connectorViewModel.lines.addAll(createLineViewModels(connectorLayoutSymbol));
//            }
            if (Tags.hasTags(connector, LineSymbol.KIND, layoutMode)) {
                Collection<LineSymbol> lineSymbols = Tags.getLineSymbols(connector, layoutMode);
                connectorViewModel.lines.addAll(createLineViewModels(lineSymbols));
            }
        });

        templateBuilder.connectors = connectorViewModel;
    }

    private List<LineViewModel> createLineViewModels(Collection<LineSymbol> lineSymbols) {
        List<LineViewModel> lineViewModels = new ArrayList<>();

        Iterator<LineSymbol> iterator = lineSymbols.iterator();
        while (iterator.hasNext()) {
            LineSymbol lineSymbol = iterator.next();
            Point start = lineSymbol.getStart();
            Point end = lineSymbol.getEnd();
            boolean isLastPoint = !iterator.hasNext();
            int lineWidth = lineSymbol.getLineWidth();
            int indexInBus = lineSymbol.getIndexInBus();

            Point knot = null;
            if (lineSymbol.hasKnotAtStart()) {
                knot = start;
            }

            lineViewModels.add(newLineViewModel(start, end, lineWidth, isLastPoint, indexInBus, knot));
        }

        return lineViewModels;
    }

    private List<LineViewModel> createLineViewModels(ConnectorLayoutSymbol connectorLayoutSymbol) {
        List<LineViewModel> lineViewModels = new ArrayList<>();
        Point[] points = connectorLayoutSymbol.getPoints();
        Point[] knots = connectorLayoutSymbol.getKnots();
        int[] lineWidths = connectorLayoutSymbol.getLineWidths();

        for (int i = 0; i < points.length - 1; i++) {
            boolean isLastPoint = i == points.length - 2;
            int indexInBus = -1;
            Point knot = null;

            if (i == 0 || isLastPoint) {
                indexInBus = connectorLayoutSymbol.getIndexInBus();
            }

            if (knots.length > i) {
                knot = knots[i];
            }

            lineViewModels.add(newLineViewModel(points[i], points[i + 1], lineWidths[i], isLastPoint, indexInBus, knot));
        }

        return lineViewModels;
    }

    private LineViewModel newLineViewModel(Point start, Point end, int lineWidth, boolean isEnd, int indexInBus, Point knot) {
        LineViewModel viewModel = new LineViewModel();
        viewModel.x = start.x;
        viewModel.y = start.y;
        viewModel.xx = end.x;
        viewModel.yy = end.y;
        viewModel.red = 0;
        viewModel.green = 0;
        viewModel.blue = 0;
        viewModel.linewidth = lineWidth;
        viewModel.isEnd = isEnd;

        if (indexInBus >= 0 && layoutMode != LayoutMode.SIMPLIFIED) {
            viewModel.indexInBus = indexInBus;
        }

        viewModel.knot = knot;

        return viewModel;
    }
}
