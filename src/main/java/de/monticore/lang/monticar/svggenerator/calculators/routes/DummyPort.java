/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;

/**
 * Created by Peters Notebook on 02.07.2017.
 */
public class DummyPort extends PortSymbol {

    private static int index = 0;

    protected DummyPort(String name) {
        super(name);
    }

    public static DummyPort create() {
        index += 1;

        return new DummyPort("dummy_port_" + index);
    }
}
