/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Bus;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Peters Notebook on 04.11.2017.
 */
public class RoutesLayoutState {
    public ExpandedComponentInstanceSymbol componentInstanceSymbol;
    public List<Node> nodeOrder;
    public List<Route> routes;
    public HashMap<Pair<Node, Node>, Bus> busses;
    public HashMap<Pair<Node, Node>, Integer> visitedConnections;
    public HashMap<Integer, Integer> verticalLinesAfterColumn;
    public HashMap<ConnectorSymbol, Bus> connectorBusses;

    public RoutesLayoutState() {
        nodeOrder = new ArrayList<>();
        routes = new ArrayList<>();
        busses = new HashMap<>();
        visitedConnections = new HashMap<>();
        verticalLinesAfterColumn = new HashMap<>();

        connectorBusses = new HashMap<>();
    }

    public RoutesLayoutState clone() {
        RoutesLayoutState layoutState = new RoutesLayoutState();

        layoutState.componentInstanceSymbol = componentInstanceSymbol;
        layoutState.nodeOrder.addAll(nodeOrder);
        layoutState.busses.putAll(busses);
        layoutState.visitedConnections.putAll(visitedConnections);
        layoutState.verticalLinesAfterColumn.putAll(verticalLinesAfterColumn);
        layoutState.connectorBusses.putAll(connectorBusses);

        routes.forEach( route -> {
            layoutState.routes.add(route.copy());
        });

        return layoutState;
    }
}
