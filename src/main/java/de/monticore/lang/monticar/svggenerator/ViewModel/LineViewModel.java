/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;

import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;

import java.util.HashMap;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class LineViewModel extends ViewModel {

    public int x;
    public int y;
    public int xx;
    public int yy;

    public int red;
    public int green;
    public int blue;

    public Integer indexInBus;
    public int linewidth;
    public boolean isEnd;

    public Point knot;

    @Override
    public HashMap<String, Object> hashMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("X", x);
        map.put("Y", y);
        map.put("XX", xx);
        map.put("YY", yy);
        map.put("red", red);
        map.put("green", green);
        map.put("blue", blue);
        map.put("linewidth", linewidth);
        map.put("isEnd", isEnd);

        if (indexInBus != null) {
            map.put("indexInBus", indexInBus);
        }

        if (knot != null) {
            map.put("knotX", knot.x);
            map.put("knotY", knot.y);
            map.put("knotRadius", DrawingConstants.KNOT_RADIUS);
        }

        return map;
    }
}
