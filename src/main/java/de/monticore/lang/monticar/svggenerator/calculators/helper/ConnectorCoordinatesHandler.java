/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Bus;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasPortLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.DrawableSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LineSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;

import java.util.*;
import java.util.List;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class ConnectorCoordinatesHandler implements DrawingConstants {

    private LayoutMode layoutMode;
    private RoutesLayoutState layoutState;
    private List<Pair<String, String>> drawnConnections;

    public ConnectorCoordinatesHandler(RoutesLayoutState layoutState, LayoutMode layoutMode) {
        this.layoutState = layoutState;
        this.layoutMode = layoutMode;
        this.drawnConnections = new ArrayList<>();
    }

    public void assignConnectorCoordinates(ExpandedComponentInstanceSymbol enclosingComponent) {
        assignConnectorCoordinatesWithLineSymbols(enclosingComponent);
    }

    public void assignConnectorCoordinatesWithLineSymbols(ExpandedComponentInstanceSymbol enclosingComponent) {
        enclosingComponent.getConnectors()
                .stream()
                .filter(this::shouldDrawConnector)
                .forEach(connector -> {
            PortSymbol sourcePort = connector.getSourcePort();
            PortSymbol targetPort = connector.getTargetPort();

            boolean sourcePortHasMultipleConnectors;
            boolean targetPortHasMultipleConnectors;

            Point previousPoint;
            Point finalPoint;

            DrawableSymbol sourcePortLayoutSymbol;
            if (Tags.hasTags(sourcePort, CanvasPortLayoutSymbol.KIND, layoutMode)) {
                sourcePortLayoutSymbol = Tags.getFirstCanvasLayoutTag(sourcePort, layoutMode).get();
                sourcePortHasMultipleConnectors = Tags.getCanvasLayoutTags(sourcePort, layoutMode).size() > 1;
            } else if (Tags.hasTags(sourcePort, PortLayoutSymbol.KIND, layoutMode)) {
                sourcePortLayoutSymbol = Tags.getFirstLayoutTag(sourcePort, layoutMode).get();
                sourcePortHasMultipleConnectors = Tags.getLayoutTags(sourcePort, layoutMode).size() > 1;
            } else {
                if (layoutMode != LayoutMode.SIMPLIFIED) {
                    System.err.println("SourcePort has no layout symbol: " + sourcePort.getName() + " -> " + targetPort.getName() + " (" + layoutMode + ")");
                }
                return;
            }
            int sourcePortX = sourcePortLayoutSymbol.getX();
            int sourcePortY = sourcePortLayoutSymbol.getY() + (int) (PORT_WIDTH / 2.0);

            if (layoutMode == LayoutMode.SIMPLIFIED) {
                sourcePortX += (int) (PORT_WIDTH / 2.0);
            } else {
                sourcePortX += PORT_WIDTH;
            }

            previousPoint = new Point(sourcePortX, sourcePortY);

            Optional<PortLayoutSymbol> optTargetPortLayoutSymbol = Tags.getFirstLayoutTag(targetPort, layoutMode);
            Optional<CanvasPortLayoutSymbol> optTargetPortCanvasLayoutSymbol = Tags.getFirstCanvasLayoutTag(targetPort, layoutMode);
            DrawableSymbol targetPortLayoutSymbol;
            if (optTargetPortCanvasLayoutSymbol.isPresent()) {
                targetPortLayoutSymbol = optTargetPortCanvasLayoutSymbol.get();
                targetPortHasMultipleConnectors = Tags.getCanvasLayoutTags(targetPort, layoutMode).size() > 1;
            } else if (optTargetPortLayoutSymbol.isPresent()) {
                targetPortLayoutSymbol = optTargetPortLayoutSymbol.get();
                targetPortHasMultipleConnectors = Tags.getLayoutTags(targetPort, layoutMode).size() > 1;
            } else {
                if (layoutMode != LayoutMode.SIMPLIFIED) {
                    System.err.println("TargetPort has no layout symbol: " + sourcePort.getName() + " -> " + targetPort.getName() + " (" + layoutMode + ")");
                }
                return;
            }
            int targetPortX = targetPortLayoutSymbol.getX();
            int targetPortY = targetPortLayoutSymbol.getY() + (int) (PORT_WIDTH / 2.0);

            if (layoutMode == LayoutMode.SIMPLIFIED) {
                targetPortX += (int) (PORT_WIDTH / 2.0);
            }

            finalPoint = new Point(targetPortX, targetPortY);

            int lineWidth = CONNECTOR_LINE_WIDTH;
            Bus bus = layoutState.connectorBusses.get(connector);
            if (bus != null && bus.removed) {
                bus = null;
            }

            int indexInBus = -1;
            if (bus != null && (bus.isConnectedWithOtherBuses() || bus.portConnections.size() > 1)) {
                indexInBus = bus.sourceBus.ports.indexOf(sourcePort);
            }

            if (bus != null) {
                Integer sourceX = bus.sourceBus.xCoordinate;

                if (sourceX == null) {
                    int sourceColumnIndex = RoutesUtils.getColumnOfNode(bus.sourceBus.busNode, layoutState.routes);
                    sourceX = RoutesUtils.getColumnLeft(sourceColumnIndex, layoutState.routes);
                    bus.sourceBus.xCoordinate = sourceX;
                }
                Point firstPoint = new Point(sourceX, previousPoint.y);

                LineSymbol firstSymbol = new LineSymbol(previousPoint, firstPoint, lineWidth, layoutMode, false, indexInBus);
                Tags.addTag(connector, firstSymbol);

                if (layoutMode != LayoutMode.SIMPLIFIED && bus.sourceBus.ports.size() > 1) {
                    lineWidth = BUS_LINE_WIDTH;
                }

                int sourceY;
                int sourceCenterRow = (int) Math.floor(bus.sourceBus.centerRow);
                int sourceCenterRowOffset = (int) ((bus.sourceBus.centerRow % 1) * RoutesUtils.getRowHeight(sourceCenterRow + 1));
                sourceY = RoutesUtils.getRowCenter(sourceCenterRow) + sourceCenterRowOffset;

                Point secondPoint = new Point(sourceX, sourceY);

                int targetY;
                int targetCenterRow = (int) Math.floor(bus.targetBus.centerRow);
                int targetCenterRowOffset = (int) ((bus.targetBus.centerRow % 1) * RoutesUtils.getRowHeight(targetCenterRow + 1));
                targetY = RoutesUtils.getRowCenter(targetCenterRow) + targetCenterRowOffset;

                boolean hasIntermediateLines = sourceY != targetY;

                boolean hasKnot = layoutMode != LayoutMode.SIMPLIFIED && bus.sourceBus.ports.size() > 1;
                LineSymbol secondSymbol = new LineSymbol(firstPoint, secondPoint, lineWidth, layoutMode, hasKnot);
                Tags.addTag(connector, secondSymbol);

                if (hasIntermediateLines) {
                    sourceX = RoutesUtils.getColumnRight(bus.sourceBus.busColumn, layoutState.routes);
                    Point intermediatePoint = new Point(sourceX, sourceY);
                    LineSymbol intermediateSymbol = new LineSymbol(secondPoint, intermediatePoint, lineWidth, layoutMode, false);
                    Tags.addTag(connector, intermediateSymbol);
                    secondPoint = intermediatePoint;

                    if (layoutMode != LayoutMode.SIMPLIFIED) {
                        lineWidth = BUS_LINE_WIDTH;
                    }

                    intermediatePoint = new Point(sourceX, targetY);
                    intermediateSymbol = new LineSymbol(secondPoint, intermediatePoint, lineWidth, layoutMode, true);
                    Tags.addTag(connector, intermediateSymbol);

                    secondPoint = intermediatePoint;
                }

                boolean targetIsBus = bus.targetBus.ports.size() > 1;
                if (layoutMode != LayoutMode.SIMPLIFIED && targetIsBus) {
                    lineWidth = BUS_LINE_WIDTH;
                } else {
                    lineWidth = CONNECTOR_LINE_WIDTH;
                }

                int targetColumnIndex = RoutesUtils.getColumnOfNode(bus.targetBus.busNode, layoutState.routes);
                int targetX = RoutesUtils.getColumnRight(targetColumnIndex, layoutState.routes);
                Point firstTargetPoint = new Point(targetX, targetY);

                hasKnot = hasIntermediateLines || bus.sourceBus.ports.size() > 1;
                LineSymbol firstTargetSymbol = new LineSymbol(secondPoint, firstTargetPoint, lineWidth, layoutMode, hasKnot);
                Tags.addTag(connector, firstTargetSymbol);

                previousPoint = firstTargetPoint;

                Point secondTargetPoint = new Point(previousPoint.x, finalPoint.y);
                LineSymbol secondTargetSymbol = new LineSymbol(previousPoint, secondTargetPoint, lineWidth, layoutMode, false);
                Tags.addTag(connector, secondTargetSymbol);

                lineWidth = CONNECTOR_LINE_WIDTH;
                previousPoint = secondTargetPoint;
            }

            if (previousPoint.y != finalPoint.y) {
                int sourceColumnIndex = RoutesUtils.getColumnForXCoordinate(sourcePortX, layoutState.routes);
                int xOffset = RoutesUtils.getOffsetForVerticalLine(sourceColumnIndex, layoutState);
                int x = RoutesUtils.getColumnRight(sourceColumnIndex, layoutState.routes) + xOffset;

                Point firstPoint = new Point(x, previousPoint.y);
                LineSymbol firstSymbol = new LineSymbol(previousPoint, firstPoint, lineWidth, layoutMode, false);
                Tags.addTag(connector, firstSymbol);

                Point secondPoint = new Point(x, finalPoint.y);
                LineSymbol secondSymbol = new LineSymbol(firstPoint, secondPoint, lineWidth, layoutMode, targetPortHasMultipleConnectors);
                Tags.addTag(connector, secondSymbol);

                previousPoint = secondPoint;
            }

            boolean hasKnot = bus != null && bus.targetBus.ports.size() > 1 && layoutMode != LayoutMode.SIMPLIFIED;
            LineSymbol finalSymbol = new LineSymbol(previousPoint, finalPoint, lineWidth, layoutMode, hasKnot, indexInBus);
            Tags.addTag(connector, finalSymbol);
        });

        layoutState.connectorBusses.values().forEach(bus -> {
            bus.sourceBus.xCoordinate = null;
            bus.targetBus.xCoordinate = null;
        });
    }

    private boolean shouldDrawConnector(ConnectorSymbol connectorSymbol) {
        if (layoutMode != LayoutMode.SIMPLIFIED) {
            return true;
        }

        String sourceName = connectorSymbol.getSource();
        String targetName = connectorSymbol.getTarget();
        Pair<String, String> namePair = new Pair<>(sourceName, targetName);

        if (drawnConnections.contains(namePair)) {
            return false;
        } else {
            drawnConnections.add(namePair);
            return true;
        }
    }
}
