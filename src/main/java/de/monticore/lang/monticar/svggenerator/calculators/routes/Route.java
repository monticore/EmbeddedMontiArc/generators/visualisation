/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public class Route {
    public List<RouteState> states;
    public List<Node> nodes;
    public List<Node> openNodes;
    public List<Node> closedNodes;
    private HashMap<Node, PortSymbol> inputPorts;
    private HashMap<Node, PortSymbol> outputPorts;
    public int id;

    public Route() {
        this.states = new ArrayList<>();
        this.nodes = new ArrayList<>();
        this.inputPorts = new HashMap<>();
        this.outputPorts = new HashMap<>();
        init();
    }

    public Route(Route route) {
        states = new ArrayList<>(route.states);
        nodes = new ArrayList<>(route.nodes);
        this.inputPorts = new HashMap<>(route.inputPorts);
        this.outputPorts = new HashMap<>(route.outputPorts);
        init();
    }

    private void init() {
        this.openNodes = new ArrayList<>();
        this.closedNodes = new ArrayList<>();
        this.id = nextRouteId();
    }

    public void addNode(Node node) {
        addNode(node, null, null);
    }

    public void addNode(Node node, PortSymbol inputPort, PortSymbol predecessorOutputPort) {
        states.add(RouteState.NODE);
        nodes.add(node);

        if (inputPort != null) {
            inputPorts.put(node, inputPort);
        }

        if (predecessorOutputPort != null) {
            Node predecessor = getPredecessor(node);
            if (predecessor != null) {
                outputPorts.put(predecessor, predecessorOutputPort);
            }
        }
    }

    public void addState(RouteState state) {
        states.add(state);
        nodes.add(null);
    }

    public void expandAt(int index) {
        if (index == states.size()) {
            states.add(RouteState.EMPTY);
            nodes.add(null);
        } else if (index == 0) {
            states.add(0, RouteState.EMPTY);
            nodes.add(0, null);
        } else {
            RouteState stateBefore = states.get(index - 1);
            RouteState stateAfter = states.get(index);

            if (stateBefore == RouteState.EMPTY || stateAfter == RouteState.EMPTY) {
                states.add(index, RouteState.EMPTY);
            } else if (stateBefore == RouteState.NODE) {
                Node nodeBefore = nodes.get(index - 1);

                if (outputPorts.get(nodeBefore) != null) {
                    states.add(index, RouteState.ROUTE);
                } else {
                    states.add(index, RouteState.EMPTY);
                }
            } else {
                states.add(index, RouteState.ROUTE);
            }
            nodes.add(index, null);
        }
    }

    public boolean isFreeBetweenNodes(Node start, Node end, boolean ignoreTempNodes) {
        int startIndex = columnIndexOf(start);
        int endIndex = columnIndexOf(end);

        int leftIndex = Math.min(startIndex, endIndex);
        int rightIndex = Math.max(startIndex, endIndex);

        if (rightIndex - leftIndex == 1) {
            return true;
        }

        if (startIndex >= 0 && endIndex >= 0) {
            return isFreeBetweenNodes(leftIndex, rightIndex, ignoreTempNodes);
        }

        return false;
    }

    public boolean isFreeBetweenNodes(int start, int end) {
        return isFreeBetweenNodes(start, end, false);
    }

    public boolean isFreeBetweenNodes(int start, int end, boolean ignoreTempNodes) {
        return isFreeBetweenNodes(start, end, ignoreTempNodes, false);
    }

    public boolean isFreeBetweenNodes(int start, int end, boolean ignoreTempNodes, boolean ignoreRoutes) {
        if (getNode(start) != null
                && (!getNode(start).isTemporary || ignoreTempNodes)
                && (getNodeOutputPort(getNode(start)) != null) && !ignoreRoutes) {
            return false;
        } else if (getNode(end) != null
                && (!getNode(end).isTemporary || ignoreTempNodes)
                && (getNodeInputPort(getNode(end)) != null) && !ignoreRoutes) {
            return false;
        } else if (getState(start) == RouteState.ROUTE || getState(end) == RouteState.ROUTE) {
            return false;
        }

        for (int i = start + 1; i < end; i++) {
            RouteState state = getState(i);
            if (state == RouteState.ROUTE && !ignoreRoutes) {
                return false;
            } else if (state == RouteState.NODE) {
                if (!ignoreTempNodes) {
                    return false;
                } else if (!getNode(i).isTemporary) {
                    return false;
                }
            }
        }

        return true;
    }

    public Route copyNodesOnly() {
        Route copy = new Route();

        for (int i = 0; i < states.size(); i++) {
            if (getState(i) == RouteState.NODE) {
                copy.addNode(getNode(i));
            } else {
                copy.addState(RouteState.EMPTY);
            }
        }

        return copy;
    }

    public Route copy() {
        Route copy = new Route();

        for (int i = 0; i < states.size(); i++) {
            if (getState(i) == RouteState.NODE) {
                copy.addNode(getNode(i));
            } else {
                copy.addState(getState(i));
            }
        }

        copy.inputPorts.putAll(inputPorts);
        copy.outputPorts.putAll(outputPorts);

        return copy;
    }

    public RouteState getState(int index) {
        if (index >= states.size()) {
            return RouteState.EMPTY;
        }
        return states.get(index);
    }

    public Node getNode(int index) {
        return nodes.get(index);
    }

    public Node lastNode() {
        for (int i = nodes.size() - 1; i >= 0; i--) {
            if (nodes.get(i) != null) {
                return nodes.get(i);
            }
        }

        return null;
    }

    public int size() {
        return states.size();
    }

    public int nodeCount() {
        return (int) nodes.stream().filter(Objects::nonNull).count();
    }

    public List<Node> getNodes() {
        return nodes.stream().filter(Objects::nonNull).collect(Collectors.toList());
    }

    public void computeOpenAndClosedNodes(List<Node> nodeOrder) {
        openNodes = nodes.stream().filter(Objects::nonNull).collect(Collectors.toList());
        closedNodes.clear();

        for (int i = 0; i < states.size(); i++) {
            RouteState state = states.get(i);
            if (state == RouteState.ROUTE) {
                closedNodes.add(nodeOrder.get(i));
            }
        }
    }

    public void set(int index, RouteState state) {
        states.set(index, state);
        nodes.set(index, null);
    }

    public void set(int index, Node node) {
        states.set(index, RouteState.NODE);
        nodes.set(index, node);
    }

    public void set(int index, Node node, PortSymbol inputPort, PortSymbol outputPort) {
        states.set(index, RouteState.NODE);
        nodes.set(index, node);

        if (inputPort != null) {
            inputPorts.put(node, inputPort);
        } else {
            inputPorts.remove(node);
        }

        if (outputPort != null) {
            outputPorts.put(node, outputPort);
        } else {
            outputPorts.remove(node);
        }
    }

    public void removeInputPortForNode(Node node) {
        inputPorts.remove(node);
    }

    public void removeOutputPortForNode(Node node) {
        outputPorts.remove(node);
    }

    public void remove(int index) {
        states.remove(index);
        nodes.remove(index);
    }

    public void clear(int index) {
        states.set(index, RouteState.EMPTY);

        Node node = nodes.get(index);
        if (node != null) {
            inputPorts.remove(node);
            outputPorts.remove(node);
            nodes.set(index, null);
        }
    }

    public Node getSuccessor(Node node) {
        if (!nodes.contains(node) || outputPorts.get(node) == null) {
            return null;
        }

        for (int i = nodes.indexOf(node) + 1; i < nodes.size(); i++) {
            RouteState state = getState(i);

            if (state == RouteState.EMPTY) {
                return null;
            } else if (state == RouteState.NODE) {
                return getNode(i);
            }
        }

        return null;
    }

    public Node getPredecessor(Node node) {
        if (!nodes.contains(node)) {
            return null;
        }

        for (int i = nodes.indexOf(node) - 1; i >= 0; i--) {
            RouteState state = getState(i);

            if (state == RouteState.EMPTY) {
                return null;
            } else if (state == RouteState.NODE) {
                return getNode(i);
            }
        }

        return null;
    }

    public boolean containsConnection(Node source, Node target) {
        List<Node> nodes = this.nodes.stream().filter(Objects::nonNull).collect(Collectors.toList());

        if (nodes.contains(source) && nodes.contains(target)) {
            if (getNodeOutputPort(source) == null || getNodeInputPort(target) == null) {
                return false;
            }

            int sourceIndex = nodes.indexOf(source);
            int targetIndex = nodes.indexOf(target);

            if (sourceIndex == targetIndex - 1) {
                return true;
            }

            for (int i = sourceIndex + 1; i < targetIndex; i++) {
                if (nodes.get(i) != null) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public boolean containsNode(Node node) {
        return nodes.contains(node);
    }

    public boolean containsNode(Node node, PortSymbol port) {
        if (nodes.contains(node)) {
            return inputPorts.get(node) == port || outputPorts.get(node) == port;
        }

        return false;
    }

    public int columnIndexOf(Node node) {
        return nodes.indexOf(node);
    }

    public PortSymbol getNodeInputPort(Node node) {
        return inputPorts.get(node);
    }

    public PortSymbol getNodeOutputPort(Node node) {
        return outputPorts.get(node);
    }

    public void merge(Route other) {
        for (int i = 0; i < other.size(); i++) {
            RouteState state = other.size() > i ? other.getState(i) : RouteState.EMPTY;

            if (state == RouteState.NODE) {
                set(i, other.getNode(i));
            } else if (state == RouteState.ROUTE) {
                set(i, state);
            }
        }

        inputPorts.putAll(other.inputPorts);
        outputPorts.putAll(other.outputPorts);
    }

    public boolean isNodeConnected(Node node) {
        return nodes.contains(node) && (inputPorts.get(node) != null || outputPorts.get(node) != null);
    }

    public String toString() {
        String message;

        if (id < 10) {
            message = "   " + String.valueOf(id) + ": ";
        } else if (id < 100) {
            message = "  " + String.valueOf(id) + ": ";
        } else if (id < 1000) {
            message = " " + String.valueOf(id) + ": ";
        } else {
            message = String.valueOf(id) + ": ";
        }

        for (int i = 0; i < size(); i++) {
            RouteState state = getState(i);

            switch (state) {
                case EMPTY:
                    message += "   ";
                    break;
                case NODE:
                    message += " " + getNode(i).compName + " ";
                    break;
                case ROUTE:
                    message += "---";
                    break;
            }
        }

        return message;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Route) {
            Route otherRoute = (Route) other;

            if (otherRoute.id != id) {
                return false;
            }

            if (otherRoute.size() != size()) {
                return false;
            }

            for (int i = 0; i < size(); i++) {
                if (otherRoute.getState(i) != getState(i)) {
                    return false;
                }

                if (getState(i) == RouteState.NODE) {
                    Node node = getNode(i);
                    Node otherNode = otherRoute.getNode(i);

                    if (node != null && otherNode != null && !node.equals(otherNode)) {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private static int routeId;

    private static int nextRouteId() {
        return routeId++;
    }
}
