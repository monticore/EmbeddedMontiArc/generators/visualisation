/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

//  de.monticore.lang.monticar.svggenerator.drawing
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 21/10/2016.
//  Copyright (c) 2016 Sven Titgemeyer. All rights reserved.
//

public enum LayoutPosition {
    LEFT,
    RIGHT,
    UNDEFINED,
    ;

    public LayoutPosition next(){
        switch (this){
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            default:
                return LEFT; //start left if previously unknown
        }
    }
}
