/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.routes.BusPart;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TranslatedModel {

    public List<Node> nodes;
    private HashMap<String, ConnectorSymbol> connectorCorrelations;
    private HashMap<String, BusPart> portBuses;

    public TranslatedModel() {
        nodes = new ArrayList<>();
        connectorCorrelations = new HashMap<>();
        portBuses = new HashMap<>();
    }

    public void putCorrelation(PortSymbol sourcePort, PortSymbol targetPort, ConnectorSymbol connector) {
        connectorCorrelations.put(hash(sourcePort, targetPort), connector);
    }

    public ConnectorSymbol getConnector(PortSymbol sourcePort, PortSymbol targetPort) {
        return connectorCorrelations.get(hash(sourcePort, targetPort));
    }

    public void putBus(PortSymbol portSymbol, BusPart bus) {
        portBuses.put(hash(portSymbol), bus);
    }

    public BusPart getBus(PortSymbol portSymbol) {
        return portBuses.get(hash(portSymbol));
    }

    private String hash(PortSymbol sourcePort, PortSymbol targetPort) {
        return sourcePort.getFullName() + " -> " + targetPort.getFullName();
    }

    private String hash(PortSymbol port) {
        return port.getFullName();
    }

}
