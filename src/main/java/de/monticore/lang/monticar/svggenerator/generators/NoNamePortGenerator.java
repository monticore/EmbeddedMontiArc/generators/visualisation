/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;
import de.monticore.lang.monticar.svggenerator.ViewModel.PortViewModel;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasPortLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LayoutPosition;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by Robert on 01.11.2016.
 */
public class NoNamePortGenerator extends InputGenerator {

    // Add the SVG code of all Ports
    @Override
    public void draw(ExpandedComponentInstanceSymbol inst,
                     TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix) {
        LinkedList<PortViewModel> portViewModels = new LinkedList<>();

        // DRAW Canvas Ports
        buildCanvasPortViewModels(inst, portViewModels, LayoutPosition.UNDEFINED);

        // Add reusable port symbol in SVG file with width and height should be 20
        //svgBuilder.append(definePortView(Color.black, PortLayoutSymbol.portWidth,
        //       PortLayoutSymbol.portHeight));

        // DRAW ports of subcomponents
        Collection<ExpandedComponentInstanceSymbol> subComponents = inst.getSubComponents();

        // For all subcomponents:
        subComponents.stream()
                .filter(comp -> Tags.hasTags(comp, ComponentLayoutSymbol.KIND, LayoutMode.NO_PORT_NAMES))
                .forEach(comp -> {
                    ComponentLayoutSymbol symComp = Tags.getFirstLayoutTag(comp, LayoutMode.NO_PORT_NAMES).get();
                    buildPortViewModels(comp, portViewModels, symComp.getLayoutPosition());
                });

        //WTF Java? Why U no cast? http://stackoverflow.com/a/5374359/1857527
        PortViewModel[] modelArray = new PortViewModel[portViewModels.size()];
        modelArray = portViewModels.toArray(modelArray);

        templateBuilder.ports = modelArray;
    }

    private void buildPortViewModels(ExpandedComponentInstanceSymbol inst, LinkedList<PortViewModel> portViewModels, LayoutPosition layoutPosition) {
        Collection<PortSymbol> portsCanvas = inst.getPorts();
        if (portsCanvas != null) {
            // Create SVG code that represents a port
            portsCanvas.stream()
                    .filter(port -> Tags.hasTags(port, PortLayoutSymbol.KIND, LayoutMode.NO_PORT_NAMES))
                    .forEach(port -> {
                        PortLayoutSymbol sym = Tags.getFirstLayoutTag(port, LayoutMode.NO_PORT_NAMES).get();
                        PortViewModel portViewModel = new PortViewModel();
                        portViewModel.x = sym.getX();
                        portViewModel.y = sym.getY();
                        portViewModel.portname = "";
                        portViewModel.isInternal = sym.isInternal();
                        portViewModel.isOutgoing = port.isOutgoing();
                        portViewModel.isIncoming = port.isIncoming();
                        portViewModel.layoutPosition = layoutPosition;
                        portViewModels.add(portViewModel);
                    });
        }
    }

    private void buildCanvasPortViewModels(ExpandedComponentInstanceSymbol inst, LinkedList<PortViewModel> portViewModels, LayoutPosition layoutPosition) {
        Collection<PortSymbol> portsCanvas = inst.getPorts();
        if (portsCanvas != null) {
            // Create SVG code that represents a port
            portsCanvas.stream()
                    .filter(port -> Tags.hasTags(port, CanvasPortLayoutSymbol.KIND, LayoutMode.NO_PORT_NAMES))
                    .forEach(port -> {
                        CanvasPortLayoutSymbol sym = Tags.getFirstCanvasLayoutTag(port, LayoutMode.NO_PORT_NAMES).get();
                        PortViewModel portViewModel = new PortViewModel();
                        portViewModel.x = sym.getX();
                        portViewModel.y = sym.getY();
                        portViewModel.portname = "";
                        portViewModel.isInternal = true;
                        portViewModel.isOutgoing = port.isOutgoing();
                        portViewModel.isIncoming = port.isIncoming();
                        portViewModel.layoutPosition = layoutPosition;
                        portViewModels.add(portViewModel);

                        //drawPortName(svgBuilder, port, LayoutPosition.UNDEFINED);
                    });
        }
    }
}

