/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.helper.NodeNameGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public class Node {
    public int indexInOrder;
    public String compName;
    public boolean isTemporary;
    private List<Node> inputs;
    private List<Node> outputs;
    private HashMap<Node, List<PortSymbol>> inputPorts;
    private HashMap<Node, List<PortSymbol>> outputPorts;
    private ExpandedComponentInstanceSymbol component;

    public static Node newDummy(Node sourceNode) {
        return new Node(null, NodeNameGenerator.nextTempNodesName(sourceNode), true);
    }

    public Node(ExpandedComponentInstanceSymbol component) {
        this.component = component;
        this.compName = component.getName();
        this.inputs = new ArrayList<>();
        this.outputs = new ArrayList<>();
        this.inputPorts = new HashMap<>();
        this.outputPorts = new HashMap<>();
        this.isTemporary = false;
    }

    public Node(ExpandedComponentInstanceSymbol component, String compName) {
        this.component = component;
        this.compName = compName;
        this.inputs = new ArrayList<>();
        this.outputs = new ArrayList<>();
        this.inputPorts = new HashMap<>();
        this.outputPorts = new HashMap<>();
        this.isTemporary = false;
    }

    public Node(ExpandedComponentInstanceSymbol component, String compName, boolean isTemporary) {
        this.component = component;
        this.compName = compName;
        this.inputs = new ArrayList<>();
        this.outputs = new ArrayList<>();
        this.inputPorts = new HashMap<>();
        this.outputPorts = new HashMap<>();
        this.isTemporary = isTemporary;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Node) {
            return compName.equals(((Node) other).compName);
        }

        return false;
    }

    public ExpandedComponentInstanceSymbol getComponent() {
        return component;
    }

    public void addInput(Node node, PortSymbol port) {
        inputs.add(node);

        List<PortSymbol> ports = inputPorts.getOrDefault(node, new ArrayList<>());
        ports.add(port);
        inputPorts.put(node, ports);
    }

    public void addOutput(Node node, PortSymbol port) {
        outputs.add(node);

        List<PortSymbol> ports = outputPorts.getOrDefault(node, new ArrayList<>());
        ports.add(port);
        outputPorts.put(node, ports);
    }

    public List<Node> getInputs() {
        return inputs;
    }

    public List<Node> getOutputs() {
        return outputs;
    }

    public PortSymbol getFirstPortForInput(Node inputNode) {
        if (inputPorts.containsKey(inputNode)) {
            return inputPorts.get(inputNode).get(0);
        } else {
            return null;
        }
    }

    public PortSymbol getFirstPortForOutput(Node outputNode) {
        if (outputPorts.containsKey(outputNode)) {
            return outputPorts.get(outputNode).get(0);
        } else {
            return null;
        }
    }

    public List<PortSymbol> getAllPortsForInput(Node inputNode) {
        if (inputPorts.containsKey(inputNode)) {
            return inputPorts.get(inputNode);
        } else {
            return new ArrayList<>();
        }
    }

    public List<PortSymbol> getAllPortsForOutput(Node outputNode) {
        if (outputPorts.containsKey(outputNode)) {
            return outputPorts.get(outputNode);
        } else {
            return new ArrayList<>();
        }
    }

    public boolean isConnected() {
        return !(inputPorts.isEmpty() && outputPorts.isEmpty());
    }

    public int getNumberOfPorts() {
        return inputPorts.size() + outputPorts.size();
    }

    @Override
    public int hashCode() {
        return compName.hashCode();
    }

    @Override
    public String toString() {
        return String.valueOf(indexInOrder);
    }
}
