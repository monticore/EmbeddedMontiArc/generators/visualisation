/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.NodeExtent;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.routes.RouteState;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class RoutesUtils implements DrawingConstants {

    public static TableDimensions tableDimensions;

    public static Route getRouteForConnection(Node source, Node target, RoutesLayoutState layoutState) {
        return getRouteForConnection(source, target, layoutState.routes, 0);
    }

    public static Route getRouteForConnection(Node source, Node target, RoutesLayoutState layoutState, int index) {
        return getRouteForConnection(source, target, layoutState.routes, index);
    }

    public static Route getRouteForConnection(Node source, Node target, List<Route> routes) {
        return getRouteForConnection(source, target, routes, 0);
    }

    private static Route getRouteForConnection(Node source, Node target, List<Route> routes, int index) {
        Route lastRoute = null;
        int counter = 0;

        for (Route route : routes) {
            if (route.containsConnection(source, target)) {
                if (counter == index) {
                    return route;
                }

                lastRoute = route;
                counter += 1;
            }
        }

        return lastRoute;
    }

    public static int getColumnOfNode(Node node, List<Route> routes) {
        int index = -1;

        for (Route route: routes) {
            if (route.containsNode(node)) {
                return route.columnIndexOf(node);
            }
        }

        return index;
    }

    public static int getOffsetForVerticalLine(int columnIndex, RoutesLayoutState layoutState) {
        int indexOfVerticalLine = layoutState.verticalLinesAfterColumn.getOrDefault(columnIndex, 0);
        layoutState.verticalLinesAfterColumn.put(columnIndex, indexOfVerticalLine + 1);

        int startOffset = (int) (COLUMN_SPACING / 2.0);
        return startOffset + indexOfVerticalLine * VERTICAL_LINES_SPACING;
    }

    static Route addRowAt(int index, RoutesLayoutState layoutState, Node... nodesToInclude) {
        Route route = new Route();
        Route above = null;
        Route below = null;
        List<Node> nodesToIncludeList = Stream.of(nodesToInclude).collect(Collectors.toList());
        int length = 0;

        if (index > 0) {
            above = layoutState.routes.get(index - 1);
            length = above.size();
        }

        if (index < layoutState.routes.size()) {
            below = layoutState.routes.get(index);
            length = below.size();
        }

        for (int i = 0; i < length; i++) {
            RouteState stateAbove = above != null ? above.getState(i) : RouteState.EMPTY;
            RouteState stateBelow = below != null ? below.getState(i) : RouteState.EMPTY;

            if (stateAbove == RouteState.NODE && stateBelow == RouteState.NODE) {
                Node nodeAbove = above.getNode(i);
                Node nodeBelow = below.getNode(i);
                if (nodeAbove.equals(nodeBelow)) {
                    route.addNode(nodeAbove);
                } else if (nodesToIncludeList.contains(nodeAbove)) {
                    route.addNode(nodeAbove);
                } else if (nodesToIncludeList.contains(nodeBelow)) {
                    route.addNode(nodeBelow);
                } else {
                    route.addState(RouteState.EMPTY);
                }

            } else if (stateAbove == RouteState.NODE) {
                Node nodeAbove = above.getNode(i);
                if (nodesToIncludeList.contains(nodeAbove)) {
                    route.addNode(nodeAbove);
                } else {
                    route.addState(RouteState.EMPTY);
                }
            } else if (stateBelow == RouteState.NODE) {
                Node nodeBelow = below.getNode(i);
                if (nodesToIncludeList.contains(nodeBelow)) {
                    route.addNode(nodeBelow);
                } else {
                    route.addState(RouteState.EMPTY);
                }
            } else {
                route.addState(RouteState.EMPTY);
            }
        }

        if (index >= layoutState.routes.size()) {
            layoutState.routes.add(route);
        } else {
            layoutState.routes.add(index, route);
        }

        return route;
    }

    public static int getColumnWidth(int column, List<Route> routes) {
        if (column == 0) {
            return 0;
        }

        int maxWidth = 0;

        for (Route route : routes) {
            if (column < route.size()) {
                Node node = route.getNode(column);
                maxWidth = Math.max(maxWidth, tableDimensions.getNodeWidth(node));
            }
        }

        return maxWidth;
    }

    public static int getColumnLeft(int column, List<Route> routes) {
        if (column <= 0) {
            return tableDimensions.getCanvasMarginLeft();
        }

        int left = tableDimensions.getCanvasMarginLeft() + tableDimensions.getColumnSpacingLeft(1);

        for (int i = 1; i < column; i++) {
            left += getColumnWidth(i, routes) + tableDimensions.getColumnSpacingRight(i);
        }

        return left;
    }

    public static int getColumnRight(int column, List<Route> routes) {
        if (column == -1) {
            return getColumnLeft(-1, routes);
        }

        return getColumnLeft(column, routes) + getColumnWidth(column, routes);
    }

    public static int getColumnCenter(int column, List<Route> routes) {
        if (column == -1) {
            return getColumnLeft(-1, routes);
        }

        return getColumnLeft(column, routes) + (int) (getColumnWidth(column, routes) / 2.0);
    }

    public static int getRowHeight(int row) {
        return tableDimensions.getRowHeight(row);
    }

    public static int getRowTop(int row) {
        int top = tableDimensions.getCanvasMarginTop() + TOP_ROW_EXTRA_SPACING + tableDimensions.getSpacingAboveRow(0);
        for (int i = 0; i < row; i++) {
            top += getRowHeight(i) + tableDimensions.getSpacingBelowRow(i);
        }
        return top;
    }

    static int getRowBottom(int row) {
        return getRowTop(row) + getRowHeight(row);
    }

    static int getRowCenter(int row) {
        return getRowBottom(row) - (int) (ROW_HEIGHT / 2.0);
    }

    static int getTableWidth(List<Route> routes) {
        int length = routes.stream().mapToInt(Route::size).max().orElse(0);
        return getColumnRight(Math.max(0, length - 2), routes) + tableDimensions.getColumnSpacingRight(length - 2) - tableDimensions.getCanvasMarginLeft();
    }

    static int getTableHeight(List<Route> routes) {
        return getRowBottom(routes.size() - 1) + tableDimensions.getSpacingBelowRow(routes.size() - 1) - tableDimensions.getCanvasMarginTop();
    }

    static int getColumnCount(RoutesLayoutState layoutState) {
        return getColumnCount(layoutState.routes);
    }

    public static int getColumnCount(List<Route> routes) {
        return routes.stream().mapToInt(Route::size).max().orElse(0);
    }

    static boolean isColumnEmpty(int columnIndex, List<Route> routes) {
        for (Route route : routes) {
            RouteState state = route.getState(columnIndex);

            if (state == RouteState.NODE) {
                return false;
            }
        }

        return true;
    }

    static boolean isNodeSplit(Node node, RoutesLayoutState layoutState) {
        int nodeColumnIndex = layoutState.nodeOrder.indexOf(node);
        boolean nodeHasStarted = false;
        boolean nodeHasEnded = false;

        for(Route route: layoutState.routes) {
            RouteState state = route.getState(nodeColumnIndex);

            if (state == RouteState.NODE) {
                if (nodeHasEnded) {
                    return true;
                }
                nodeHasStarted = true;
            } else if (state == RouteState.ROUTE) {
                if (nodeHasStarted) {
                    nodeHasEnded = true;
                }
            }
        }

        return false;
    }

    static NodeExtent getNextNodeExtent(int columnIndex, int startRowIndex, List<Route> routes) {
        NodeExtent nodeExtent = new NodeExtent();

        for (int i = startRowIndex; i < routes.size(); i++) {
            Route route = routes.get(i);
            if (route.getState(columnIndex) == RouteState.NODE) {
                if (nodeExtent.node == null) {
                    nodeExtent.node = route.getNode(columnIndex);
                    nodeExtent.topRowIndex = i;
                    nodeExtent.bottomRowIndex = i;
                } else if (nodeExtent.node.equals(route.getNode(columnIndex))) {
                    nodeExtent.bottomRowIndex = i;
                }
            }
        }

        return nodeExtent;
    }

    static int getColumnForXCoordinate(int x, List<Route> routes) {
        int columnCount = getColumnCount(routes);
        int searchX = getColumnLeft(0, routes);

        if (searchX > x) {
            return -1;
        }

        for (int i = 1; i < columnCount; i++) {
            searchX = getColumnRight(i, routes);

            if (searchX > x) {
                return i - 1;
            }
        }

        return columnCount;
    }
}
