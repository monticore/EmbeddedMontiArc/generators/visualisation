/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class RoutesBuilder implements DrawingConstants {

    private RoutesLayoutState layoutState;
    private TranslatedModel translatedModel;

    public RoutesBuilder(RoutesLayoutState layoutState, TranslatedModel translatedModel) {
        this.layoutState = layoutState;
        this.translatedModel = translatedModel;
    }

    public List<Route> findRoutes() {
        List<Node> nodes = translatedModel.nodes;
        Node rootIn = nodes.stream().filter(node -> node.compName.equals(NAME_ROOT_IN)).findFirst().orElseGet(null);
        Node rootOut = nodes.stream().filter(node -> node.compName.equals(NAME_ROOT_OUT)).findFirst().orElseGet(null);
        List<Route> routes = new ArrayList<>();

        if (rootIn == null) {
            System.err.println("Could not find in-port of root node. Aborting.");
            return routes;
        }

        layoutState.nodeOrder.add(rootOut);

        List<Node> nodesToBeVisited = new ArrayList<>(nodes);
        List<Node> visitedNodes = new ArrayList<>();
        Node currentNode = rootIn;
        int startIndex = 0;
        while (currentNode != null) {
            Route route = new Route();
            route.addNode(currentNode, null, null);

            StepTransition transition = buildRoutes(route, startIndex, visitedNodes);

            routes.addAll(transition.foundRoutes);

            for (Node visited : transition.visitedNodes) {
                if (!visitedNodes.contains(visited)) {
                    visitedNodes.add(visited);
                }
            }

            nodesToBeVisited.remove(currentNode);
            nodesToBeVisited.removeAll(visitedNodes);

            if (!nodesToBeVisited.isEmpty()) {
                currentNode = nodesToBeVisited.remove(0);
            } else {
                currentNode = null;
            }

            startIndex = 1;
        }

        return routes;
    }

    private StepTransition buildRoutes(Route route, int startIndex, List<Node> visitedNodes) {
        List<Route> routes = new ArrayList<>();
        Node last = route.lastNode();
        int indexOffset = 0;

        if (last == null) {
            routes.add(route);
            return new StepTransition(routes, visitedNodes);
        } else if (visitedNodes.contains(last)) {
            routes.add(route);
            return new StepTransition(routes, visitedNodes);
        }

        if (!layoutState.nodeOrder.contains(last)) {
            if (startIndex == layoutState.nodeOrder.size()) {
                layoutState.nodeOrder.add(last);
            } else {
                layoutState.nodeOrder.add(startIndex, last);
            }
        }

        if (last.getOutputs().isEmpty()) {
            routes.add(route);
            return new StepTransition(routes, visitedNodes);
        }

        if (!visitedNodes.contains(last)) {
            visitedNodes.add(last);
        }

        boolean first = true;
        List<Node> sortedOutputs = last.getOutputs().stream()
                .sorted(Comparator.comparingInt(n -> -n.getOutputs().size()))
                .collect(Collectors.toList());

        for (Node next : sortedOutputs) {
            if (addToBusIfPossible(last, next)) {
                continue;
            }

            Route newRoute;
            if (first) {
                newRoute = new Route(route);
                first = false;
            } else {
                newRoute = new Route();
                newRoute.addNode(last, null, null);
            }

            if (layoutState.nodeOrder.contains(next) && layoutState.nodeOrder.indexOf(next) <= layoutState.nodeOrder.indexOf(last)) {
                StepTransition transition = createBackConnector(last, next, newRoute);

                if (newRoute.size() > 1) {
                    routes.add(newRoute);
                }
                routes.addAll(transition.foundRoutes);

                for (Node visited : transition.visitedNodes) {
                    if (!visitedNodes.contains(visited)) {
                        visitedNodes.add(visited);
                    }
                }

                indexOffset += transition.indexOffset;
                continue;
            }

            createBusNodes(last, next, routes);

            Pair<Node, Node> nodePair = new Pair<>(last, next);
            Bus bus = layoutState.busses.get(nodePair);

            if (bus.sourceBus.parentBuses.size() == 1) {
                newRoute.addNode(bus.sourceBus.busNode, DummyPort.create(), DummyPort.create());
            } else {
                int indexOfLastNode = newRoute.columnIndexOf(last);
                newRoute.set(indexOfLastNode, RouteState.EMPTY);

                newRoute.addNode(bus.sourceBus.busNode, null, null);
            }

            if (bus.targetBus.parentBuses.size() == 1) {
                newRoute.addNode(bus.targetBus.busNode, DummyPort.create(), DummyPort.create());
            } else {
                newRoute.addNode(bus.targetBus.busNode, null, null);
            }
            newRoute.addNode(next, next.getFirstPortForInput(last), last.getFirstPortForOutput(next));

            int newStartIndex = layoutState.nodeOrder.indexOf(bus.targetBus.busNode) + 1;

            StepTransition transition = buildRoutes(newRoute, newStartIndex, visitedNodes);
            routes.addAll(transition.foundRoutes);

            for (Node visited : transition.visitedNodes) {
                if (!visitedNodes.contains(visited)) {
                    visitedNodes.add(visited);
                }
            }
        }

        return new StepTransition(routes, visitedNodes, indexOffset);
    }


    private void createBusNodes(Node source, Node target, List<Route> routes) {
        Pair<Node, Node> pair = new Pair<>(source, target);

        Bus bus;
        PortSymbol sourcePort;
        PortSymbol targetPort;

        bus = new Bus(source, target);
        sourcePort = source.getFirstPortForOutput(target);
        targetPort = target.getFirstPortForInput(source);
        BusPart sourcePortBus = translatedModel.getBus(sourcePort);
        BusPart targetPortBus = translatedModel.getBus(targetPort);

        if (sourcePortBus != null) {
            bus.setSourceBus(sourcePortBus);
        } else {
            bus.sourceBus.busNode = Node.newDummy(source);

            int tempNodeIndex = layoutState.nodeOrder.indexOf(source) + 1;
            layoutState.nodeOrder.add(tempNodeIndex, bus.sourceBus.busNode);

            for (Route route : routes) {
                if (route.containsNode(source, sourcePort)) {
                    tempNodeIndex = route.columnIndexOf(source) + 1;
                    route.expandAt(tempNodeIndex);
                    route.set(tempNodeIndex, bus.sourceBus.busNode, DummyPort.create(), DummyPort.create());
                }
            }
        }

        if (targetPortBus != null) {
            bus.setTargetBus(targetPortBus);
        } else {
            bus.targetBus.busNode = Node.newDummy(target);

            int tempNodeIndex = layoutState.nodeOrder.indexOf(bus.sourceBus.busNode) + 1;
            layoutState.nodeOrder.add(tempNodeIndex, bus.targetBus.busNode);

            for (Route route : routes) {
                if (route.containsNode(target, targetPort)) {
                    tempNodeIndex = route.columnIndexOf(target);
                    route.expandAt(tempNodeIndex);
                    route.set(tempNodeIndex, bus.targetBus.busNode, DummyPort.create(), DummyPort.create());
                }
            }
        }

        bus.sourceBus.addPort(sourcePort);
        bus.targetBus.addPort(targetPort);
        layoutState.busses.put(pair, bus);
        translatedModel.putBus(sourcePort, bus.sourceBus);
        translatedModel.putBus(targetPort, bus.targetBus);
        bus.portConnections.add(new Pair<>(sourcePort, targetPort));

        ConnectorSymbol connector = translatedModel.getConnector(sourcePort, targetPort);
        layoutState.connectorBusses.put(connector, bus);
    }

    private boolean addToBusIfPossible(Node source, Node target) {
        Pair<Node, Node> pair = new Pair<>(source, target);
        int portIndex = layoutState.visitedConnections.getOrDefault(pair, 0);
        layoutState.visitedConnections.put(pair, portIndex + 1);

        Bus bus;
        PortSymbol sourcePort;
        PortSymbol targetPort;

        if (layoutState.busses.containsKey(pair)) {
            bus = layoutState.busses.get(pair);
            sourcePort = source.getAllPortsForOutput(target).get(portIndex);
            targetPort = target.getAllPortsForInput(source).get(portIndex);
        } else {
            return false;
        }

        bus.sourceBus.addPort(sourcePort);
        bus.targetBus.addPort(targetPort);
        layoutState.busses.put(pair, bus);
        translatedModel.putBus(sourcePort, bus.sourceBus);
        translatedModel.putBus(targetPort, bus.targetBus);
        bus.portConnections.add(new Pair<>(sourcePort, targetPort));

        ConnectorSymbol connector = translatedModel.getConnector(sourcePort, targetPort);
        layoutState.connectorBusses.put(connector, bus);

        return true;
    }

    private StepTransition createBackConnector(Node source, Node target, Route route) {
        List<Route> newRoutes = new ArrayList<>();
        int indexOffset = 0;
        Node tempNodeTarget;
        Node tempNodeSource;
        PortSymbol sourceOutputPort = source.getFirstPortForOutput(target);
        PortSymbol targetInputPort = target.getFirstPortForInput(source);

        List<Node> addedNodes = new ArrayList<>();
        addedNodes.add(target);
        addedNodes.add(source);

        Bus bus = new Bus(source, target);
        BusPart sourceBusPart = translatedModel.getBus(sourceOutputPort);
        BusPart targetBusPart = translatedModel.getBus(targetInputPort);

        if (sourceBusPart != null) {
            bus.setSourceBus(sourceBusPart);
            tempNodeSource = bus.sourceBus.busNode;
        } else {
            tempNodeSource = new Node(null, NodeNameGenerator.nextTempNodesName(source), true);
            int tempNodeLeftIndex = layoutState.nodeOrder.indexOf(source) + 1;
            if (tempNodeLeftIndex <= layoutState.nodeOrder.size()) {
                layoutState.nodeOrder.add(tempNodeLeftIndex, tempNodeSource);
            } else {
                layoutState.nodeOrder.add(tempNodeSource);
            }

            bus.sourceBus.busNode = tempNodeSource;
            addedNodes.add(tempNodeSource);
        }

        if (targetBusPart != null) {
            bus.setTargetBus(targetBusPart);
            tempNodeTarget = bus.targetBus.busNode;
        } else {
            tempNodeTarget = new Node(null, NodeNameGenerator.nextTempNodesName(target), true);
            layoutState.nodeOrder.add(layoutState.nodeOrder.indexOf(target), tempNodeTarget);
            indexOffset = 1;

            bus.targetBus.busNode = tempNodeTarget;
            addedNodes.add(tempNodeTarget);
        }

        tempNodeTarget.addOutput(target, DummyPort.create());
        tempNodeTarget.addOutput(tempNodeSource, DummyPort.create());
        source.addOutput(tempNodeSource, sourceOutputPort);

        target.addInput(tempNodeTarget, targetInputPort);
        tempNodeSource.addInput(tempNodeTarget, DummyPort.create());
        tempNodeSource.addInput(source, DummyPort.create());

        if (bus.targetBus.ports.isEmpty()) {
            Route targetRoute = new Route();
            targetRoute.addNode(tempNodeTarget, null, null);
            targetRoute.addNode(target, targetInputPort, DummyPort.create());
            newRoutes.add(targetRoute);
        }
        bus.targetBus.addPort(targetInputPort);

        Route middleRoute = new Route();
        middleRoute.addNode(tempNodeTarget);
        middleRoute.addNode(tempNodeSource, DummyPort.create(), DummyPort.create());
        newRoutes.add(middleRoute);

        if (bus.sourceBus.ports.isEmpty()) {
            Route sourceRoute = new Route();
            newRoutes.add(sourceRoute);
            sourceRoute.addNode(source, null, DummyPort.create());
            sourceRoute.addNode(tempNodeSource, DummyPort.create(), sourceOutputPort);
        }

        bus.sourceBus.addPort(sourceOutputPort);
        bus.portConnections.add(new Pair<>(sourceOutputPort, targetInputPort));

        ConnectorSymbol connector = translatedModel.getConnector(sourceOutputPort, targetInputPort);
        layoutState.connectorBusses.put(connector, bus);

        layoutState.busses.put(new Pair<>(source, target), bus);
        translatedModel.putBus(sourceOutputPort, bus.sourceBus);
        translatedModel.putBus(targetInputPort, bus.targetBus);

        return new StepTransition(newRoutes, addedNodes, indexOffset);
    }

    private static class StepTransition {
        List<Node> visitedNodes;
        List<Route> foundRoutes;
        int indexOffset;

        private StepTransition(List<Route> foundRoutes, List<Node> visitedNodes) {
            this.visitedNodes = visitedNodes;
            this.foundRoutes = foundRoutes;
            this.indexOffset = 0;
        }

        private StepTransition(List<Route> foundRoutes, List<Node> visitedNodes, int indexOffset) {
            this.visitedNodes = visitedNodes;
            this.foundRoutes = foundRoutes;
            this.indexOffset = indexOffset;
        }
    }
}
