/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;//

import java.util.HashMap;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class CanvasViewModel extends ViewModel {

    public int x;
    public int y;
    public int id;
    public int height;
    public int width;
    public String name;

    @Override
    public HashMap<String, Object> hashMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("X", this.x);
        map.put("Y", this.y);
        map.put("id", this.id);
        map.put("height", this.height);
        map.put("width", this.width);
        map.put("name", this.name);

        return map;
    }

    public static class HierarchyElement {
        public String name;
        public String fullName;

        public HierarchyElement(String name, String fullName) {
            this.name = name;
            this.fullName = fullName;
        }
    }
}
