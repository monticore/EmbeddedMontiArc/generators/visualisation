/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.IdGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class ComponentCoordinatesHandler implements DrawingConstants {

    private LayoutMode layoutMode;

    public ComponentCoordinatesHandler(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    public void assignComponentCoordinates(ExpandedComponentInstanceSymbol enclosingComponent, List<Route> routes) {
        int length = routes.stream().mapToInt(Route::size).max().orElse(0);

        assignCanvasLayoutSymbol(enclosingComponent, routes);

        for (int columnIndex = 0; columnIndex < length; columnIndex++) {
            assignComponentCoordinatesForColumn(columnIndex, routes);
        }
    }

    private void assignCanvasLayoutSymbol(ExpandedComponentInstanceSymbol enclosingComponent, List<Route> routes) {
        int tableWidth = RoutesUtils.getTableWidth(routes);
        int tableHeight = RoutesUtils.getTableHeight(routes);
        int x = RoutesUtils.tableDimensions.getCanvasMarginLeft();
        int y = RoutesUtils.tableDimensions.getCanvasMarginTop();

        Tags.addTag(enclosingComponent, new CanvasLayoutSymbol(IdGenerator.getUniqueId(), x, y, tableWidth, tableHeight, layoutMode));
    }

    private void assignComponentCoordinatesForColumn(int columnIndex, List<Route> routes) {
        Node currentNode = null;
        List<PortSymbol> ports = new ArrayList<>();
        int startRowIndex = -1;
        int endRowIndex = -1;

        for (int rowIndex = 0; rowIndex < routes.size(); rowIndex++) {
            Route route = routes.get(rowIndex);
            if (route.getState(columnIndex) == RouteState.NODE) {
                Node node = route.getNode(columnIndex);
                PortSymbol inputPort = route.getNodeInputPort(node);
                PortSymbol outputPort = route.getNodeOutputPort(node);
                boolean hasNewPorts = (inputPort != null && !ports.contains(inputPort) || outputPort != null && !ports.contains(outputPort));

                if (!hasNewPorts) {
                    if (!node.isConnected()) {
                        assignComponentCoordinates(node, columnIndex, rowIndex, rowIndex, routes);
                    }
                    continue;
                }

                if (inputPort != null) {
                    ports.add(inputPort);
                }

                if (outputPort != null) {
                    ports.add(outputPort);
                }

                if (currentNode == null) {
                    currentNode = node;
                    startRowIndex = rowIndex;
                } else if (!currentNode.equals(node)) {
                    assignComponentCoordinates(currentNode, columnIndex, startRowIndex, endRowIndex, routes);

                    currentNode = node;
                    startRowIndex = rowIndex;
                }

                endRowIndex = rowIndex;
            } else if (currentNode != null) {
                if (!currentNode.compName.equals(NAME_ROOT_IN) && !currentNode.compName.equals(NAME_ROOT_OUT)) {
                    assignComponentCoordinates(currentNode, columnIndex, startRowIndex, endRowIndex, routes);
                }

                currentNode = null;
                startRowIndex = -1;
                endRowIndex = -1;
            }
        }

        if (currentNode != null) {
            assignComponentCoordinates(currentNode, columnIndex, startRowIndex, endRowIndex, routes);
        }
    }

    private void assignComponentCoordinates(Node node, int column, int startRow, int endRow, List<Route> routes) {
        ExpandedComponentInstanceSymbol component = node.getComponent();

        if (component == null) {
            return;
        }

        int left = RoutesUtils.getColumnLeft(column, routes);
        int top = RoutesUtils.getRowTop(startRow);
        int width = RoutesUtils.getColumnRight(column, routes) - left;
        int height = RoutesUtils.getRowBottom(endRow) - top;

        Tags.addTag(component, new ComponentLayoutSymbol(IdGenerator.getUniqueId(), left, top, width, height, LayoutPosition.LEFT, layoutMode));
    }
}
