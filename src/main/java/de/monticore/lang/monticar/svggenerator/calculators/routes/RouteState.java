/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public enum RouteState {
    NODE, ROUTE, EMPTY
}
