/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.helper.*;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Bus;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by Peters Notebook on 31.05.2017.
 */
public class RoutesLayoutCalculator implements DrawingConstants {

    public RoutesLayoutState layoutState;
    public TranslatedModel translatedModel;
    private RouteOrderCalculator orderCalculator;

    public RoutesLayoutCalculator() {
        orderCalculator = new SimulatedAnnealingRouteOrderCalculator(2, 50000);
    }

    public RoutesLayoutCalculator(RouteOrderCalculator orderCalculator) {
        this.orderCalculator = orderCalculator;
    }

    public void calculateLayout(ExpandedComponentInstanceSymbol inst) {
        layoutState = new RoutesLayoutState();
        layoutState.componentInstanceSymbol = inst;

        translatedModel = ModelTranslator.translateModel(inst);
        RoutesBuilder routesBuilder = new RoutesBuilder(layoutState, translatedModel);

        layoutState.routes = fixRouteIndices(routesBuilder.findRoutes());
        setNodeIndices(layoutState);

        layoutState.routes = orderCalculator.calculateRouteOrder(layoutState);

        handleSingletonLayout();

        BusHandler busHandler = new BusHandler(layoutState, translatedModel);
        busHandler.insertBuses();

        fillNodeGaps(layoutState.routes);

        TableSizeOptimizer.optimizeVertically(layoutState);

        new SplitHandler(layoutState, translatedModel).removeSplits();

        TableSizeOptimizer.optimizeHorizontally(layoutState);

        setDetourIndices(layoutState, LayoutMode.NORMAL);

        // Set coordinates for normal mode
        TableDimensions normalTableDimensions = new TableDimensions(LayoutMode.NORMAL);
        normalTableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = normalTableDimensions;

        new ComponentCoordinatesHandler(LayoutMode.NORMAL).assignComponentCoordinates(inst, layoutState.routes);
        new PortCoordinatesHandler(LayoutMode.NORMAL).assignPortCoordinates(layoutState);
        new ConnectorCoordinatesHandler(layoutState, LayoutMode.NORMAL).assignConnectorCoordinates(inst);

//        System.out.println("### NO_PORT_NAMES");
//      Set coordinates for no-port-names mode
        layoutState.verticalLinesAfterColumn.clear();
        TableDimensions noPortNamesTableDimensions = new TableDimensions(LayoutMode.NO_PORT_NAMES);
        noPortNamesTableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = noPortNamesTableDimensions;

        new ComponentCoordinatesHandler(LayoutMode.NO_PORT_NAMES).assignComponentCoordinates(inst, layoutState.routes);
        new PortCoordinatesHandler(LayoutMode.NO_PORT_NAMES).assignPortCoordinates(layoutState);
        new ConnectorCoordinatesHandler(layoutState, LayoutMode.NO_PORT_NAMES).assignConnectorCoordinates(inst);

//        System.out.println("\n\nUndrawn stuff:");
//        Log.printUndrawnComponents(layoutState);
//        Log.printUndrawnPorts(layoutState);
//        Log.printUndrawnConnectors(layoutState);
    }

    private void handleSingletonLayout() {
//        System.out.println("### SIMPLIFIED");
        RoutesLayoutState layoutState = this.layoutState.clone();

        fillNodeGaps(layoutState.routes);
        new BusHandler(layoutState, translatedModel).removeBuses();

        TableSizeOptimizer.optimizeVertically(layoutState);
        SplitHandler splitHandler = new SplitHandler(layoutState, translatedModel);
        splitHandler.removeSplits();
        TableSizeOptimizer.optimizeHorizontally(layoutState);
        setDetourIndices(layoutState, LayoutMode.SIMPLIFIED);

        TableDimensions tableDimensions = new TableDimensions(LayoutMode.SIMPLIFIED);
        tableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = tableDimensions;

//        Log.printRoutes("simplified final: ", layoutState);

        new ComponentCoordinatesHandler(LayoutMode.SIMPLIFIED).assignComponentCoordinates(layoutState.componentInstanceSymbol, layoutState.routes);
        new PortCoordinatesHandler(LayoutMode.SIMPLIFIED).assignPortCoordinates(layoutState);

        ConnectorCoordinatesHandler connectorCoordinatesHandler = new ConnectorCoordinatesHandler(layoutState, LayoutMode.SIMPLIFIED);
        connectorCoordinatesHandler.assignConnectorCoordinates(layoutState.componentInstanceSymbol);
//        System.out.println("### SIMPLIFIED DONE");

        layoutState.busses.values().forEach(bus -> bus.removed = false);
    }

    private List<Route> fixRouteIndices(List<Route> routes) {
        for (Route route : routes) {
            int index = 0;

            while (index < route.size()) {
                if (route.getState(index) == RouteState.NODE) {
                    Node node = route.getNode(index);
                    int orderIndex = layoutState.nodeOrder.indexOf(node);

                    for (int i = 0; index + i < orderIndex; i++) {
                        route.expandAt(index);
                    }
                }

                index++;
            }
        }

        int length = routes.stream().mapToInt(Route::size).max().orElse(0);

        for (Route route : routes) {
            while (route.size() < length) {
                route.addState(RouteState.EMPTY);
            }
        }

        return routes;
    }

    private void setNodeIndices(RoutesLayoutState layoutState) {
        for (int i = 0; i < layoutState.nodeOrder.size(); i++) {
            layoutState.nodeOrder.get(i).indexInOrder = i;
        }
    }

    private void fillNodeGaps(List<Route> routes) {
        int length = routes.stream().mapToInt(Route::size).max().orElse(0);

        for (int column = 0; column < length; column++) {
            Node columnNode = layoutState.nodeOrder.get(column);
            List<Integer> indicesToUpdate = new ArrayList<>();
            boolean nodeHasStarted = false;

            for (int routeIndex = 0; routeIndex < routes.size(); routeIndex++) {
                Route route = routes.get(routeIndex);
                RouteState state = route.getState(column);

                if (state == RouteState.NODE) {
                    nodeHasStarted = true;

                    while (!indicesToUpdate.isEmpty()) {
                        int index = indicesToUpdate.remove(0);
                        routes.get(index).set(column, columnNode);
                    }
                } else if (state == RouteState.EMPTY && nodeHasStarted) {
                    indicesToUpdate.add(routeIndex);
                }
            }
        }
    }

    private void setDetourIndices(RoutesLayoutState layoutState, LayoutMode layoutMode) {
        List<Route> routes = layoutState.routes;
        for (Bus bus: layoutState.connectorBusses.values()) {
            if (bus.removed) {
                continue;
            }

            for (Route route : routes) {
                if (route.containsNode(bus.sourceBus.busNode)) {
                    bus.sourceBus.busColumn = route.columnIndexOf(bus.sourceBus.busNode);
                }

                if (route.containsNode(bus.targetBus.busNode)) {
                    bus.targetBus.busColumn = route.columnIndexOf(bus.targetBus.busNode);
                }
            }

            String path = "";
            if (layoutMode != LayoutMode.SIMPLIFIED) {
                path += "1: ";
                Route centerRoute = RoutesUtils.getRouteForConnection(bus.getLeftPart().busNode, bus.getRightPart().busNode, layoutState);

                if (bus.sourceBus.isSplit || bus.isBackBus()) {
                    path += "A";
                    bus.sourceBus.centerRow = routes.indexOf(centerRoute);
                } else {
                    path += "B";
                    int[] indicesOfSourceRoutes = routes.stream()
                            .filter(r -> r.containsConnection(bus.sourceBus.node, bus.sourceBus.busNode))
                            .mapToInt(routes::indexOf)
                            .toArray();

                    bus.sourceBus.centerRow = IntStream.of(indicesOfSourceRoutes).sum() / (float) indicesOfSourceRoutes.length;
                }

                path += "-";

                if (bus.targetBus.isSplit || bus.isBackBus()) {
                    path += "A";
                    bus.targetBus.centerRow = routes.indexOf(centerRoute);
                } else {
                    int[] indicesOfTargetRoutes = routes.stream()
                            .filter(r -> r.containsNode(bus.getRightPart().busNode))
                            .filter(r -> r.isFreeBetweenNodes(bus.getLeftPart().busColumn, bus.getRightPart().busColumn, true, true))
                            .mapToInt(routes::indexOf)
                            .toArray();

                    if (indicesOfTargetRoutes.length > 0) {
                        path += "B";
                        bus.targetBus.centerRow = IntStream.of(indicesOfTargetRoutes).sum() / (float) indicesOfTargetRoutes.length;
                    } else {
                        path += "C";
                        bus.targetBus.centerRow = routes.indexOf(centerRoute);
                    }
                }
            } else {
                path += "2: ";
                if (bus.isBackBus()) {
                    path += "A";
                    Route route = RoutesUtils.getRouteForConnection(bus.getLeftPart().busNode, bus.getRightPart().busNode, layoutState);
                    bus.sourceBus.centerRow = layoutState.routes.indexOf(route);
                    bus.targetBus.centerRow = bus.sourceBus.centerRow;
                } else {
                    path += "B";
                    Route sourceRoute = RoutesUtils.getRouteForConnection(bus.sourceBus.node, bus.sourceBus.busNode, layoutState);
                    Route targetRoute = RoutesUtils.getRouteForConnection(bus.targetBus.busNode, bus.targetBus.node, layoutState);

                    bus.sourceBus.centerRow = layoutState.routes.indexOf(sourceRoute);
                    bus.targetBus.centerRow = layoutState.routes.indexOf(targetRoute);
                }

            }

            if (bus.sourceBus.centerRow < 0 || bus.targetBus.centerRow < 0) {
                System.err.println("no center row found for bus " + bus.sourceBus.node.compName + " -> " + bus.targetBus.node.compName + ", path: " + path);
            }
        }
    }
}
