/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.*;
import de.monticore.lang.monticar.svggenerator.calculators.routes.DummyPort;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class SplitHandler {

    private RoutesLayoutState layoutState;
    private TranslatedModel translatedModel;

    public SplitHandler(RoutesLayoutState layoutState, TranslatedModel translatedModel) {
        this.layoutState = layoutState;
        this.translatedModel = translatedModel;
    }

    public void removeSplits() {
        int length = RoutesUtils.getColumnCount(layoutState);
        int columnIndex = 1;
        while (columnIndex < length) {
            List<List<Integer>> nodeParts = new ArrayList<>();
            List<Integer> currentPart = new ArrayList<>();
            Node currentNode = null;
            int currentConnectorCount = 0;
            int maxConnectorCount = 0;
            int maxConnectorPartIndex = 0;

            for (int routeIndex = 0; routeIndex < layoutState.routes.size(); routeIndex++) {
                Route route = layoutState.routes.get(routeIndex);
                RouteState state = route.getState(columnIndex);

                if (state == RouteState.NODE) {
                    if (route.getNode(columnIndex).isTemporary) {
                        continue;
                    }

                    if (currentNode == null) {
                        currentNode = route.getNode(columnIndex);
                    } else if (!currentNode.equals(route.getNode(columnIndex))) {
                        if (!currentPart.isEmpty()) {
                            nodeParts.add(currentPart);
                        }

                        removeSplitsForNode(currentNode, nodeParts, maxConnectorPartIndex);

                        nodeParts = new ArrayList<>();
                        currentPart = new ArrayList<>();
                        maxConnectorCount = 0;
                        maxConnectorPartIndex = 0;
                        currentConnectorCount = 0;
                        currentNode = route.getNode(columnIndex);
                        length += nodeParts.size() - 1;
                    }
                    currentPart.add(routeIndex);

                    currentConnectorCount++;
                    if (route.getNodeInputPort(currentNode) != null) {
                        currentConnectorCount++;
                    }

                    if (route.getNodeOutputPort(currentNode) != null) {
                        currentConnectorCount++;
                    }

                    if (currentConnectorCount > maxConnectorCount) {
                        maxConnectorCount = currentConnectorCount;
                        maxConnectorPartIndex = nodeParts.size();
                    }
                } else if (!currentPart.isEmpty()) {
                    nodeParts.add(currentPart);
                    currentPart = new ArrayList<>();
                    currentConnectorCount = 0;
                }
            }

            if (currentNode != null && !currentPart.isEmpty() && !currentNode.isTemporary) {
                nodeParts.add(currentPart);

                if (currentConnectorCount > maxConnectorCount) {
                    maxConnectorCount = currentConnectorCount;
                    maxConnectorPartIndex = nodeParts.size();
                }
            }

            if (currentNode != null) {
                removeSplitsForNode(currentNode, nodeParts, maxConnectorPartIndex);
                length += nodeParts.size() - 1;
            }

            nodeParts.clear();
            columnIndex += 1;
            length = RoutesUtils.getColumnCount(layoutState);
        }
    }

    private void removeSplitsForNode(Node node, List<List<Integer>> parts, int biggestPartIndex) {
        if (parts.size() <= 1 || node.isTemporary) {
            return;
        }

        if (biggestPartIndex > 0) {
            removeSplitsAbove(node, parts, biggestPartIndex);
        }

        if (biggestPartIndex < parts.size() - 1) {
            removeSplitsBelow(node, parts, biggestPartIndex);
        }
    }

    private void removeSplitsAbove(Node node, List<List<Integer>> parts, int biggestPartIndex) {
        List<Integer> biggestPart = parts.get(biggestPartIndex);

        for (int partIndex = biggestPartIndex - 1; partIndex >= 0; partIndex--) {
            List<Integer> currentPart = parts.get(partIndex);

            for (int i = currentPart.size() - 1; i >= 0; i--) {
                int index = currentPart.get(i);
                Route oldRoute = layoutState.routes.get(index);
                int columnIndex = oldRoute.columnIndexOf(node);
                PortSymbol inputPort = oldRoute.getNodeInputPort(node);
                PortSymbol outputPort = oldRoute.getNodeOutputPort(node);

                if (inputPort == null && outputPort == null) {
                    continue;
                }

                Node nodeLeft = oldRoute.getPredecessor(node);
                Node nodeRight = oldRoute.getSuccessor(node);

                Route newRoute = RoutesUtils.addRowAt(biggestPart.get(0), layoutState, nodeLeft, node, nodeRight);

                createSplitConnectors(newRoute, oldRoute, columnIndex, node);
            }
        }
    }

    private void removeSplitsBelow(Node node, List<List<Integer>> parts, int biggestPartIndex) {
        List<Integer> biggestPart = parts.get(biggestPartIndex);
        int rowsInserted = 0;

        for (int partIndex = biggestPartIndex + 1; partIndex < parts.size(); partIndex++) {
            List<Integer> currentPart = parts.get(partIndex);

            for (int index : currentPart) {
                Route oldRoute = layoutState.routes.get(index + rowsInserted);
                int columnIndex = oldRoute.columnIndexOf(node);
                PortSymbol inputPort = oldRoute.getNodeInputPort(node);
                PortSymbol outputPort = oldRoute.getNodeOutputPort(node);

                if (inputPort == null && outputPort == null) {
                    continue;
                }

                Node nodeLeft = oldRoute.getPredecessor(node);
                Node nodeRight = oldRoute.getSuccessor(node);

                int insertionIndex = biggestPart.get(biggestPart.size() - 1) + rowsInserted + 1;
                Route newRoute = RoutesUtils.addRowAt(insertionIndex, layoutState, nodeLeft, node, nodeRight);

                createSplitConnectors(newRoute, oldRoute, columnIndex, node);

                rowsInserted += 1;
            }
        }
    }

    private void createSplitConnectors(Route newRoute, Route oldRoute, int columnIndex, Node node) {
        Node nodeLeft = oldRoute.getPredecessor(node);
        Node nodeRight = oldRoute.getSuccessor(node);

        newRoute.set(columnIndex, node);
        createSplitConnectorsIfNeeded(nodeLeft, node, nodeRight, newRoute, oldRoute);

        columnIndex = newRoute.columnIndexOf(node);

        PortSymbol inputPort = oldRoute.getNodeInputPort(node);
        PortSymbol outputPort = oldRoute.getNodeOutputPort(node);

        newRoute.set(columnIndex, node, inputPort, outputPort);
        oldRoute.clear(oldRoute.columnIndexOf(node));

        if (inputPort != null) {
            BusPart leftBusPart = translatedModel.getBus(inputPort);

            if (leftBusPart != null) {
                leftBusPart.isSplit = true;
            }
        }

        if (outputPort != null) {
            BusPart rightBusPart = translatedModel.getBus(outputPort);

            if (rightBusPart != null) {
                rightBusPart.isSplit = true;
            }
        }
    }

    private void createSplitConnectorsIfNeeded(Node nodeLeft, Node centerNode, Node nodeRight, Route newRoute, Route oldRoute) {
        if (nodeLeft != null) {
            createLeftSplitConnector(nodeLeft, centerNode, newRoute, oldRoute);
//            Log.printRoutes("new left split: ", layoutState);
        }

        if (nodeRight != null) {
            createRightSplitConnector(nodeRight, centerNode, newRoute, oldRoute);
//            Log.printRoutes("new right split: ", layoutState);
        }
    }

    private void createLeftSplitConnector(Node nodeLeft, Node centerNode, Route newRoute, Route oldRoute) {
        int centerColumnIndex = oldRoute.columnIndexOf(centerNode);
        int leftColumnIndex = oldRoute.columnIndexOf(nodeLeft);
        boolean isFreeLeft = newRoute.containsNode(nodeLeft) && newRoute.isFreeBetweenNodes(leftColumnIndex, centerColumnIndex, true);

        if (isFreeLeft) {
            newRoute.set(leftColumnIndex, nodeLeft, null, oldRoute.getNodeOutputPort(nodeLeft));
            oldRoute.removeOutputPortForNode(nodeLeft);

            for (int i = leftColumnIndex + 1; i < centerColumnIndex; i++) {
                oldRoute.set(i, RouteState.EMPTY);
                newRoute.set(i, RouteState.ROUTE);
            }
        } else if (newRoute.isFreeBetweenNodes(nodeLeft, centerNode, true)) {
            newRoute.set(leftColumnIndex, nodeLeft, null, DummyPort.create());
            oldRoute.set(leftColumnIndex, nodeLeft, DummyPort.create(), null);
        } else {
            if (leftColumnIndex < centerColumnIndex - 1) {
                layoutState.routes.forEach(r -> {
                    r.expandAt(centerColumnIndex);

                    if (r.containsNode(nodeLeft)) {
                        r.set(centerColumnIndex, nodeLeft, r.getNodeInputPort(nodeLeft), r.getNodeOutputPort(nodeLeft));
                        r.set(leftColumnIndex, RouteState.ROUTE);
                    }
                });

                oldRoute.set(centerColumnIndex, nodeLeft, oldRoute.getNodeInputPort(nodeLeft), null);
                newRoute.set(centerColumnIndex, nodeLeft, null, DummyPort.create());
            } else {
                oldRoute.set(leftColumnIndex, nodeLeft, oldRoute.getNodeInputPort(nodeLeft), null);
                newRoute.set(leftColumnIndex, nodeLeft, null, DummyPort.create());
            }
        }
    }

    private void createRightSplitConnector(Node nodeRight, Node centerNode, Route newRoute, Route oldRoute) {
        int centerColumnIndex = oldRoute.columnIndexOf(centerNode);
        int rightColumnIndex = oldRoute.columnIndexOf(nodeRight);
        boolean isFreeRight = newRoute.containsNode(nodeRight) && newRoute.isFreeBetweenNodes(centerColumnIndex, rightColumnIndex, true);

        if (isFreeRight) {
            newRoute.set(rightColumnIndex, nodeRight, oldRoute.getNodeInputPort(nodeRight), null);
            oldRoute.removeInputPortForNode(nodeRight);

            for (int i = centerColumnIndex + 1; i < rightColumnIndex; i++) {
                oldRoute.set(i, RouteState.EMPTY);
                newRoute.set(i, RouteState.ROUTE);
            }
        } else if (newRoute.isFreeBetweenNodes(centerNode, nodeRight, true)) {
            oldRoute.set(rightColumnIndex, nodeRight, null, DummyPort.create());
            newRoute.set(rightColumnIndex, nodeRight, DummyPort.create(), null);
        } else {
            if (centerColumnIndex < rightColumnIndex - 1) {
                layoutState.routes.forEach(r -> {
                    r.expandAt(centerColumnIndex + 1);

                    if (r.containsNode(nodeRight)) {
                        r.set(centerColumnIndex + 1, nodeRight, r.getNodeInputPort(nodeRight), r.getNodeOutputPort(nodeRight));
                        r.set(rightColumnIndex + 1, RouteState.ROUTE);
                    }
                });
            }
            oldRoute.set(centerColumnIndex + 1, nodeRight, null, oldRoute.getNodeOutputPort(nodeRight));
            newRoute.set(centerColumnIndex + 1, nodeRight, DummyPort.create(), null);
        }
    }
}
