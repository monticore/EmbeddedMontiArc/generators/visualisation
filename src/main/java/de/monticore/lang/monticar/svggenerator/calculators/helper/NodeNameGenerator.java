/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class NodeNameGenerator {

    private static int tempNodesIndex = 0;

    public static String nextTempNodesName(Node rootNode) {
        return rootNode.compName + "_" + String.valueOf(tempNodesIndex++);
    }
}
