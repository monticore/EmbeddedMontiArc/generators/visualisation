/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Peters Notebook on 04.11.2017.
 */
public class Bus {

    public BusPart sourceBus;
    public BusPart targetBus;
    public List<Pair<PortSymbol, PortSymbol>> portConnections;
    public boolean removed;
    public boolean decomposed;

    public Bus(Node source, Node target) {
        setSourceBus(new BusPart(source, true));
        setTargetBus(new BusPart(target, false));
        portConnections = new ArrayList<>();
        removed = false;
        decomposed = true;
    }

    public void setSourceBus(BusPart sourceBus) {
        this.sourceBus = sourceBus;
        this.sourceBus.addParentBus(this);
    }

    public void setTargetBus(BusPart targetBus) {
        this.targetBus = targetBus;
        this.targetBus.addParentBus(this);
    }

    public boolean isBackBus() {
        return targetBus.busColumn <= sourceBus.busColumn;
    }

    public BusPart getLeftPart() {
        if (isBackBus()) {
            return targetBus;
        } else {
            return sourceBus;
        }
    }

    public BusPart getRightPart() {
        if (isBackBus()) {
            return sourceBus;
        } else {
            return targetBus;
        }
    }

    public boolean isConnectedWithOtherBuses() {
        return sourceBus.parentBuses.size() > 1 || targetBus.parentBuses.size() > 1;
    }
}
