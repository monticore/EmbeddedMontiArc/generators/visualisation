/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

/**
 * Created by Peters Notebook on 01.05.2017.
 */
public interface DrawingConstants {
    int PORT_HEIGHT = 16;
    int PORT_WIDTH = 16;

    int BUS_LINE_WIDTH = 4;

    int CONNECTOR_LINE_WIDTH = 2;

    int COLUMN_SPACING = 60;
    int COLUMN_SPACING_SMALL = 30;
    int COLUMN_WIDTH = 200;
    int ROW_HEIGHT_LARGE = 55;
    int ROW_HEIGHT = 40;
    int ROW_SPACING = 20;
    int TEMP_NODE_COLUMN_WIDTH = 30;
    int VERTICAL_LINES_SPACING = 10;

    int KNOT_RADIUS = 4;

    int TOP_ROW_EXTRA_SPACING = 16;

    String NAME_ROOT_IN = "root_in";
    String NAME_ROOT_OUT = "root_out";
}
