/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.RoutesUtils;
import de.monticore.lang.monticar.svggenerator.calculators.helper.TableDimensions;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.SymbolsHelper;
import de.monticore.lang.monticar.svggenerator.generators.CanvasGenerator;
import de.monticore.lang.monticar.svggenerator.generators.ComponentGenerator;
import de.monticore.lang.monticar.svggenerator.generators.ConnectorGenerator;
import de.monticore.lang.monticar.svggenerator.generators.PortGenerator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileEntry;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.util.*;

/**
 * Created by MatlabEclipseVM on 27.06.2016.
 */
public class SVGGenerator {
    private final static String FILE_NAME_SUFFIX_COMPLETE = "";
    private final static String FILE_NAME_SUFFIX_NO_PORT_NAMES = "_no_port_names";
    private final static String FILE_NAME_SUFFIX_SIMPLIFIED = "_simplified";

    private String outputPath;

    public SVGGenerator() {
        this("output/");
    }

    public SVGGenerator(String outputPath) {
        if (outputPath.isEmpty()) {
            this.outputPath = "output/";
        } else {
            this.outputPath = outputPath;
        }

        try {
            copyIcons();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * DRAW: Creates the SVG output file
     */
    public void drawLayouts(ExpandedComponentInstanceSymbol inst, RoutesLayoutState layoutState) throws IOException {
        drawComplete(layoutState.componentInstanceSymbol, layoutState);
        drawNoPortNames(layoutState.componentInstanceSymbol, layoutState);
        drawSimplified(layoutState.componentInstanceSymbol, layoutState);
    }

    private void copyIcons() throws IOException {
        List<String> iconsToCopy = new ArrayList<>();
        iconsToCopy.add("icon_full_detail.svg");
        iconsToCopy.add("icon_no_port_names.svg");
        iconsToCopy.add("icon_simplified.svg");

        for (String fileName : iconsToCopy) {
            URL inputUrl = ClassLoader.getSystemResource("icons/" + fileName);
            File target = Paths.get(outputPath + "icons/" + fileName).toFile();
            FileUtils.copyURLToFile(inputUrl, target);
        }
    }

    /**
     * Draws the component the "usual" way with all connectors ports and names
     */
    private void drawComplete(ExpandedComponentInstanceSymbol inst, RoutesLayoutState layoutState) throws IOException {
        TemplateBuilder tb = new TemplateBuilder();

        TableDimensions noPortNamesTableDimensions = new TableDimensions(LayoutMode.NORMAL);
        noPortNamesTableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = noPortNamesTableDimensions;

        String fileNameSuffix = FILE_NAME_SUFFIX_COMPLETE;
        CanvasGenerator canvasGenerator = new CanvasGenerator(LayoutMode.NORMAL);
        ComponentGenerator componentGenerator = new ComponentGenerator(LayoutMode.NORMAL);
        PortGenerator portGenerator = new PortGenerator(LayoutMode.NORMAL);
        ConnectorGenerator connectorGenerator = new ConnectorGenerator(LayoutMode.NORMAL);

        // Draw Canvas, Components, Ports, Connectors
        canvasGenerator.draw(inst, tb, false, fileNameSuffix);
        componentGenerator.draw(inst, tb, false, fileNameSuffix);
        portGenerator.draw(inst, tb, false, fileNameSuffix);
        connectorGenerator.draw(inst, tb, true, fileNameSuffix);

        TemplateBuilder.Hierarchy hierarchy = getHierarchy(inst);

        File file = new File(outputPath);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new IOException("Could not create output directory");
            }
        }

        // Create the SVG output file and fill with content
        String svg = tb.build();
        Files.write(Paths.get(getSvgFileName(inst, LayoutMode.NORMAL)), svg.getBytes());

        int width;
        int height;
        List<CanvasLayoutSymbol> canvasLayoutSymbols = SymbolsHelper.canvasLayoutSymbolsOfInstance(inst, LayoutMode.NORMAL);
        if (!canvasLayoutSymbols.isEmpty()) {
            CanvasLayoutSymbol sym = canvasLayoutSymbols.get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        } else {
            ComponentLayoutSymbol sym = SymbolsHelper.componentLayoutSymbolsOfInstance(inst, LayoutMode.NORMAL).get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        }

        width += RoutesUtils.tableDimensions.getCanvasMarginLeft() + RoutesUtils.tableDimensions.getCanvasMarginRight();
        height += RoutesUtils.tableDimensions.getCanvasMarginTop() + RoutesUtils.tableDimensions.getCanvasMarginBottom();

        String html = tb.buildHTMLFile(inst.getFullName(), width, height, hierarchy, fileNameSuffix);

        // Create the HTML output file and fill with content
        Files.write(Paths.get(getHtmlFileName(inst, LayoutMode.NORMAL)), html.getBytes());
    }

    /**
     * Draws the component without port names
     */
    private void drawNoPortNames(ExpandedComponentInstanceSymbol inst, RoutesLayoutState layoutState) throws IOException {
        TemplateBuilder tb = new TemplateBuilder();

        TableDimensions noPortNamesTableDimensions = new TableDimensions(LayoutMode.NO_PORT_NAMES);
        noPortNamesTableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = noPortNamesTableDimensions;

        String fileNameSuffix = FILE_NAME_SUFFIX_NO_PORT_NAMES;
        CanvasGenerator canvasGenerator = new CanvasGenerator(LayoutMode.NO_PORT_NAMES);
        ComponentGenerator componentGenerator = new ComponentGenerator(LayoutMode.NO_PORT_NAMES);
        PortGenerator portGenerator = new PortGenerator(LayoutMode.NO_PORT_NAMES);
        ConnectorGenerator connectorGenerator = new ConnectorGenerator(LayoutMode.NO_PORT_NAMES);

        // Draw Canvas, Components, Ports, Connectors
        canvasGenerator.draw(inst, tb, false, fileNameSuffix);
        componentGenerator.draw(inst, tb, false, fileNameSuffix);
        portGenerator.draw(inst, tb, false, fileNameSuffix);
        connectorGenerator.draw(inst, tb, true, fileNameSuffix);

        TemplateBuilder.Hierarchy hierarchy = getHierarchy(inst);

        File file = new File(outputPath);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new IOException("Could not create output directory");
            }
        }

        // Create the SVG output file and fill with content
        String svg = tb.build();
        Files.write(Paths.get(getSvgFileName(inst, LayoutMode.NO_PORT_NAMES)), svg.getBytes());

        int width;
        int height;
        List<CanvasLayoutSymbol> canvasLayoutSymbols = SymbolsHelper.canvasLayoutSymbolsOfInstance(inst, LayoutMode.NO_PORT_NAMES);
        if (!canvasLayoutSymbols.isEmpty()) {
            CanvasLayoutSymbol sym = canvasLayoutSymbols.get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        } else {
            ComponentLayoutSymbol sym = SymbolsHelper.componentLayoutSymbolsOfInstance(inst, LayoutMode.NO_PORT_NAMES).get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        }

        String html = tb.buildHTMLFile(inst.getFullName(), width + 160, height + 160, hierarchy, fileNameSuffix);

        // Create the HTML output file and fill with content
        Files.write(Paths.get(getHtmlFileName(inst, LayoutMode.NO_PORT_NAMES)), html.getBytes());
    }

    /**
     * Draws the component very simplified. So there will be no ports displayed and for every pair of components there will only one connector be drawn.
     */
    private void drawSimplified(ExpandedComponentInstanceSymbol inst, RoutesLayoutState layoutState) throws IOException {
        TemplateBuilder tb = new TemplateBuilder();

        TableDimensions noPortNamesTableDimensions = new TableDimensions(LayoutMode.SIMPLIFIED);
        noPortNamesTableDimensions.computeDimensions(layoutState);
        RoutesUtils.tableDimensions = noPortNamesTableDimensions;

        String fileNameSuffix = FILE_NAME_SUFFIX_SIMPLIFIED;
        CanvasGenerator canvasGenerator = new CanvasGenerator(LayoutMode.SIMPLIFIED);
        ComponentGenerator componentGenerator = new ComponentGenerator(LayoutMode.SIMPLIFIED);
        ConnectorGenerator connectorGenerator = new ConnectorGenerator(LayoutMode.SIMPLIFIED);

        // Draw Canvas, Components, Ports, Connectors
        canvasGenerator.draw(inst, tb, false, fileNameSuffix);
        componentGenerator.draw(inst, tb, false, fileNameSuffix);
        connectorGenerator.draw(inst, tb, true, fileNameSuffix);

        TemplateBuilder.Hierarchy hierarchy = getHierarchy(inst);

        File file = new File(outputPath);
        if (!file.exists()) {
            if (!file.mkdirs()) {
                throw new IOException("Could not create output directory");
            }
        }

        // Create the SVG output file and fill with content
        String svg = tb.build();
        Files.write(Paths.get(getSvgFileName(inst, LayoutMode.SIMPLIFIED)), svg.getBytes());

        int width;
        int height;
        List<CanvasLayoutSymbol> canvasLayoutSymbols = SymbolsHelper.canvasLayoutSymbolsOfInstance(inst, LayoutMode.SIMPLIFIED);
        if (!canvasLayoutSymbols.isEmpty()) {
            CanvasLayoutSymbol sym = canvasLayoutSymbols.get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        } else {
            ComponentLayoutSymbol sym = SymbolsHelper.componentLayoutSymbolsOfInstance(inst, LayoutMode.SIMPLIFIED).get(0);
            width = sym.getWidth();
            height = sym.getHeight();
        }

        width += RoutesUtils.tableDimensions.getCanvasMarginLeft();
        width += RoutesUtils.tableDimensions.getCanvasMarginRight();
        height += RoutesUtils.tableDimensions.getCanvasMarginBottom();
        height += RoutesUtils.tableDimensions.getCanvasMarginTop();

        String html = tb.buildHTMLFile(inst.getFullName(), width, height, hierarchy, fileNameSuffix);

        // Create the HTML output file and fill with content
        Files.write(Paths.get(getHtmlFileName(inst, LayoutMode.SIMPLIFIED)), html.getBytes());
    }

    private TemplateBuilder.Hierarchy getHierarchy(ExpandedComponentInstanceSymbol inst) {
        TemplateBuilder.Hierarchy hierarchy =  new TemplateBuilder.Hierarchy();
        hierarchy.names = new ArrayList<>();
        hierarchy.names.add(inst.getName());
        hierarchy.fullNames = new ArrayList<>();
        hierarchy.fullNames.add(inst.getFullName());

        Optional<ExpandedComponentInstanceSymbol> optionalSymbol = inst.getEnclosingComponent();
        while (optionalSymbol.isPresent()) {
            ExpandedComponentInstanceSymbol symbol = optionalSymbol.get();

            hierarchy.names.add(0, symbol.getName());
            hierarchy.fullNames.add(0, symbol.getFullName());

            optionalSymbol = symbol.getEnclosingComponent();
        }

        return hierarchy;
    }

    public String getSvgFileName(ExpandedComponentInstanceSymbol instance, LayoutMode layoutMode) {
        return getFileName(instance, layoutMode, ".svg");
    }

    public String getHtmlFileName(ExpandedComponentInstanceSymbol instance, LayoutMode layoutMode) {
        return getFileName(instance, layoutMode, ".html");
    }

    private String getFileName(ExpandedComponentInstanceSymbol instance, LayoutMode layoutMode, String fileEnding) {
        String fileNameSuffix;
        switch (layoutMode) {
            case SIMPLIFIED:
                fileNameSuffix = FILE_NAME_SUFFIX_SIMPLIFIED;
                break;
            case NO_PORT_NAMES:
                fileNameSuffix = FILE_NAME_SUFFIX_NO_PORT_NAMES;
                break;
            case NORMAL:
                fileNameSuffix = FILE_NAME_SUFFIX_COMPLETE;
                break;
            default:
                fileNameSuffix = "";
                break;
        }

        return outputPath + instance.getFullName() + fileNameSuffix + fileEnding;
    }
}
