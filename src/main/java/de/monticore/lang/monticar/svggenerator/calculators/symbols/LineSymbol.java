/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.tagging._symboltable.TagKind;
import de.monticore.lang.tagging._symboltable.TagSymbol;

public class LineSymbol extends TagSymbol implements LayoutModeDepending {
    public static final LineSymbolKind KIND = LineSymbolKind.INSTANCE;

    public LineSymbol(Point start, Point end, int lineWidth, LayoutMode layoutMode, boolean hasKnotAtStart) {
        this(start, end, lineWidth, layoutMode, hasKnotAtStart, -1);
    }

    public LineSymbol(Point start, Point end, int lineWidth, LayoutMode layoutMode, boolean hasKnotAtStart, int indexInBus) {
        super(KIND, start, end, lineWidth, layoutMode, hasKnotAtStart, indexInBus);
    }

    public Point getStart() {
        return getValue(0);
    }

    public Point getEnd() {
        return getValue(1);
    }

    public int getLineWidth() {
        return getValue(2);
    }

    public LayoutMode getLayoutMode() {
        return getValue(3);
    }

    public boolean hasKnotAtStart() {
        return getValue(4);
    }

    public int getIndexInBus() {
        return getValue(5);
    }

    public boolean isHorizontal() {
        return getStart().y == getEnd().y;
    }

    public boolean isVertical() {
        return getStart().x == getEnd().x;
    }

    public static class LineSymbolKind extends TagKind {
        public static final LineSymbolKind INSTANCE = new LineSymbolKind();

        protected LineSymbolKind() {
        }
    }

    @Override
    public String toString() {
        return getStart().toString() + " -> " + getEnd().toString();
    }
}
