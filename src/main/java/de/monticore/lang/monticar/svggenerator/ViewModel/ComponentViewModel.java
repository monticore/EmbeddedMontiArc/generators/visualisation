/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;//

import java.util.HashMap;
import java.util.List;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class ComponentViewModel extends ViewModel {

    public int x;
    public int y;
    public int id;
    public int height;
    public int width;
    public String name;
    public String fullname;
    public boolean isEmpty;
    public List<String> hierarchy;
    public String fileNameSuffix;
    public String idePath;

    @Override
    public HashMap<String, Object> hashMap() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("X", this.x);
        map.put("Y", this.y);
        map.put("id", this.id);
        map.put("height", this.height);
        map.put("width", this.width);
        map.put("name", this.name);
        map.put("fullName", this.fullname);
        map.put("isEmpty", this.isEmpty);
        map.put("fileNameSuffix", this.fileNameSuffix);
        map.put("idePath", this.idePath);

        if(hierarchy != null) {
            map.put("hierarchy", this.hierarchy);
        }

        return map;
    }
}
