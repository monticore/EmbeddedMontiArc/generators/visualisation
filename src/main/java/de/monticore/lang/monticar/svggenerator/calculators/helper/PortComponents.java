/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;

import java.util.HashMap;

public class PortComponents {

    private static HashMap<String, ExpandedComponentInstanceSymbol> components = new HashMap<>();

    public static void clear() {
        components.clear();
    }

    public static void put(String portName, ExpandedComponentInstanceSymbol componentInstanceSymbol) {
        components.put(portName, componentInstanceSymbol);
    }

    public static ExpandedComponentInstanceSymbol get(String portName) {
        return components.get(portName);
    }

}
