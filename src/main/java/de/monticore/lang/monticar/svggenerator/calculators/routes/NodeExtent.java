/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

/**
 * Created by Peters Notebook on 10.11.2017.
 */
public class NodeExtent {
    public Node node;
    public int columnIndex;
    public int topRowIndex;
    public int bottomRowIndex;
}
