/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;

public class ComponentLayoutSymbol extends DrawableSymbol implements LayoutModeDepending {
    public static final ComponentLayoutKind KIND = ComponentLayoutKind.INSTANCE;

    public ComponentLayoutSymbol(int id, int x, int y, int width, int height, LayoutPosition layoutPosition, LayoutMode layoutMode) {
        super(KIND, id, x, y);
        addValues(width, height, layoutPosition, layoutMode);
    }

    public int getWidth() {
        return getValue(3);
    }

    public int getHeight() {
        return getValue(4);
    }

    public LayoutPosition getLayoutPosition() {
        return getValue(5);
    }

    public LayoutMode getLayoutMode() {
        return getValue(6);
    }

    @Override
    public String toString() {
        return String.format(
                "componentLayout = { id=%d, x=%d, y=%d, width=%d, height=%d, \n" +
                "                  layoutPosition=%s }",
                getId(), getX(), getY(), getWidth(), getHeight(), getLayoutPosition());
    }

    public static class ComponentLayoutKind extends DrawableKind {
        public static final ComponentLayoutKind INSTANCE = new ComponentLayoutKind();

        protected ComponentLayoutKind() {
        }
    }
}
