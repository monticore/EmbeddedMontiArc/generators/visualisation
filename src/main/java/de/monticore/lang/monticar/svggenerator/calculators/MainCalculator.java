/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;

/**
 * Created by Robert on 01.11.2016.
 */
public class MainCalculator {

    public RoutesLayoutCalculator routesLayoutCalculator;

    public MainCalculator() {
        routesLayoutCalculator = new RoutesLayoutCalculator();
    }

    public MainCalculator(RouteOrderCalculator routeOrderCalculator) {
        routesLayoutCalculator = new RoutesLayoutCalculator(routeOrderCalculator);
    }

    public void calculateLayout(ExpandedComponentInstanceSymbol inst) {
        routesLayoutCalculator.calculateLayout(inst);
    }

}
