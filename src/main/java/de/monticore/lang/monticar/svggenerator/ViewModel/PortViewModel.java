/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;

import de.monticore.lang.monticar.svggenerator.calculators.symbols.LayoutPosition;

import java.util.HashMap;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class PortViewModel extends ViewModel {

    public int x;
    public int y;
    public int angle;
    public String anchor;
    public String portname;

    public LayoutPosition layoutPosition;
    public boolean isInternal;
    public boolean isOutgoing;
    public boolean isIncoming;

    @Override
    public HashMap<String, Object> hashMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("X", this.x);
        map.put("Y", this.y);
        map.put("angle", this.angle);
        map.put("anchor", this.anchor);
        map.put("portname", this.portname);

        return map;
    }
}
