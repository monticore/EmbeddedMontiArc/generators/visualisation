/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;

import java.util.ArrayList;
import java.util.List;

public class SymbolsHelper {

    public static List<CanvasLayoutSymbol> canvasLayoutSymbolsOfInstance(ExpandedComponentInstanceSymbol instance, LayoutMode layoutMode) {
        return new ArrayList<>(Tags.getCanvasLayoutTags(instance, layoutMode));
    }

    public static List<ComponentLayoutSymbol> componentLayoutSymbolsOfInstance(ExpandedComponentInstanceSymbol instance, LayoutMode layoutMode) {
        return new ArrayList<>(Tags.getLayoutTags(instance, layoutMode));
    }

    public static List<ConnectorLayoutSymbol> connectorLayoutSymbolsOfConnector(ConnectorSymbol connectorSymbol, LayoutMode layoutMode) {
        return new ArrayList<>(Tags.getLayoutTags(connectorSymbol, layoutMode));
    }

    public static List<PortLayoutSymbol> portLayoutSymbolsOfPort(PortSymbol portSymbol, LayoutMode layoutMode) {
        return new ArrayList<>(Tags.getLayoutTags(portSymbol, layoutMode));
    }

    public static List<CanvasPortLayoutSymbol> canvasPortLayoutSymbolsOfPort(PortSymbol portSymbol, LayoutMode layoutMode) {
        return new ArrayList<>(Tags.getCanvasLayoutTags(portSymbol, layoutMode));
    }

}
