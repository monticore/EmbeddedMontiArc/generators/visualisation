/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.helper.Point;
import de.monticore.lang.tagging._symboltable.TagKind;
import de.monticore.lang.tagging._symboltable.TagSymbol;

import java.util.stream.Stream;

/**
 * Created by Peters Notebook on 13.05.2017.
 */
public class ConnectorLayoutSymbol extends TagSymbol implements LayoutModeDepending {
    public final static ConnectorLayoutKind KIND = ConnectorLayoutKind.INSTANCE;

    public ConnectorLayoutSymbol(int id, Point[] points, Point portSpacingBypass, LayoutMode layoutMode) {
        super(KIND, id, points);

        int[] widths = new int[points.length - 1];
        for (int i = 0; i < widths.length; i++) {
            widths[i] = DrawingConstants.CONNECTOR_LINE_WIDTH;
        }

        addValues(widths, portSpacingBypass, layoutMode, -1, new Point[0]);
    }

    public ConnectorLayoutSymbol(int id, Point[] points, Point portSpacingBypass, LayoutMode layoutMode, Point[] knots) {
        super(KIND, id, points);

        int[] widths = new int[points.length - 1];
        for (int i = 0; i < widths.length; i++) {
            widths[i] = DrawingConstants.CONNECTOR_LINE_WIDTH;
        }

        addValues(widths, portSpacingBypass, layoutMode, -1, knots);
    }

    public ConnectorLayoutSymbol(int id, Point[] points, int[] lineWidths, Point portSpacingBypass, LayoutMode layoutMode, int indexInBus) {
        super(KIND, id, points, lineWidths, portSpacingBypass, layoutMode, indexInBus, new Point[0]);
    }

    public Point[] getPoints() {
        return getValue(1);
    }

    public int[] getLineWidths() {
        return getValue(2);
    }

    public Point getPortSpacingBypassPoint() {
        return getValue(3);
    }

    public LayoutMode getLayoutMode() {
        return getValue(4);
    }

    public int getIndexInBus() {
        return getValue(5);
    }

    public Point[] getKnots() {
        return getValue(6);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("Connector: ");

        Stream.of(getPoints())
                .forEach(point -> {
            builder.append("\n   (");
            builder.append(point.x);
            builder.append(", ");
            builder.append(point.y);
            builder.append("),");
        });

        return builder.toString();
    }

    public static class ConnectorLayoutKind extends TagKind {
        public static final ConnectorLayoutKind INSTANCE = new ConnectorLayoutKind();

        protected ConnectorLayoutKind() {
        }
    }
}
