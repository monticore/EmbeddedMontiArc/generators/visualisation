/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;

/**
 * Created by Peters Notebook on 05.06.2017.
 */
public class CanvasLayoutSymbol extends DrawableSymbol implements LayoutModeDepending {
    public final static CanvasLayoutKind KIND = CanvasLayoutKind.INSTANCE;

    public CanvasLayoutSymbol(int id, int x, int y, int width, int height, LayoutMode layoutMode) {
        super(KIND, id, x, y);

        addValues(width, height, layoutMode);
    }

    public int getWidth() {
        return getValue(3);
    }

    public int getHeight() {
        return getValue(4);
    }

    public LayoutMode getLayoutMode() {
        return getValue(5);
    }

    @Override
    public String toString() {
        System.out.println("toString");
        return "CanvasLayoutSymbol";
    }

    public static class CanvasLayoutKind extends DrawableKind {
        public static final CanvasLayoutSymbol.CanvasLayoutKind INSTANCE = new CanvasLayoutSymbol.CanvasLayoutKind();

        protected CanvasLayoutKind() {
        }
    }
}
