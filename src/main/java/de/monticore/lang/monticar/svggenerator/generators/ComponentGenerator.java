/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.generators;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.monticar.svggenerator.TemplateBuilder;
import de.monticore.lang.monticar.svggenerator.ViewModel.ComponentViewModel;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.ComponentLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.SymbolsHelper;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Robert on 01.11.2016.
 */
public class ComponentGenerator extends InputGenerator {

    private LayoutMode layoutMode;

    public ComponentGenerator(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    @Override
    public void draw(ExpandedComponentInstanceSymbol inst,
                     TemplateBuilder templateBuilder, boolean useColors, String fileNameSuffix) {

        // Get subcomponents because inst represents the canvas/supercomponent
        Collection<ExpandedComponentInstanceSymbol> subComponents = inst.getSubComponents();

        LinkedList<ComponentViewModel> models = new LinkedList<>();
        // For all subcomponents:

        subComponents.stream()
                .filter(comp -> !SymbolsHelper.componentLayoutSymbolsOfInstance(comp, layoutMode).isEmpty())
                .forEach(comp -> {
                    List<ComponentLayoutSymbol> layoutSymbols = SymbolsHelper.componentLayoutSymbolsOfInstance(comp, layoutMode);

                    for (ComponentLayoutSymbol layoutSymbol: layoutSymbols) {
                        ComponentViewModel viewModel = new ComponentViewModel();
                        viewModel.x = layoutSymbol.getX();
                        viewModel.y = layoutSymbol.getY();
                        viewModel.id = layoutSymbol.getId();
                        viewModel.height = layoutSymbol.getHeight();
                        viewModel.width = layoutSymbol.getWidth();
                        viewModel.name = comp.getName();
                        viewModel.fullname = comp.getFullName();
                        viewModel.isEmpty = comp.getSubComponents().isEmpty();
                        viewModel.fileNameSuffix = fileNameSuffix;
                        viewModel.idePath = getIDEPath(comp);

                        models.add(viewModel);
                    }
                });

        //WTF Java? Why U no cast? http://stackoverflow.com/a/5374359/1857527
        ComponentViewModel[] modelArray = new ComponentViewModel[models.size()];
        modelArray = models.toArray(modelArray);


        templateBuilder.components = modelArray;
    }

    private String getIDEPath(ExpandedComponentInstanceSymbol component) {
        String[] fullNameParts = component.getFullName().split("\\.");
        String path = "";

        for(int i = 1; i < fullNameParts.length; i++) {
            path += "/" + fullNameParts[i];
        }

        path += ".ema";

        return path;
    }
}
