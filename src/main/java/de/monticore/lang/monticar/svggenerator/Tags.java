/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.helper.LayoutMode;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.*;
import de.monticore.lang.tagging._symboltable.TagKind;
import de.monticore.lang.tagging._symboltable.TagSymbol;
import de.monticore.lang.tagging._symboltable.TaggingResolver;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.Symbol;
import de.monticore.symboltable.resolving.CommonResolvingFilter;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Tags {

    private static TaggingResolver taggingResolver;

    public static void initTaggingResolver(GlobalScope scope, String modelPath) {
        taggingResolver = new TaggingResolver(scope, Arrays.asList(modelPath));

        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(ComponentLayoutSymbol.KIND));
        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(PortLayoutSymbol.KIND));
        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(ConnectorLayoutSymbol.KIND));
        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(CanvasLayoutSymbol.KIND));
        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(CanvasPortLayoutSymbol.KIND));
        taggingResolver.addTagSymbolResolvingFilter(CommonResolvingFilter.create(LineSymbol.KIND));
    }

    public static void addTag(Symbol symbol, TagSymbol tagSymbol) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        taggingResolver.addTag(symbol, tagSymbol);
    }

    public static Collection<TagSymbol> getTags(Symbol symbol, TagKind kind) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        return taggingResolver.getTags(symbol, kind);
    }

    public static <T extends TagSymbol> Collection<T> getTags(Symbol symbol, TagKind kind, Class<T> c) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        Collection<TagSymbol> tags = taggingResolver.getTags(symbol, kind);
        return tags.stream().filter(c::isInstance).map(c::cast).collect(Collectors.toList());
    }

    public static <T extends TagSymbol> Collection<T> getTags(Symbol symbol, TagKind kind, Class<T> c, Predicate<T> predicate) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        Collection<TagSymbol> tags = taggingResolver.getTags(symbol, kind);
        return tags.stream().filter(c::isInstance).map(c::cast).filter(predicate).collect(Collectors.toList());
    }

    public static <T extends TagSymbol> Optional<T> getFirstTag(Symbol symbol, TagKind kind, Class<T> c) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        Collection<TagSymbol> tags = taggingResolver.getTags(symbol, kind);
        return tags.stream().filter(c::isInstance).map(c::cast).findFirst();
    }

    public static <T extends TagSymbol> Optional<T> getFirstTag(Symbol symbol, TagKind kind, Class<T> c, Predicate<T> predicate) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        Collection<TagSymbol> tags = taggingResolver.getTags(symbol, kind);
        return tags.stream().filter(c::isInstance).map(c::cast).filter(predicate).findFirst();
    }

    public static boolean hasTags(Symbol symbol, TagKind kind) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        return !taggingResolver.getTags(symbol, kind).isEmpty();
    }

    public static boolean hasTags(Symbol symbol, TagKind kind, LayoutMode layoutMode) {
        if (taggingResolver == null) {
            throw new RuntimeException("TaggingResolver is null");
        }

        return taggingResolver.getTags(symbol, kind).stream()
                .filter(LayoutModeDepending.class::isInstance)
                .map(LayoutModeDepending.class::cast)
                .anyMatch(tag -> tag.getLayoutMode() == layoutMode);
    }

    public static Collection<PortLayoutSymbol> getLayoutTags(PortSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, PortLayoutSymbol.KIND, PortLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Optional<PortLayoutSymbol> getFirstLayoutTag(PortSymbol symbol, LayoutMode layoutMode) {
        if (layoutMode != null) {
            return getFirstTag(symbol, PortLayoutSymbol.KIND, PortLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
        } else {
            return getFirstTag(symbol, PortLayoutSymbol.KIND, PortLayoutSymbol.class);
        }
    }

    public static Collection<CanvasPortLayoutSymbol> getCanvasLayoutTags(PortSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, CanvasPortLayoutSymbol.KIND, CanvasPortLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Optional<CanvasPortLayoutSymbol> getFirstCanvasLayoutTag(PortSymbol symbol, LayoutMode layoutMode) {
        return getFirstTag(symbol, CanvasPortLayoutSymbol.KIND, CanvasPortLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Collection<ComponentLayoutSymbol> getLayoutTags(ExpandedComponentInstanceSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, ComponentLayoutSymbol.KIND, ComponentLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Optional<ComponentLayoutSymbol> getFirstLayoutTag(ExpandedComponentInstanceSymbol symbol, LayoutMode layoutMode) {
        return getFirstTag(symbol, ComponentLayoutSymbol.KIND, ComponentLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Collection<CanvasLayoutSymbol> getCanvasLayoutTags(ExpandedComponentInstanceSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, CanvasLayoutSymbol.KIND, CanvasLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Optional<CanvasLayoutSymbol> getFirstCanvasLayoutTag(ExpandedComponentInstanceSymbol symbol, LayoutMode layoutMode) {
        return getFirstTag(symbol, CanvasLayoutSymbol.KIND, CanvasLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Collection<ConnectorLayoutSymbol> getLayoutTags(ConnectorSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, ConnectorLayoutSymbol.KIND, ConnectorLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Optional<ConnectorLayoutSymbol> getFirstLayoutTag(ConnectorSymbol symbol, LayoutMode layoutMode) {
        return getFirstTag(symbol, ConnectorLayoutSymbol.KIND, ConnectorLayoutSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }

    public static Collection<LineSymbol> getLineSymbols(ConnectorSymbol symbol, LayoutMode layoutMode) {
        return getTags(symbol, LineSymbol.KIND, LineSymbol.class, s -> s.getLayoutMode() == layoutMode);
    }
}
