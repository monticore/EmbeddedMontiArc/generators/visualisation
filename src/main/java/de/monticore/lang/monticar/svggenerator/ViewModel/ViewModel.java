/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;//

import java.util.HashMap;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public abstract class ViewModel {
    public abstract HashMap<String, Object> hashMap();
}
