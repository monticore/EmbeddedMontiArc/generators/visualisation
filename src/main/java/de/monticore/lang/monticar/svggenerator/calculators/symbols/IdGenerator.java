/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

/**
 * Created by Michael von Wenckstern on 04.06.2016.
 */
public class IdGenerator {
    protected static int id = 0;


    private IdGenerator() {

    }

    /**
     * returns unique ID, e.g in one SVG file
     */
    public static int getUniqueId() {
        return id++;
    }
}
