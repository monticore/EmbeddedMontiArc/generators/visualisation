/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.symbols;

import de.monticore.lang.tagging._symboltable.TagKind;
import de.monticore.lang.tagging._symboltable.TagSymbol;

/**
 * Super class which defines basic properties for all drawable objects
 * Derived classes: Canvas, Component, Port and Connector
 */
public class DrawableSymbol extends TagSymbol {
    public static final DrawableKind KIND = DrawableKind.INSTANCE;

    // unique id moved to IdGenerator

    protected DrawableSymbol(DrawableKind kind, int id, int x, int y) {
        super(kind, id, x, y);
    }

    public int getId() {
        return getValue(0);
    }

    public int getX() {
        return getValue(1);
    }

    public int getY() {
        return getValue(2);
    }

    public static class DrawableKind extends TagKind {
        public static final DrawableKind INSTANCE = new DrawableKind();

        protected DrawableKind() {
        }
    }
}
