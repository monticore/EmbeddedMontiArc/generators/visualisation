/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;//

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

//  de.monticore.lang.monticar.svggenerator.ViewModel
//  montiarc-core-features
//
//  Created by Sven Titgemeyer on 08/03/2017.
//  Copyright (c) 2017 Sven Titgemeyer. All rights reserved.
//
public class ConnectorViewModel extends ViewModel {

    public List<LineViewModel> lines;
    public HashSet<String> colorSet;

    @Override
    public HashMap<String, Object> hashMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("colors", colorSet.toArray());
        return map;
    }
}
