/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.routes;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BusPart {

    public Node node;
    public Node busNode;
    public int busColumn;
    public float centerRow;
    public Integer xCoordinate;
    public List<PortSymbol> ports;
    public boolean isSource;
    public boolean isSplit;
    public List<Bus> parentBuses;

    public BusPart(Node node, boolean isSource) {
        this.node = node;
        this.ports = new ArrayList<>();
        this.isSource = isSource;
        this.parentBuses = new ArrayList<>();
        this.isSplit = false;
    }

    public List<PortSymbol> getDistinctPorts() {
        return ports.stream().distinct().collect(Collectors.toList());
    }

    public Node getLeftNode() {
        return isSource ? node : busNode;
    }

    public Node getRightNode() {
        return isSource ? busNode : node;
    }

    public void addPort(PortSymbol portSymbol) {
        if (!ports.contains(portSymbol)) {
            ports.add(portSymbol);
        }
    }

    public void addParentBus(Bus bus) {
        if (!parentBuses.contains(bus)) {
            parentBuses.add(bus);
        }
    }

    public void removeParentBus(Bus bus) {
        parentBuses.remove(bus);
    }
}
