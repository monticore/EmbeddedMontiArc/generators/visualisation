/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators.helper;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.monticar.svggenerator.Tags;
import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.RoutesLayoutState;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Bus;
import de.monticore.lang.monticar.svggenerator.calculators.routes.DummyPort;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Node;
import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.CanvasPortLayoutSymbol;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.IdGenerator;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.PortLayoutSymbol;

import java.util.List;

/**
 * Created by Peters Notebook on 07.11.2017.
 */
public class PortCoordinatesHandler implements DrawingConstants {

    private LayoutMode layoutMode;

    public PortCoordinatesHandler(LayoutMode layoutMode) {
        this.layoutMode = layoutMode;
    }

    public void assignPortCoordinates(RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;

        routes.forEach(route -> {
            List<Node> nodes = route.getNodes();

            for (int nodeIndex = 0; nodeIndex < nodes.size() - 1; nodeIndex++) {
                Node node = nodes.get(nodeIndex);

                if (node.isTemporary) {
                    continue;
                }

                assignPortCoordinatesAt(route, node, layoutState);
            }
        });

        for (Pair<Node, Node> pair : layoutState.busses.keySet()) {
            Bus bus = layoutState.busses.get(pair);
            if (layoutMode == LayoutMode.SIMPLIFIED) {
                assignSingletonBusPortCoordinates(bus, layoutState);
            } else {
                assignBusPortCoordinates(bus, layoutState);
            }
        }
    }

    private void assignPortCoordinatesAt(Route route, Node node, RoutesLayoutState layoutState) {
        Node next = route.getSuccessor(node);
        int tableWidth = RoutesUtils.getTableWidth(layoutState.routes);
        int routeIndex = layoutState.routes.indexOf(route);

        if (next != null && !next.isTemporary) {
            PortSymbol sourceOutputPort = route.getNodeOutputPort(node);
            PortSymbol targetInputPort = route.getNodeInputPort(next);

            Pair<Node, Node> nodePair = new Pair<>(node, next);
            if (layoutState.busses.containsKey(nodePair)) {
                return;
            }

            int sourceNodeColumnIndex = route.columnIndexOf(node);
            int targetNodeColumnIndex = route.columnIndexOf(next);

            if (node.compName.equals(NAME_ROOT_IN)) {
                assignCoordinatesToEnclosingInputPort(routeIndex, sourceOutputPort);
            } else {
                assignCoordinatesToInternalOutputPort(sourceNodeColumnIndex, routeIndex, sourceOutputPort, layoutState.routes);
            }

            if (next.compName.equals(NAME_ROOT_OUT)) {
                assignCoordinatesToEnclosingOutgoingPort(routeIndex, tableWidth, targetInputPort);
            } else {
                assignCoordinatesToInternalInputPort(targetNodeColumnIndex, routeIndex, targetInputPort, layoutState.routes);
            }
        }
    }

    private void assignBusPortCoordinates(Bus bus, RoutesLayoutState layoutState) {
        List<Route> routes = layoutState.routes;
        List<PortSymbol> sourceOutputPorts = bus.sourceBus.ports;
        List<PortSymbol> targetInputPorts = bus.targetBus.ports;
        int tableWidth = RoutesUtils.getTableWidth(routes);

        boolean sourceIsRoot = bus.sourceBus.node.compName.equals(NAME_ROOT_IN);
        boolean targetIsRoot = bus.targetBus.node.compName.equals(NAME_ROOT_OUT);

        for (int i = 0; i < sourceOutputPorts.size(); i++) {
            PortSymbol sourcePortSymbol = sourceOutputPorts.get(i);

            if (sourceIsRoot && Tags.hasTags(sourcePortSymbol, CanvasPortLayoutSymbol.KIND, layoutMode)) {
                continue;
            } else if (!sourceIsRoot && Tags.hasTags(sourcePortSymbol, PortLayoutSymbol.KIND, layoutMode)) {
                continue;
            }

            Node tempNodeSource = bus.sourceBus.busNode;

            if (tempNodeSource == null) {
                System.err.println("PortCH: BusSourceTempNode is null: " + bus.sourceBus.node.compName + " -> " + bus.targetBus.node.compName + " #" + i);
            }

            Route route = RoutesUtils.getRouteForConnection(bus.sourceBus.node, tempNodeSource, layoutState, i);
            int routeIndex = routes.indexOf(route);

            if (route == null) {
                System.err.println("PortCH: route is null: " + bus.sourceBus.node.compName + " -> " + tempNodeSource.compName + " #" + i);
                continue;
            }

            if (bus.sourceBus.node.compName.equals(NAME_ROOT_IN)) {
                assignCoordinatesToEnclosingInputPort(routeIndex, sourcePortSymbol);
            } else {
                assignCoordinatesToInternalOutputPort(route.columnIndexOf(bus.sourceBus.node), routeIndex, sourcePortSymbol, routes);
            }
        }

        for (int i = 0; i < targetInputPorts.size(); i++) {
            PortSymbol targetPortSymbol = targetInputPorts.get(i);
            if (targetIsRoot && Tags.hasTags(targetPortSymbol, CanvasPortLayoutSymbol.KIND, layoutMode)) {
                continue;
            } else if (!targetIsRoot && Tags.hasTags(targetPortSymbol, PortLayoutSymbol.KIND, layoutMode)) {
                continue;
            }

            Node tempNodeTarget = bus.targetBus.busNode;

            if (tempNodeTarget == null) {
                System.err.println("PortCH: BusTargetTempNode is null: " + bus.sourceBus.node.compName + " -> " + bus.targetBus.node.compName + " #" + i);
            }

            Route route = RoutesUtils.getRouteForConnection(tempNodeTarget, bus.targetBus.node, layoutState, i);
            int routeIndex = routes.indexOf(route);

            if (route == null) {
                System.err.println("PortCH: route is null: " + tempNodeTarget.compName + " -> " + bus.targetBus.node.compName + " #" + i);
                continue;
            }

            if (bus.targetBus.node.compName.equals(NAME_ROOT_OUT)) {
                assignCoordinatesToEnclosingOutgoingPort(routeIndex, tableWidth, targetPortSymbol);
            } else {
                assignCoordinatesToInternalInputPort(route.columnIndexOf(bus.targetBus.node), routeIndex, targetPortSymbol, routes);
            }
        }
    }

    private void assignSingletonBusPortCoordinates(Bus bus, RoutesLayoutState layoutState) {
        Node source = bus.sourceBus.node;
        Node target = bus.targetBus.node;

        Route sourceRoute = RoutesUtils.getRouteForConnection(source, target, layoutState);
        Route targetRoute;
        if (sourceRoute != null) {
            targetRoute = sourceRoute;
        } else {
            sourceRoute = RoutesUtils.getRouteForConnection(source, bus.sourceBus.busNode, layoutState);
            targetRoute = RoutesUtils.getRouteForConnection(bus.targetBus.busNode, target, layoutState);
        }

        int tableWidth = RoutesUtils.getTableWidth(layoutState.routes);
        int sourceRouteIndex = layoutState.routes.indexOf(sourceRoute);
        int targetRouteIndex = layoutState.routes.indexOf(targetRoute);

        PortSymbol sourceOutputPort = bus.sourceBus.ports.get(0);
        PortSymbol targetInputPort = bus.targetBus.ports.get(0);

        if (sourceRoute != null) {
            int sourceNodeColumnIndex = sourceRoute.columnIndexOf(source);
            if (source.compName.equals(NAME_ROOT_IN)) {
                assignCoordinatesToEnclosingInputPort(sourceRouteIndex, sourceOutputPort);
            } else {
                assignCoordinatesToInternalOutputPort(sourceNodeColumnIndex, sourceRouteIndex, sourceOutputPort, layoutState.routes);
            }
        }

        if (targetRoute != null) {
            int targetNodeColumnIndex = targetRoute.columnIndexOf(target);
            if (target.compName.equals(NAME_ROOT_OUT)) {
                assignCoordinatesToEnclosingOutgoingPort(targetRouteIndex, tableWidth, targetInputPort);
            } else {
                assignCoordinatesToInternalInputPort(targetNodeColumnIndex, targetRouteIndex, targetInputPort, layoutState.routes);
            }
        }
    }

    private void assignCoordinatesToInternalInputPort(int column, int row, PortSymbol inputPort, List<Route> routes) {
        if (inputPort != null) {
            if (inputPort instanceof DummyPort) {
                return;
            }

            int left = RoutesUtils.getColumnLeft(column, routes) - (int) (PORT_WIDTH / 2.0);
            int top = RoutesUtils.getRowCenter(row) - (int) (PORT_HEIGHT / 2.0);

            Tags.addTag(inputPort, new PortLayoutSymbol(IdGenerator.getUniqueId(), left, top, true, layoutMode));
        }
    }

    private void assignCoordinatesToInternalOutputPort(int column, int row, PortSymbol outputPort, List<Route> routes) {
        if (outputPort != null) {
            if (outputPort instanceof DummyPort) {
                return;
            }

            int left = RoutesUtils.getColumnRight(column, routes) - (int) (PORT_WIDTH / 2.0);
            int top = RoutesUtils.getRowCenter(row) - (int) (PORT_HEIGHT / 2.0);

            Tags.addTag(outputPort, new PortLayoutSymbol(IdGenerator.getUniqueId(), left, top, true, layoutMode));
        }
    }

    private void assignCoordinatesToEnclosingInputPort(int row, PortSymbol port) {
        if (port != null) {
            if (port instanceof DummyPort) {
                return;
            }

            int top = RoutesUtils.getRowCenter(row) - (int) (PORT_HEIGHT / 2.0);
            int left = RoutesUtils.tableDimensions.getCanvasMarginLeft() - (int) (PORT_WIDTH / 2.0);

            Tags.addTag(port, new CanvasPortLayoutSymbol(IdGenerator.getUniqueId(), left, top, layoutMode));
        } else {
            System.err.println("port is null");
        }
    }

    private void assignCoordinatesToEnclosingOutgoingPort(int row, int tableWidth, PortSymbol port) {
        if (port != null) {
            if (port instanceof DummyPort) {
                return;
            }

            int top = RoutesUtils.getRowCenter(row) - (int) (PORT_HEIGHT / 2.0);
            int left = RoutesUtils.tableDimensions.getCanvasMarginLeft() + tableWidth - (int) (PORT_WIDTH / 2.0);

            Tags.addTag(port, new CanvasPortLayoutSymbol(IdGenerator.getUniqueId(), left, top, layoutMode));
        } else {
            System.err.println("port is null");
        }
    }
}
