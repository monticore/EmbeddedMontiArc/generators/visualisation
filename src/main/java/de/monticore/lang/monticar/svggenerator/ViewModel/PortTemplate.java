/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.ViewModel;

import de.monticore.lang.monticar.svggenerator.calculators.DrawingConstants;
import de.monticore.lang.monticar.svggenerator.calculators.symbols.LayoutPosition;

/**
 * Created by Felix on 11.03.2017.
 */
public class PortTemplate {


    public static final int MAX_PORT_NAME_LEN_BEFORE_WRAPPING = 14;

    // Add SVG code of all PortNames to SVGBuilder
    public static void drawPortDescription(StringBuilder svgBuilder, PortViewModel vm) {
        svgBuilder.append("<use x=\"" + vm.x + "\" y=\"" + vm.y + "\" " +
                "xlink:href=\"#port\" />\n");

        // Start parameters
        int x = vm.x;
        int y = vm.y + 10;
        String anchor = new String();
        int angle = 0;

        // Names for Canvas Ports
        if(vm.isIncoming && vm.layoutPosition == LayoutPosition.UNDEFINED) {
            x += -4;
            y += 0;
            angle = 0;
            anchor = "end";
        } else if(vm.isOutgoing && vm.layoutPosition == LayoutPosition.UNDEFINED) {
            x += DrawingConstants.PORT_WIDTH + 4;
            y += 0;
            angle = 0;
            anchor = "start";
        }
        // Names of Subcomponent Ports
        // Position of the port name depends on position of the port
        else if(vm.isInternal && vm.isOutgoing) {
            x -= 3; // optical improvement
            anchor = "end";
        } else if(vm.isInternal && vm.isIncoming) {
            x += 19; // width of port + optical improvement
            anchor = "start";
        } else if(!vm.isInternal && vm.isIncoming) {
            x += 19;
            if (vm.portname.length() < MAX_PORT_NAME_LEN_BEFORE_WRAPPING) {
                // If name exceeds a certain length, put it higher since it will have two lines
                x += 10;
            }
            anchor = "start";
        } else if(!vm.isInternal && vm.isOutgoing) {
            x -= 3;
            anchor = "end";
        }

        // SVG code
        svgBuilder.append("<text text-anchor=\"" + anchor + "\" alignment-baseline=\"middle\" " +
                "x=\"" + x + "\" y=\"" + y + "\" transform=\"rotate("
                + angle + "," + x + "," + y + ")\" ");
        svgBuilder.append("style=\"font-weight: regular\" font-family=\"Verdana\" " +
                "font-size=\"9\" fill=\"black\">\n");

        // Split the name into several lines if they exceed a certain length (prevent overlapping)
//        if(vm.portname.length() < MAX_PORT_NAME_LEN_BEFORE_WRAPPING) {
            svgBuilder.append(vm.portname + "\n");
//        } else {
//            // Split at Uppercase Letters
//            String[] split = vm.portname.split("(?<=.)(?=\\p{Lu})");
//
//            svgBuilder.append("<tspan>" + split[0] + "</tspan>");
//
//            // Create a new line for every segment
//            for (int i = 1; i < split.length; i++) {
//                svgBuilder.append("<tspan x=\"" + x + "\" dy=\"12\">" + split[i] + "</tspan>");
//            }
//        }

        svgBuilder.append("</text>\n\n");
    }


}
