/* (c) https://github.com/MontiCore/monticore */
package de.monticore.lang.monticar.svggenerator.calculators;

import de.monticore.lang.monticar.svggenerator.calculators.routes.Route;

import java.util.List;

/**
 * Created by Peters Notebook on 03.06.2017.
 */
public interface RouteOrderCalculator {
    List<Route> calculateRouteOrder(RoutesLayoutState layoutState);
}
