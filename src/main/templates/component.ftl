<#-- (c) https://github.com/MontiCore/monticore -->
<!-- Component: ${name} -->
<g transform="translate(${X} ${Y})">
    <rect
        data-name="${fullName}"
        id="${id}"
        width="${width}"
        height="${height}"
        style="
            <#if isEmpty == true>
                fill:white;
            <#else>
                fill:rgb(242,242,242);
            </#if>
            stroke:black;
            stroke-width:2;
            fill-opacity:1.0;
            stroke-opacity:1.0"
            onmouseout="this.style.stroke = '#000000';
                        this.style['stroke-width'] = 2;
                        this.style.cursor='default';"

        <#if isEmpty == false>
            onmouseover="this.style.stroke = '#316FC0';
                         this.style['stroke-width'] = 5;
                         this.style.cursor='pointer';"
            onclick="window.open('${fullName}${fileNameSuffix}.html','_parent',false);
                     parent.openFileInEditor('${idePath}');"
        </#if>
    />

    <text
        x="10"
        y="15"
        alignment-baseline="middle"
        style="font-weight: bold"
        font-family="Verdana"
        font-size="11"
        fill="black">
    ${name}
    </text>
</g>
