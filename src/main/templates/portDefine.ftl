<#-- (c) https://github.com/MontiCore/monticore -->
<defs>
    <!-- Small box indicating a port -->
    <rect id="port" x="0" y="0" width="${portWidth}" height="${portHeight}"
          style="fill:white;stroke:rgb(0,0,0);stroke-width:1;fill-opacity:1.0;stroke-opacity:1.0"/>
</defs>
