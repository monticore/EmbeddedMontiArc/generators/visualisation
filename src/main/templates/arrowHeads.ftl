<#-- (c) https://github.com/MontiCore/monticore -->
<defs>
<#list colors as color>
    <marker id="${color}" markerWidth="13" markerHeight="13" refX="5" refY="5" orient="auto">
            <polyline points="0,2 5,5 0,8" style="fill:none; stroke:rgb(${color});
                    stroke-width:1" />
    </marker>
</#list>
</defs>
