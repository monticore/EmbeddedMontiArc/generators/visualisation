<#-- (c) https://github.com/MontiCore/monticore -->
<line
    stroke-linecap="square"
    x1="${X}"
    y1="${Y}"
    x2="${XX}"
    y2="${YY}"
    style="stroke:rgb(${red}, ${green},${blue});
            stroke-width:${linewidth};
            <#if isEnd == true>
                marker-end: url(#${red},${green},${blue})
            </#if>"
/>
<#if indexInBus??>
    <text
    <#if isEnd == true>
        x="${XX - 11}"
        y="${YY - 11}"
    <#else>
        x="${X + 3}"
        y="${Y - 7}"
    </#if>
        alignment-baseline="middle"
        font-family="Verdana"
        font-size="11"
        fill="black">
    ${indexInBus}
    </text>
</#if>

<#if knotX?? && knotY?? && knotRadius??>
    <circle cx="${knotX}" cy="${knotY}" r="${knotRadius}" fill="black" />
</#if>
