<#-- (c) https://github.com/MontiCore/monticore -->

<!-- Canvas: ${name} -->
<g transform="translate(${X} ${Y})">

    <rect
        id="${id}"
        width="${width}"
        height="${height}"
        style="fill:white;stroke:black;stroke-width:2;fill-opacity:1.0;stroke-opacity:1.0"/>

    <text
        x="8"
        y="20"
        style="font-weight: bold"
        font-family="Verdana"
        font-size="13"
        fill="black">
            ${name}
    </text>

</g>
