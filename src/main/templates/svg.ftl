<#-- (c) https://github.com/MontiCore/monticore -->
<svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink">
    ${svgbody}
</svg>
