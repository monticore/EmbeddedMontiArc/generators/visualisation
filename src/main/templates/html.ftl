<#-- (c) https://github.com/MontiCore/monticore -->
<!DOCTYPE html>
<html>
<head>
    <title>${fullName}</title>
    <meta name="viewport" content="width=device-width">
    <#if onlineIDE>
    <script type="text/javascript">
        function openFileInEditor(filePath) {
    	    var parent = window.parent;
    	    while (parent.parent != null && parent.api == null) {
    	        parent = parent.parent;
    	    }

    	    if (parent.api == null) {
    	        return;
    	    }

            var api = window.parent.api;
            var tabManager = api.tabManager;

            function onOpenFile(error, tab) {
                if(error) console.error(error);
            }

            tabManager.openFile(filePath, true, onOpenFile);
    	}
     </script>
    </#if>
</head>
<body link="black" vlink="black">

<!-- Bread crumb navigation -->
<table>
    <tr height="25px">
    <td width="25px" align="center"
        <#if fileNameSuffix == "">style="background-color:LightGray;"</#if>>
        <a href="${fullName}.html">
            <img src="icons/icon_full_detail.svg"
                 height="20px"
                 width="20px"
                 align="middle"
                 valign="middle">
        </a>
    </td>
    <td width="25px" align="center"
        <#if fileNameSuffix == "_no_port_names">style="background-color:LightGray;"</#if>>
        <a href="${fullName}_no_port_names.html">
            <img src="icons/icon_no_port_names.svg"
                 height="20px"
                 width="20px"
                 align="middle"
                 valign="middle">
        </a>
    </td>
    <td width="25px" align="center"
        <#if fileNameSuffix == "_simplified">style="background-color:LightGray;"</#if>>
        <a href="${fullName}_simplified.html">
            <img src="icons/icon_simplified.svg"
                 height="20px"
                 width="20px"
                 align="middle"
                 valign="middle">
        </a>
    </td>
        <#if nameHierarchy?? && fullNameHierarchy??>
        <td align="center" style="min-width: 10px;"/>
            <#list 0..(nameHierarchy?size - 1) as index>
                <td>
                <#if index < (nameHierarchy?size - 1)>
                    <a href="${fullNameHierarchy[index]}${fileNameSuffix}.html">
                        <b><big>${nameHierarchy[index]}</big></b>
                    </a>&nbsp;&#8594;&nbsp;
                <#else>
                    <b><big>${nameHierarchy[index]}</big></b>
                </#if>
                </td>
            </#list>
        </#if>
    </tr>
</table>

<!-- HTML Object to display an external SVG-File -->
<object
    data="${fullName}${fileNameSuffix}.svg"
    type="image/svg+xml"
    id="svg"
    width="${width}"
    height="${height}">
</object>

</body>
</html>
