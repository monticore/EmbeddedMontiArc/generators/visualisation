<!-- (c) https://github.com/MontiCore/monticore -->
  [![Maintainability](https://api.codeclimate.com/v1/badges/61384df055df4f50c231/maintainability)](https://codeclimate.com/github/EmbeddedMontiArc/visualisation/maintainability)
  [![Build Status](https://travis-ci.org/EmbeddedMontiArc/visualisation.svg?branch=visualization)](https://travis-ci.org/EmbeddedMontiArc/visualisation)
  [![Build Status](https://circleci.com/gh/EmbeddedMontiArc/visualisation/tree/visualization.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/EmbeddedMontiArc/visualisation/tree/visualization)
  [![Coverage Status](https://coveralls.io/repos/github/EmbeddedMontiArc/visualisation/badge.svg?branch=visualization&service=github)](https://coveralls.io/github/EmbeddedMontiArc/visualisation)

# visualisation

Example generated outputs are available at:
* https://embeddedmontiarc.github.io/webspace/Models2018.EXE/htmlModels/
* https://embeddedmontiarc.github.io/webspace2/index_master9.htm
